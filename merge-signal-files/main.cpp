#include "td_signal.hpp"
#include <cerrno>
#include <cstdint>
#include <cstdio>
#include <ctime>
#include <limits>

time_t
first_time(const char* tick_data_path, const int64_t spos)
{
  size_t size = 0;
  time_t tm = 0;
  TICK_DATA* td = load_tick_bin(tick_data_path, &size, spos, spos + 10);
  if (td) {
    tm = time_t(td->ftime);
    free(td);
  }

  return tm;
}

int
main(int argc, char** argv)
{
  if (argc != 7) {
    printf("Usage msf <signal file path> <signal index> <amount> <destination "
           "file> <tick-data-path> <start pos>\n");
    return (0);
  }

  const int64_t spos = std::stoll(argv[6]);
  const time_t stm = first_time(argv[5], spos);
  if (!stm)
    return (-100);

  // get the first time
  printf("Start time is %ld\n", stm);

  const int n = std::stol(argv[3]);
  FILE** fps = new FILE*[n];
  FILE* fd = nullptr;

  int ret = 0;

  for (int i = 0; i < n; ++i) {
    const int signal_idx = std::stol(argv[2]);
    char filepath[1024];
    snprintf(filepath, 1024, "%s/signal-%d-%da.log", argv[1], signal_idx, i);
    fps[i] = fopen(filepath, "r");
    if (!fps[i]) {
      ret = -1;
      printf("Can not open %s, error %d\n", filepath, errno);
    } else {
      printf("OK! %s\n", filepath);
    }
  }

  if (!ret) {
    fd = fopen(argv[4], "w+");
    if (!fd) {
      ret = -2;
      printf("Can not open %s to write, eoor %d\n", argv[4], errno);
    }
  }

  if (!ret) {
    char* buf = new char[2048 * n];
    bool* good = new bool[n];
    int* conter = new int[n];
    bool* early = new bool[n];
    time_t* tm = new time_t[n];
    int k = -1;

    int good_file = n;
    memset(good, 1, sizeof(bool) * n);
    memset(early, 1, sizeof(bool) * n);
    memset(conter, 0, sizeof(int) * n);
    memset(tm, 0, sizeof(time_t) * n);

    auto get_time = [](const char* line) {
      Document doc;
      if (doc.Parse(line).HasParseError()) {
        printf("Error at %s\n", line);
        return time_t(0);
      }

      time_t tm = 0;
      if (doc.HasMember("i")) {
        tm = time_t(doc["i"].GetInt64());
      } else {
        printf("Error pos at %s\n", line);
      }
      return tm;
    };

    for (int i = 0; i < n; ++i) {
      char* line = buf + i * 2048;
      while ((tm[i] < stm) && fgets(line, 2048, fps[i])) {
        ++conter[i];

        tm[i] = get_time(line);
        if (tm[i] >= stm) {
          break;
        }
      }
    }

    auto get_min_time = [tm, n]() {
      int idx = 0;
      time_t t = tm[0];
      // printf("%ld, %ld, %ld, %ld, %ld\n", tm[0], tm[1], tm[2], tm[3], tm[4]);
      for (int i = 1; i < n; ++i) {
        if (t > tm[i]) {
          t = tm[i];
          idx = i;
        }
      }
      return idx;
    };

    while (good_file > 0) {
      // printf("K is %d, time is %ld\n", k, t_now);
      k = get_min_time();

      char* line = buf + 2048 * k;
      fputs(line, fd);

      // get next time from file #k.
      time_t tm_k = 0;
      while (!tm_k && good[k]) {
        if (fgets(line, 2048, fps[k])) {
          ++conter[k];
          tm_k = get_time(line);
          // printf("Get time %ld from file #%d\n", tm_k, k);
        } else {
          good[k] = false;
          --good_file;
          printf("File %d was over\n", (k + 1));
        }
      }
      if (tm_k)
        tm[k] = tm_k;
      else
        tm[k] = std::numeric_limits<time_t>::max();
    }

    delete[] tm;
    delete[] conter;
    delete[] good;
    delete[] buf;
  }

  fclose(fd);

  for (int i = 0; i < n; ++i) {
    if (fps[i])
      fclose(fps[i]);
  }

  delete[] fps;
  return ret;
}
