#include "tick.h"
#include "rapidjson/document.h"
#include <cstddef>
#include <cstdint>
#include <string>

using Document = rapidjson::Document;

int
main(int argc, char** argv)
{
  if (argc != 6) {
    printf("Usage: %s <signal filepath> <tick filepath> <new signal filepath> "
           "<spos> <epos>\n",
           argv[0]);
    return (0);
  }

  int64_t spos = std::stoll(argv[4]);
  int64_t epos = std::stoll(argv[5]);
  int64_t size = 0;
  TICK_DATA* td = load_all_tick_bin(argv[2], (size_t*)(&size));
  if (td) {
    if (spos < 0)
      spos = 0;
    if (epos <= spos)
      epos = size - 1;

    printf("%.02lf, %.02lf ~~ %.02lf %.02lf\n",
           td[spos].price,
           td[spos].ftime,
           td[epos].price,
           td[epos].ftime);

    time_t st = time_t(td[spos].ftime);
    time_t et = time_t(td[epos].ftime);

    free(td);

    FILE* fp_src = fopen(argv[1], "r");
    if (!fp_src) {
      printf("Can not open %s to read\n", argv[1]);
      return -1;
    }

    FILE* fp_dst = fopen(argv[3], "w+");
    if (!fp_dst) {
      printf("Can not open %s to write\n", argv[3]);
      return -2;
    }

    char buf[2048];
    while (fgets(buf, 2048, fp_src)) {
      Document doc;
      if (doc.Parse(buf).HasParseError())
        continue;
      if (doc.HasMember("i")) {
        time_t tm = time_t(doc["i"].GetInt64());
        if (tm > et)
          break;
        else if (tm >= st)
          fputs(buf, fp_dst);
      }
    }

    if (fp_dst) {
      fclose(fp_dst);
    }

    if (fp_src) {
      fclose(fp_src);
    }
  }

  return (0);
}
