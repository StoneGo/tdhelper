# Created by and for Qt Creator This file was created for editing the project sources only.
# You may attempt to use it for building too, by modifying this file here.

cmake_minimum_required(VERSION 3.5)
project(tdhelper VERSION 1.0 DESCRIPTION "the trading backtest")

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set (MU /ssd1/mycat/usr)
# set (MU /usr/local)
set (LIB_INSTALL_DIR ${MU}/lib)
set (BIN_INSTALL_DIR ${MU}/bin)
set (INCLUDE_INSTALL_DIR ${MU}/include)

include_directories(
  include
  ../include
  /usr/include
  /usr/local/include
  ${MU}/include
)



link_directories(
  /usr/local/lib
  /usr/local/lib64
  /usr/lib
  /usr/lib64
  /lib
  /lib64
  ${MU}/lib
  ${MU}/lib64
)


#configuration types
SET(CMAKE_CONFIGURATION_TYPES "Debug;Release" CACHE STRING "Configs" FORCE)
IF(DEFINED CMAKE_BUILD_TYPE AND CMAKE_VERSION VERSION_GREATER "2.8")
  SET_PROPERTY(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS ${CMAKE_CONFIGURATION_TYPES})
ENDIF()


IF (CMAKE_BUILD_TYPE MATCHES Debug)
  message("Debug build")

  add_compile_definitions(STONE_DEBUG=1)
  add_compile_definitions(STEP_BY_STEP=0)
  add_compile_definitions(RELEASE_DEBUG=0)
  add_compile_definitions(ONLINE_DATA_COMM=0)
  add_compile_definitions(USING_FILE_TRADE_SPOT=1)


  set(CMAKE_C_FLAGS_DEBUG "-Wall -g")
  set(CMAKE_CXX_FLAGS_DEBUG "-Wall -g")
  set(CMAKE_DEBUG_POSTFIX "d")
ENDIF ()


IF (CMAKE_BUILD_TYPE MATCHES Release)
  message("Release build")

  add_compile_definitions(STONE_DEBUG=0)
  add_compile_definitions(STEP_BY_STEP=1)
  add_compile_definitions(RELEASE_DEBUG=0)
  add_compile_definitions(ONLINE_DATA_COMM=0)
  add_compile_definitions(USING_FILE_TRADE_SPOT=0)
  add_compile_definitions(SAVE_STOP_IDX_TO_FILE=0)

  
  set(CMAKE_C_FLAGS "-Wall -pipe -m64 -O3 -flto=auto -ffast-math -march=native -funroll-loops -fomit-frame-pointer")
  set(CMAKE_CXX_FLAGS "-Wall -pipe -m64 -O3 -flto=auto -ffast-math -march=native -funroll-loops -fomit-frame-pointer")

ENDIF ()


set (LIBSDG sdgdbg)

add_library(${LIBSDG} SHARED ../include/make_deal.hpp
  ../include/dirs.hpp
  ../include/energy.hpp
  ../include/fighting.hpp
  ../include/lines.hpp
  ../include/mockif.hpp
  ../include/position.hpp
  ../include/schrodinger.hpp
  ../include/spacetime.hpp
  ../include/swing.hpp
  ../include/td_signal.hpp
  ../include/tick.h
  ../include/tjm.hpp
  ../include/turn.hpp
  ../include/wave.hpp
  ../include/wfw.hpp
  ../src/dirs.cpp
  ../src/energy.cpp
  ../src/fighting.cpp
  ../src/lines.cpp
  ../src/make_deal.cpp
  ../src/md.cpp
  ../src/mockif.cpp
  ../src/position.cpp
  ../src/schrodinger.cpp
  ../src/spacetime.cpp
  ../src/spacetime.cpp
  ../src/swing.cpp
  ../src/td_signal.cpp
  ../src/tick.c
  ../src/tjm.cpp
  ../src/turn.cpp  
  ../src/wave.cpp
  ../src/wfw.cpp)
target_compile_definitions(${LIBSDG} PRIVATE _UNIX)

target_link_libraries(${LIBSDG} pthread)

install(TARGETS ${LIBSDG}
LIBRARY DESTINATION ${LIB_INSTALL_DIR}
PUBLIC_HEADER DESTINATION ${INCLUDE_INSTALL_DIR})
