# Play Hard To Get

## Up And Down

-- Sure, any signal is probability wrong.

### The key direction signal 
- Wave (w)
- VerifySpotSet (mid)
- Einstien (ein)
When these signals were happened, Check the conditions for going up or going down.

## The Conditions
### Single Spot, Ambigouous Spots
```c++
  if (dir == -spot->dir) {
    if (std::abs(fpos - spot->fpos) < HALF_HOUR) {
      atype |= AMBIGUOUS_SPOT_DELTA;
    }

    if (spot->grade == grade) {
      atype |= AMBIGUOUS_SPOT_LEVEL;
    }
  }
```
### SpotPair
The two spots have same dir and same grade.

```c++
  for (auto it1 = ss->begin(); it1 != ss->end(); ++it1) {
    if ((*it1)->ip_type == IP_TYPE_MISSED_TB_SPOT ||
        (*it1)->ip_type == IP_TYPE_MYSELF)
      continue;

    auto it2 = it1;
    ++it2;
    for (; it2 != ss->end(); ++it2) {
      if (((*it1)->dir == (*it2)->dir) && ((*it1)->grade == (*it2)->grade)) {
        SpotPairPtr vs = std::make_shared<SpotPair>();
        vs->dir = (*it1)->dir;
        if ((*it2)->fpos - (*it1)->fpos <= 900)
          vs->slope_dir = 0;
        else
          vs->slope_dir = sign(((*it2)->hp - (*it1)->hp));

        if ((std::abs((*it2)->fpos - (*it1)->fpos) >=
             int64_t(3 * double(ONE_HOUR))) &&
            (std::abs((*it2)->hp - (*it1)->hp) >= 6.9)) {
          vs->type = SPOT_PAIR_TYPE::SPOT_PAIR_SLOPED;
        } else if (std::abs((*it2)->fpos - (*it1)->fpos) >=
                   int64_t(1.2 * double(ONE_HOUR))) {
          if (std::abs((*it2)->hp - (*it1)->hp) >= 9) {
            vs->type = SPOT_PAIR_TYPE::SPOT_PAIR_SLOPED;
          } else {
            vs->type = SPOT_PAIR_TYPE::SPOT_PAIR_HORIZONTAL;
          }
        } else {
          if (std::abs((*it2)->hp - (*it1)->hp) >= 9) {
            vs->type = SPOT_PAIR_TYPE::SPOT_PAIR_VERTICAL;
          } else {
            vs->type = SPOT_PAIR_TYPE::SPOT_PAIR_LIKE_ONE;
          }
        }

        vs->s1 = *it1;
        vs->s2 = *it2;
        vsv.push_back(vs);
      }
    }
  }
```
### The G2 Spots
The A2 and V2 spots
```c++
  for (auto it = ss.begin(); it != ss.end(); ++it) {
    if ((*it)->delta < 0)
      continue;

    if ((*it)->grade == 2) {
      // ignore very large difference
      if (((*it)->dir < 0 && hpx - bottom > 90 && hpx - (*it)->hp > 90) ||
          ((*it)->dir > 0 && top - hpx > 90 && (*it)->hp - hpx > 90))
        continue;

      if ((*it)->dir > 0) {
        a2 = *it;
      } else {
        v2 = *it;
      }
    }
  }

  if (a2 && v2) {
    if (a2->fpos <= v2->fpos)
      type = 2;
    else
      type = -2;
  } else {
    if (a2)
      type = 1;
    else if (v2)
      type = -1;
  }
```

## The score of up and down

### bitmap for the condition hiting mark
```js
 +------------------------------------------+
 |           UP        |      Down          |
 +------------------------------------------+
 |        ss | sp | a2 | ss | sp |v2|       |
 +------------------------------------------+
```
### the score
How to set the wight of conditions?
For Now:
- Every Condition Is Same

## The Strategy: playing hard to get
The Strategy:
- going down before going up
- going up before going down

