## Communication
- mqtt_comm.hpp mqtt_comm.cpp
- rapidsjson, JSON for c++

## Mock Trade Interface
- mockif.hpp, mockif.cpp
- position.hpp, position.cpp

## Basic Data and Signal
### Tick data
- tick.h, tick.c

### Directions, Time and Space
- dirs.hpp, dirs.cpp
- lines.hpp, lines.cpp
- wave.hpp, wave.cpp
- wfw.hpp, wfw.cpp

### Parse Signal and Tick
- td_signal.hpp, td_signal.cpp

## Signal Compound
based on basic data and signal

- spactime.hpp, spactime.cpp
- energy.hpp, energy.cpp
- fighting.hpp, figting.cpp
- swing.hpp, swing.cpp
- turn.hpp, turn.cpp

## Pet for trade
- schrodinger.hpp, schrodinger.cpp
  - TOM, Jerry, Mickey

## Trade Strategy
- make_deal.hpp
- make_deal.cpp
- md.cpp
- fighting.hpp fighting.cpp
- tjm.hpp, tjm.cpp
