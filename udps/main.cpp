#include <arpa/inet.h>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <memory.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

int
main(const int argc, const char** argv)
{
  if (argc != 3) {
    printf("Usage %s <port> <bind ip>\n", argv[0]);
    return (0);
  }
  
  struct sockaddr_in addr;
  addr.sin_family = AF_INET;
  addr.sin_port = htons(uint16_t(atoi(argv[1])));
  addr.sin_addr.s_addr = inet_addr(argv[2]);

  const int recvbuf_len = 2048;
  char line[recvbuf_len];
  int sock_fd = 0;
  if ((sock_fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
    printf("Error 1\n");
    return -1;
  }

  // port bind to server
  if (bind(sock_fd, (struct sockaddr*)&addr, sizeof(addr)) < 0) {
    printf("Error 2\n");
    return -2;
  }

  struct sockaddr_in clientAddr;
  memset(&clientAddr, 0, sizeof(sockaddr_in));
  socklen_t socklen = sizeof(sockaddr_in);

  for (;;) {
    int len = recvfrom(
      sock_fd, line, recvbuf_len, 0, (struct sockaddr*)&clientAddr, &socklen);
    if (len < 0) {
      printf("Receive UDP Data error %d\n", len);
      break;
    } else if (len > 0) {
      line[len] = 0;
      printf("%s\n", line);
    }
  }

  close(sock_fd);
  sock_fd = 0;

  return (0);
}
