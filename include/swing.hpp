#pragma once
#include "lines.hpp"
#include "spacetime.hpp"
#include <cstdint>
#include <memory.h>

namespace Trade {

struct Swing
{
  bool swing = false;
  dir_t dir_swing = 0;
  int64_t idx = 0;
  double hpx = 0;
  time_t ts = 0;
  time_t te = 0;
  time_t prev_stime = 0;
  time_t prev_etime = 0;
  Lines prev_lines;
  Spacetime target;

  void print(const time_t t_now, const double hp_now) noexcept;
  void reset() noexcept;
  void set(const int64_t idx,
           const double hp_now,
           const time_t t_now,
           const Lines& lines) noexcept;
  int8_t update_lines(const Lines& lines, const double hp_now) noexcept;
  uint8_t reached(const time_t t_now,
                  const double hp_now,
                  Lines& l_now,
                  const int64_t idx) noexcept;
  uint8_t reacheded() noexcept;
  void clear_reacheded() noexcept;
  bool reached_line2(const dir_t d) noexcept;
  bool reached_line1(const dir_t d) noexcept;
  bool timer_outed() noexcept;
  void update_timer(const time_t tm) noexcept;
  bool set_swing_dir(const uint8_t rchd) noexcept;
};

} // namespace Trade