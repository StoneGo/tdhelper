#pragma once

#include "energy.hpp"
#include "schrodinger.hpp"
#include <cstdint>
#include <memory>
#include <vector>


using case_t = uint32_t;

namespace Trade {

struct TjmAction
{
  dir_t d = 0;
  PET_ACTION pet_action;
  DealCasePtr dc;
};

using TjmActionPtr = std::shared_ptr<TjmAction>;
using TjmActionVector = std::vector<TjmActionPtr>;

extern TjmActionVector tjm;

bool
add_tjm_action(const dir_t d,
               const PET_ACTION pa,
               const DealCasePtr& dc,
               const time_t ts) noexcept;
bool
verify_tjm_action() noexcept;
void
clear_tjm_action() noexcept;

// 0.5 * (sqrt(5) - 1)
#define phi (0.6180339887498949)
// 1 - phi
#define ppi (0.3819660112501051)

#define TJM_ONE_DIR(d) ((d) == DIR_UP || (d) == DIR_DN)
#define TJM_TWO_DIR(d) ((d) == DIR_SWING)

#define TJM_SOLVED_SPP_DIR_WITH_TIMER 4 // case number
#define TJM_CASENO_REACHED_OPEN_BUT_OPPOSITE 20001

#define MAX_OPENING_LOSS_LINE_1 35
#define MAX_OPENING_LOSS_LINE_2 55

const case_t PRICE_CNO_GOOD_OPPOSITE_LINE2 = 1;
const case_t PRICE_CNO_NOT_GOOD_BUT_GOOD = 64;

const double CLOSE_REVENUE_FOOL_FORCED = -80;
const double CLOSE_REVENUE_LOSS_ONE_THIRD = -25;
const double CLOSE_REVENUE_TOLERATE_REVENUE = -10;
const double CLOSE_REVENUE_WIN_ONE_THIRD = 25;
const double CLOSE_REVENUE_MIN_TO_WIN = 8;

} // namespace Trade
