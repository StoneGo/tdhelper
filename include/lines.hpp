#pragma once
#include <cstdint>
#include <ctime>
#include <limits>
#include <memory.h>

using dir_t = int8_t;
using case_t = uint32_t;
using event_t = uint32_t;

namespace Trade {

struct ExtRegion
{
  dir_t dir = 0;
  bool top = false;
  bool btm = false;
  double hp_A = 0;
  double hp_V = 0;
  double hpx = 0;

  void reset() noexcept { memset((void*)(this), 0, sizeof(ExtRegion)); };
  dir_t large_reflection(const double hp) const noexcept;
  dir_t ext_dir(const double hpx) noexcept;
  double get_top() const noexcept { return hpx - hp_A; };
  double get_btm() const noexcept { return hpx - hp_V; };
};

enum LIMIT_REACHED : uint8_t
{
  NONE = 0,
  UP_1 = 1,
  UP_2 = 2,
  DN_1 = 4,
  DN_2 = 8,
  TIMER_OUT = 16,
  ENTERED = 32,
  LEFT = 64,
};

enum class LINE_REGION : uint8_t
{
  UNKNOW = 0,
  MIDDLE = 1,
  ABOVE_1 = 2,
  ABOVE_2 = 3,
  ABOVE_2_1 = 4,
  BELOW_1 = 5,
  BELOW_2 = 6,
  BELOW_2_1 = 7,
};

enum PRICE_REGION : uint8_t
{
  UNKNOW = 0,
  UP = 1,
  UP2 = 2,
  MID = 4,
  DN = 8,
  DN2 = 16,
  BAD_HEIGHT = 32,
};

struct NearestDelta
{
  double up_1 = 0;
  double up_2 = 0;
  double dn_1 = 0;
  double dn_2 = 0;
  double up_1_pct = 0;
  double up_2_pct = 0;
  double dn_1_pct = 0;
  double dn_2_pct = 0;

  void reset() noexcept
  {
    up_1 = up_2 = dn_1 = dn_2 = up_1_pct = up_2_pct = dn_1_pct = dn_2_pct =
      std::numeric_limits<double>::max();
  }
};

struct Lines
{
  int8_t nup = 0;
  int8_t ndn = 0;
  uint8_t reached = 0; // hit
  double hpx = 0;
  double hp_up_1 = 0;
  double hp_up_2 = 0;
  double hp_dn_1 = 0;
  double hp_dn_2 = 0;
  NearestDelta nd;
  int64_t idx = 0;

  ExtRegion ext_rgn;
  // SmallGradePrice sgp;

  void reset() noexcept { memset((void*)this, 0, sizeof(Lines)); }
  void print(const int64_t idx, const double hpx) noexcept;
  double main_space() const noexcept { return (hp_up_1 - hp_dn_1); }
  bool good_main_space() const noexcept;
  bool between_the_lines(const double hpx, const dir_t d) noexcept;
  double get_max_min_line(const dir_t d) noexcept;
  double get_middle_line(const dir_t d) noexcept;

  uint8_t limit_reached(const double hpx) noexcept;

  double get_line1_space(const dir_t d, const double x) const noexcept;
  double get_opposite_line_1(const dir_t d, const double hpx) const noexcept;
  bool good_space_for_dir(const double x,
                          const dir_t d,
                          int8_t& n_target,
                          int8_t& x1_id,
                          int8_t& x2_id,
                          double& space,
                          double& target_x1,
                          double& target_x2) noexcept;
  double space_for_dir(const double x, const dir_t d) noexcept;

  bool is_ext_top_btm(const double y, const dir_t d) noexcept;
  int8_t amount(const dir_t d) noexcept
  {
    return (d > 0) ? nup : ((d < 0) ? ndn : 0);
  }

  void copy(const Lines& l) noexcept
  {
    memcpy((void*)(this), &l, sizeof(Lines));
    reached = 0;
  }

  LINE_REGION region(const double hpx) noexcept;

  //
  // d > 0, update up12; d < 0, update dn12; d = update both.
  //
  int8_t update_lines(const Lines& l, const dir_t d, const double hpx) noexcept;
  void print_hited(const double hp_now, const int64_t idx) noexcept;
  uint8_t retrive_reached(const dir_t d) noexcept;
  uint8_t retrive_opposite_reached(const dir_t d) noexcept;
  uint8_t retrive_all_reached() noexcept;
  bool reached_opposite_line1(const dir_t d) noexcept;
  bool reached_opposite_line2(const dir_t d) noexcept;
  bool reached_line1(const dir_t d) noexcept;
  bool reached_line2(const dir_t d) noexcept;

  bool almost_reached_line2(const dir_t d) const noexcept;
  bool only_one_line(const dir_t d) const noexcept;
  double get_up_golden_point() const noexcept
  {
    if (ndn && nup) {
      return (hp_up_1 - hp_dn_1) * .618 + hp_dn_1;
    }
    return (0);
  }

  double get_dn_golden_point() const noexcept
  {
    if (ndn && nup) {
      return (hp_up_1 - hp_dn_1) * .382 + hp_dn_1;
    }
    return (0);
  }

  uint8_t ext_reached() noexcept;
  dir_t get_ext_hit_dir(const ExtRegion& ext_rgn) noexcept;

  PRICE_REGION get_price_region(const double hpx) const noexcept;
  bool only_one_line_and_large_space(const dir_t d,
                                     const double hpx) const noexcept;
  bool good_entry_region(const dir_t d, const double hpx) noexcept;
  bool good_line_region(const dir_t d, const double hpx) noexcept;
};

extern bool
limit_reached_this_side(const dir_t d, const uint8_t reached);
extern bool
limit_reached_line2(const dir_t d, const uint8_t reached);
extern bool
limit_reached_line1(const dir_t d, const uint8_t reached);

extern uint8_t
idx_of_reached_line(const uint8_t reached);
extern void
print_reached(const char* name,
              const dir_t d,
              const uint8_t reached,
              const int64_t idx) noexcept;
extern bool
longer_than(const time_t tm, const time_t t_now, const int64_t delta) noexcept;
extern bool
less_than(const time_t tm, const time_t t_now, const int64_t delta) noexcept;


} // namespace Trade
