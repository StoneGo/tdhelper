#pragma once

#include "lines.hpp"
#include <cstdint>
#include <memory>
#include <vector>

namespace Trade {

enum class DEAL_ACTION : uint8_t
{
  DA_NONE = 0,
  DA_RUN_TOM = 1,
  DA_RUN_JERRY = 2,
  DA_RUN_MICKEY = 3,
  DA_STOP_TOM = 4,
  DA_STOP_JERRY = 5,
  DA_STOP_MIKEY = 6,
  DA_CLOSE_POSITION = 7,
  DA_OPEN_POSITION = 8,
  DA_EMPTY_POSITION = 9,
};

struct DealAction
{
  dir_t dir = 0; // the dir to close/reverse/open
  DEAL_ACTION action = DEAL_ACTION::DA_NONE;

  DealAction(const dir_t d, const DEAL_ACTION a) noexcept
    : dir(d)
    , action(a)
  {
  }
};

using DealActionPtr = std::shared_ptr<DealAction>;
using DealActionVec = std::vector<DealActionPtr>;

extern void
print_pet_action(const DEAL_ACTION action) noexcept;

extern int
fight_add_fight_action(DealActionVec& dav,
                       const DEAL_ACTION action,
                       const dir_t d) noexcept;

} // namespace Trade