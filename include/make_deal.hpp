#pragma once

#include "dirs.hpp"
#include "energy.hpp"
#include "fighting.hpp"
#include "lines.hpp"
#include "mockif.hpp"
#include "position.hpp"
#include "schrodinger.hpp"
#include "swing.hpp"
#include "tick.h"
#include "tjm.hpp"
#include "turn.hpp"
#include "wfw.hpp"
#include <cstdint>
#include <cstring>
#include <ctime>
#include <sys/types.h>
#include <utility>
#include <vector>

namespace Trade {

struct Pricing
{
  dir_t d = 0;
  PRICING_TYPE asap_type = PRICING_TYPE_NONE;
  int8_t n_lines = 0;

  // 1 for hit, -1 for had delt, 0 for not hit
  int8_t pr_hpx = 0;    // we have asap hpx, come from now_hp or box-hp
  int8_t pr_line_1 = 0; // we have line 1 hpx, come from lines-1
  int8_t pr_line_2 = 0; // we have line 2 hpx, come from lines-2
  // 1 for hit, -1 for had delt, 0 for not hit

  int take_unit = 0;
  double hpx = 0;
  double line1 = 0;
  double line2 = 0;

  void reset() noexcept { memset((void*)(this), 0, sizeof(Pricing)); }
  void print(const double hpx) noexcept;
};

using VolumeVector = std::vector<int>;

enum class PRICING_STRATEGY : uint8_t
{
  Wukong = 0,
  Bajie = 1,
  Shaseng = 2,
  Tang = 3,
};

enum TRADE_ACTION_TYPE : int8_t
{
  TRADE_ACTION_NONE = 0,
  TRADE_ACTION_DO_DIR = 1,
  TRADE_ACTION_CLOSE = 2,
  TRADE_ACTION_OPEN = 3,
  TRADE_ACTION_CUT_PART = 4,
};

enum EVENT_CHANGE_TYPE : event_t
{
  EVENT_CHANGE_NONE = 0,
  EVENT_5DIRS_CHANGED = 1,
  EVENT_TOM_REACHED = 2,
  EVENT_JERRY_REACHED = 4,
  EVENT_MICKEY_REACHED = 8,
  EVENT_ENERGY_DIR_CHANGED = 16,
  EVENT_SWING_CHANGED = 32,
  EVENT_SWING_DIR_CHANGED = 64,
  EVENT_TOM_REACHED_PRICE = 128,
  EVENT_BIG_DIR_CHANGED = 256,
  EVENT_PDC_DIR_CHANGED = 512,
  EVENT_FIGHT_DIR_CHANGED = 1024,
  EVENT_CE_DIR_CHANGED = 2048,
  EVENT_IMPORTANT_TURN = 4096,
  EVENT_BIG_NEEDLE_OCCURED = 8192,
  EVENT_TTS_CHANGED = 16384,
  EVENT_TOM_REACHED_CLOSE_HPX = 32768,
  EVENT_TOM_REACHED_FORCED = 65536,
  EVENT_TURN_CHANGED = 131072,      // 65536 * 2,
  EVENT_SPACE_DIR_CHANGED = 262144, // 65536 * 4,
  EVENT_SPACE_DIR_STABLE = 524288,  // 65536 * 8,
  EVENT_BIG_NEEDLE_DISAPPEARED = 1048576,  // 65536 * 16,
};

struct TheDeal
{
  char dir = 0;
  uint8_t to_amount = 0;
  uint8_t reached = 0;
  uint8_t c_case = 0;
  bool only_for_space = false;
  uint8_t turn_time_status = 0;
  int index = 0;
  int hited = 0;
  int64_t o_idx = 0;
  int64_t c_idx = 0;
  time_t o_tm = 0;
  time_t c_tm = 0;
  double o_hp = 0;
  double c_hp = 0;
  double rvn_max = 0;
  double hp_rvn_max = 0;
  int64_t idx_rvn_max = 0;
  double rvn_min = 0;
  double hp_rvn_min = 0;
  int64_t idx_rvn_min = 0;
  double rvn_px = 0;
  double rvn_hp = 0;
  double hpx_avg = 0;
};
using TheDealPtr = std::shared_ptr<TheDeal>;
using TheDealVector = std::vector<TheDealPtr>;

enum class STRONG_LEVEL : uint8_t
{
  UNKNOW = 0,
  TIME_OUT = 1,
  LINE_OPPO = 2,
  LINE_1 = 3,
  LINE_2 = 4,
  LINE_2_2 = 5,
  LINE_2_2_2 = 6,
};

struct Dir
{
  dir_t d = 0;
  case_t cno = 0;

  Dir(const dir_t dir, const case_t case_no)
    : d(dir)
    , cno(case_no)
  {
  }
};

using DirVector = std::vector<Dir>;

using Case_Result = std::pair<int, case_t>;
extern Case_Result
make_case_result(int result, case_t case_no) noexcept;

class MakeDeal
{
public:
  MakeDeal();
  ~MakeDeal();

#if (ONLINE_DATA_COMM)
  int make_deal(const short udp_port) noexcept;
#else
  int mock_make_deal(const TICK_DATA* td,
                     const time_t stm,
                     const int64_t spos,
                     const int64_t epos) noexcept;
#endif // ONLINE_DATA_COMM
  void set_hop(const double hop) noexcept;
  void show_revenue() noexcept;
  void show_params() noexcept;
  void show_deal_record() noexcept;
  void set_save_stop_idx(const bool save) noexcept { save_stop_idx = save; }

public:
  dir_t bear_dir = 0;
  dir_t the_dir = 0;
  dir_t important_turn_dir = 0;
  dir_t dir_turns_space = 0;
  bool space_dir_is_stable = 0;
  DIR_TYPE tjm_dir = DIR_NONE;
  uint8_t r_tom = 0;
  uint8_t r_jerry = 0;
  uint8_t r_mickey = 0;
  uint8_t r_swing = 0;
  uint16_t jerry_to_amount = 0;
  uint8_t jerry_reached = 0; // 0, 1, 2
  bool save_stop_idx = false;
  bool dir_possible_change = false;

  int total_unit = 8;
  int vol_per_unit = 16;
  int time_reverse_case = 0;
  case_t ft_up = 0;
  case_t ft_dn = 0;
  case_t tjm_cno = 0;
  time_t remained_cat_timer = 0;
  time_t new_dog_timer = 0;
  int64_t idx__ = 0;
  int64_t idx_tm_ = 0;
  time_t t_now_ = 0;
  double hp_now_ = 0;
  double revenue = 0;
  case_t deal_times = 0;

private:
  TICK_DATA tick;
  Lines lines;
  Wave wave;
  StageDirs dirs;
  WaitingForWrong wfw;
  Dir5 d5;
  EnergyMark em_up;
  EnergyMark em_dn;
  PossibleDirChange pdc;
  EnergyDir edir;
  Schrodinger Tom;
  Schrodinger Jerry;
  Schrodinger mickey;
  Swing swing;

  DirDreamer dd_bd;
  DirDreamer dd_pdc;
  DirDreamer dd_em;
  DirDreamer dd_ft;
  DirDreamer dd_ced;
  DirDreamer dd_tts;
  DirDreamer dd_sd;

  Turns turns;

public:
  Mock mock;

  Pricing pricing_open;
  Pricing pricing_close;
  TheDealVector tdv;
  DirVector dv_get_dir;

private:
  //
  // Debug
  //
#if (STEP_BY_STEP)
  int64_t step_by_step() noexcept;
#endif // STEP_BY_STEP

  Pricing* get_pricing_(const bool is_open) noexcept
  {
    return (is_open) ? &pricing_open : &pricing_close;
  }

private:
  //
  // trade strategy
  //

  bool good_params() noexcept;
  void recall_all_orders_();
  int lines_changed_() noexcept;
  int check_position__(const dir_t d) noexcept;

  uint8_t dirs_was_changed__() noexcept;
  int event_d5_energy_was_changed_(const uint8_t changed,
                                   const bool em_up_changed,
                                   const bool em_dn_changed) noexcept;

private:
  //
  // dt_xx__ function group: calculate the cases of direction and time
  //
  uint8_t dt_good_timing(const dir_t d) noexcept;
  uint8_t dt_perfect_timing(const dir_t d) noexcept;
  bool dt_dir_same_spp_dir__(const dir_t d) const noexcept;
  bool dt_dir_nice_spp_dir__(const dir_t d) noexcept;
  bool dt_dir_same_wave_dir__(const dir_t d) noexcept;
  bool dt_dir_same_g2_dir__(const dir_t d) noexcept;
  bool dt_dir_g2_oppo_dir__(const dir_t d) noexcept;
  bool dt_dir_is_large_reflection__(const dir_t d) noexcept;
  bool dt_dir_same_mid_ein_dir__(const dir_t d) const noexcept;
  bool dt_dir_nice_region__(const dir_t d) noexcept;
  bool dt_dir_first_goto_dir__(const dir_t d) noexcept;
  bool dt_dir_first_dir_with_good_tm__(const dir_t d) noexcept;
  bool dt_dir_weak_same_but_long_time__(const dir_t d) noexcept;
  bool dt_dir_same_spot_without_spp__(const dir_t d) noexcept;
  bool dt_time_two_g2_first_to__(const dir_t d) noexcept;
  bool dt_time_two_g2_first_good__(const dir_t d) noexcept;
  bool dt_dir_vss_to_same_wave__(const dir_t d) noexcept;
  bool dt_dir_min_ein_opposite__(const dir_t d) noexcept;
  bool dt_dir_g2_to__(const dir_t d) noexcept;
  bool dt_dir_shapes_are_same__(const dir_t d) noexcept;
  bool dt_time_two_spp_to__(const dir_t d) const noexcept;
  bool dt_time_vss_spp_to_but_work__() const noexcept;
  bool dt_time_g2_less_spp__(const dir_t d) noexcept;
  bool dt_dir_solved_one_spp__(const dir_t d) noexcept;
  bool dt_time_solved_one_spp__(const dir_t d) noexcept;
  bool dt_dir_energe_without_spp__(const dir_t d) noexcept;
  bool dt_dir_sure_cls_spp_dir__(const dir_t d) const noexcept;
  bool dt_dir_pdc_cur_goto__(const dir_t d) const noexcept;
  bool dt_dir_mid_zero_other_same__(const dir_t d) const noexcept;
  bool dt_dir_pdc_going_to_reverse__(const dir_t d) const noexcept;
  bool dt_dir_one_spp_solved_dir__(const dir_t d) const noexcept;
  bool dt_dir_diff_mid_ein_same_spp__(const dir_t d) const noexcept;
  bool dt_dir_mid_ein_oppo_but_g2_to__(const dir_t d) const noexcept;
  bool dt_dir_diff_cls_ein_g2_2type__(const dir_t d) const noexcept;
  bool dt_dir_one_spp_and_first_and_g2__(const dir_t d) const noexcept;
  bool dt_dir_zzzz_g2_and_first__(const dir_t d) const noexcept;
  bool dt_dir_zzzz_and_no_g2__(const dir_t d) const noexcept;
  bool dt_dir_zzzz_and_gz__() const noexcept;
  case_t dt_very_good_timing__(const dir_t d) noexcept;
  bool tjm_solved_spp_dir_with_timer(const dir_t d) noexcept;

  dir_t get_probability_dir_() const noexcept;
  dir_t get_no_mid_ein_dir_() const noexcept;

  case_t dir_zz_or_zzzz__(const dir_t d) const noexcept;

  bool calculate_energy__(const dir_t d, EnergyMark& em) noexcept;

  EnergyMark& get_energy_mark(const dir_t d) noexcept
  {
    return (d > 0) ? em_up : em_dn;
  }

private:
  time_t weak_timer___(const dir_t d) noexcept;
  time_t strong_timer___(const dir_t d) noexcept;
  time_t get_weak_timer___(const dir_t d) noexcept;
  time_t get_strong_timer___(const dir_t d) noexcept;
  bool its_good_timing_for_dir__(const dir_t d, const time_t tm) noexcept;
  bool intuitive_wave_risk__() const noexcept;

private:
  int to_min(const time_t tm) noexcept
  {
    return (tm ? int((tm - t_now_) / 60) : 0);
  }

  int8_t check_energy_dir() noexcept;

private:
  int fight_mickey_mouse_reached(const uint8_t reached) noexcept;

  int8_t parse_signal(const char* line, time_t& tm_signal) noexcept;

  int check_zzzz() noexcept;

private:
  int swing_energy_battle(const event_t event) noexcept;
  int swing_no_pet_(const event_t event) noexcept;

private:
  int reached(event_t& event) noexcept;
  int test_dirs(event_t& event) noexcept;
  int hited_tom_open_or_stop_price(event_t& event) noexcept;
  int energy_battle(event_t& event, const time_t tm_signal) noexcept;
  int time_battle(event_t& event) noexcept;
  int fighting(const event_t event) noexcept;
  int fighting_(const event_t event) noexcept;
  uint16_t basic_data_changed(const int8_t signal_probe,
                              const time_t tm_signal) noexcept;

#if (ONLINE_DATA_COMM)
private:
  int send_signal_energy_dir_changed() noexcept;
  int send_signal_pets_was_changed() noexcept;
#else
  int parse_signal_line(const char* line,
                        event_t& event,
                        int8_t& signal_probe,
                        bool& start_trade,
                        int8_t& test_signal,
                        time_t& tm_signal) noexcept;
#endif // ONLINE_DATA_COMM

public:
  DealActionVec dav;

  case_t fight_the_side_(const dir_t d, const bool is_opposite) noexcept;
  bool fight_now_is_swing_() noexcept;
  dir_t fight_strong_compound_energy_dir() noexcept;
  dir_t big_dir_swing_to_dir_() noexcept;
  int get_first_ced_duration__() noexcept;
  int big_dir_long_long_time__() noexcept;
  Lines& fight_get_the_lines__() noexcept;

  time_t tjm_retrive_timer(const dir_t d) noexcept;
  uint8_t tjm_check_space_level(const dir_t d) noexcept;
  bool tjm_large_time_with_swing(const dir_t d) const noexcept;
  bool tjm_good_entry_region(const dir_t d) noexcept;
  bool tjm_good_line_region(const dir_t d) noexcept;

  bool tjm_is_large_turn(const dir_t d) noexcept;

  uint8_t tjm_tom_dont_need_reached_line2(const dir_t d) noexcept;
  uint8_t tjm_tom_should_stop(const dir_t d) noexcept;
  uint8_t tjm_huge_inflection(const dir_t d) noexcept;
  dir_t tjm_is_break_2lines() noexcept;
  bool tjm_bad_turns();
  uint16_t tjm_should_stop_jerry_as_good(const dir_t d) noexcept;

  bool tjm_confused_turn_timer(const dir_t d) noexcept;
  void save_all_deal() noexcept;

  int check_ced_change(event_t& event) noexcept;

  bool tjm_space_is_good(const dir_t d) const noexcept;
  STRONG_LEVEL tjm_retive_strong_level(const dir_t d, case_t& cno) noexcept;

  bool tjm_should_reache_line2(const dir_t d) noexcept;
  case_t tjm_should_run_jerry() noexcept;
  case_t tjm_tom_should_stop_or_reverse(const dir_t d) noexcept;

  int tjm_jerry_jerry(const dir_t d) noexcept;
  case_t tjm_check_ambiguous_dirs(const dir_t timer_dir,
                                  const dir_t space_dir,
                                  const case_t oppo_cno) noexcept;
  case_t tjm_timer_dir_is_opposite(const dir_t timer_dir) noexcept;
  int tjm_check_prerequisite(const dir_t deal_dir) noexcept;

  case_t tjm_tom_reached_limit() noexcept;
  case_t tjm_tom_reached_limit_lines(const uint8_t tom_reached) noexcept;
  case_t tjm_tom_reached_opposite_limit_lines(
    const uint8_t tom_reached) noexcept;
  int tjm_tom_reached_close_price(const event_t event) noexcept;
  int tjm_tom_reached_open_price(const event_t event) noexcept;
  int tjm_jerry_reached(const uint8_t event) noexcept;
  uint8_t tjm_tom_reached_something() noexcept;

  int tjm_framework(const event_t event) noexcept;
  int tjm_hited_something(const event_t event) noexcept;
  int tjm_make_deal(const event_t event) noexcept;
  int tjm_do_pet_actions() noexcept;

  // Set Stop, Run, Reverse
  int tjm_add_action_set_stop(const case_t cno) noexcept;
  int tjm_add_action_set_reverse(const case_t cno) noexcept;
  int tjm_add_action_jerry_run(const dir_t d, const case_t cno) noexcept;
  int tjm_add_action_jerry_stop(const case_t cno) noexcept;

  int tjm_add_action_tom_stop(const case_t cno) noexcept;
  int tjm_add_action_tom_run(const dir_t d,
                             const case_t cno,
                             const double hp_base,
                             const case_t hp_base_cno,
                             bool b_set_forced_stop) noexcept;
  int tjm_add_action_tom_run(const dir_t d, const DealCasePtr& dc) noexcept;
  int tjm_add_action_mickey_run(const dir_t d, const case_t cno) noexcept;
  int tjm_add_action_mickey_stop(const case_t cno) noexcept;
  // Set Close-Price
  int tjm_try_set_close_price(const dir_t d) noexcept;
  int tjm_add_action_set_good_to_win(const case_t cno) noexcept;
  int tjm_add_action_set_stop_loss(const case_t cno) noexcept;
  int tjm_add_action_set_min_to_win(const case_t cno) noexcept;
  int tjm_add_action_set_stop_win(const case_t cno) noexcept;
  int tjm_add_action_cancel_seted_close(const case_t cno,
                                        const closed_t close_type) noexcept;
  int tjm_add_action_close_part(const ClosePrice* const close_price) noexcept;
  int tjm_add_action_close_all(const ClosePrice* const close_price) noexcept;
  bool tjm_get_entry_price(const dir_t deal_dir, double& hp_base) noexcept;

  // Excute Actions
  int tjm_action_tom_run(const dir_t d, const DealCasePtr& dc) noexcept;
  int tjm_action_tom_stop(const DealCasePtr& dc) noexcept;
  int tjm_action_jerry_run(const dir_t d, const DealCasePtr& dc) noexcept;
  int tjm_action_jerry_stop(const DealCasePtr& dc) noexcept;
  void tjm_jerry_reset() noexcept;
  int tjm_action_mickey_run(const dir_t d, const DealCasePtr& dc) noexcept;
  int tjm_action_mickey_stop(const DealCasePtr& dc) noexcept;
  int tjm_action_tom_open(const dir_t d, const double hpx) noexcept;
  int tjm_action_set_close_all(const DealCasePtr& dc) noexcept;
  int tjm_action_set_close_part(const DealCasePtr& dc) noexcept;
  int tjm_action_close_part(const DealCasePtr& dc) noexcept;
  int tjm_action_close_all(const DealCasePtr& dc) noexcept;
  int tjm_action_opened(const DealCasePtr& dc) noexcept;
  int tjm_action_cancel_seted_close(const DealCasePtr& dc) noexcept;

  int tjm_run_jerry(const case_t cno) noexcept;

  dir_t tjm_good_dir_case(case_t& good_case_no) noexcept;

  bool tjm_get_entry_price_from_lines(const dir_t deal_dir,
                                      const Lines& lines,
                                      double& hp_base) noexcept;
  DIR_TYPE tjm_estimate_dir(case_t& case_no) noexcept;
  bool tjm_estimate_tolerate() noexcept;
  bool tjm_good_entry_price(const dir_t d,
                            double& hp_base,
                            case_t& case_no) noexcept;
  DIR_TYPE tjm_ced_is_going_to(const dir_t d) noexcept;
  DIR_TYPE tjm_force_is_going_to(const dir_t d) noexcept;
  DIR_TYPE tjm_swing_is_going_to(const dir_t d) noexcept;
  DIR_TYPE tjm_big_dir_is_going_to(const dir_t d) noexcept;

  bool tjm_cal_close_forced(const case_t cno,
                            const case_t hp_base_cno,
                            const bool just_run,
                            const ClosePricePtr& close_price) noexcept;
  bool tjm_cal_close_for_good_to_win(const case_t cno,
                                     const ClosePricePtr& close_price) noexcept;
  bool tjm_cal_close_for_min_to_win(const case_t cno,
                                    const ClosePricePtr& close_price) noexcept;
  bool tjm_cal_close_for_stop_win(const case_t cno,
                                  const ClosePricePtr& close_price) noexcept;
};

extern MakeDeal md;
extern double g_big_loss;

#define TIMER(tm) ((tm) + t_now_)

#if (SAVE_STOP_IDX_TO_FILE)
extern bool
init_save_stop_file() noexcept;
extern bool
save_stop_idx_to_file(const int64_t idx,
                      const double hp_delta,
                      const dir_t dir,
                      const int t[4],
                      const dir_t bd,
                      const bool swing,
                      const dir_t sdir,
                      const double hpx,
                      const Lines& l) noexcept;
extern void
close_save_stop_file() noexcept;
#endif // SAVE_STOP_IDX_TO_FILE

} // namespace Trade

extern double g_hop;
