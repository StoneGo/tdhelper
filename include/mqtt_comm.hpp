#pragma once
#include <pthread.h>
#include <cstdint>
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <unistd.h>

#ifndef DEFINE_STRUCT_CONFIG
#define DEFINE_STRUCT_CONFIG

#define CONFIG_STR_LENGHT 64

#ifndef MAX_PATH
#define MAX_PATH 512
#endif // MAX_PATH

struct FbConfig {
  bool udp;
  int udp_port;
  int data_index;
  int scale_len;
  int64_t espos;
  int64_t tspos;
  int64_t spos;
  int64_t epos;
  int64_t min_stop_pos;
  double hop;
  double float_hp;
  char *udp_ip;
  char *data_path;
  char *inst;
  char *instrument;
  char *mqtt_broker;
  char *mqtt_clientid;
  char *mqtt_sub_market_data;
  char *mqtt_sub_future_data;
  char *mqtt_sub_day_signal;
  char *mqtt_sub_week_signal;
  char *mqtt_sub_order;
  char *mqtt_sub_position;
  char *mqtt_pub_trend;
  char *mqtt_pub_order;
  char *trade_logpath;
  char *signal_logpath;
  char *candle_port;

  FbConfig() { memset((void *)(this), 0, sizeof(FbConfig)); }
  ~FbConfig() {
    free(udp_ip);
    free(data_path);
    free(inst);
    free(instrument);
    free(mqtt_broker);
    free(mqtt_clientid);
    free(mqtt_sub_market_data);
    free(mqtt_sub_future_data);
    free(mqtt_sub_day_signal);
    free(mqtt_sub_week_signal);
    free(mqtt_sub_order);
    free(mqtt_sub_position);
    free(mqtt_pub_trend);
    free(mqtt_pub_order);
    free(trade_logpath);
    free(signal_logpath);
    free(candle_port);
  }
};

// extern FbConfig *get_config();
// extern int read_config(const char *config_filpath, FbConfig &config);

#endif // DEFINE_STRUCT_CONFIG

extern void set_mqtt_ready();
extern int mqtt_init();
extern int mqtt_release();
extern int mqtt_send_message(const char* msg);

// extern int mqtt_pub_bit_direction(const char* inst, bool master, int dir,
//                                   int64_t pos);
// extern int mqtt_pub_trend(const char* msg);
// extern int mqtt_pub_trading_command(const char *msg);
// extern int mqtt_pub_trade_info(const char *msg);
//
// extern int open_market(const int vol, const int dir);
// extern int close_market(const int vol, const int dir);
// extern int open_post_only(const double price, const int vol, const int dir);
// extern int close_post_only(const double price, const int vol, const int dir);
// extern int cancle_order_by_id(const char *clid);
