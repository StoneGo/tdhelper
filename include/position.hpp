#pragma once
#include <stdint.h>
#include <memory.h>

using dir_t = int8_t;

namespace Trade {

struct Position
{
  dir_t d = 0;
  int vol = 0;
  double avg_hpx = 0;
  double max_hop_win = 0;
  double hp_max_win = 0;     // price of max-win
  double max_hop_lost = 0;   // max-lost
  double max_lost_hp = 0;    // price of max-lost
  double float_hop_rvn = 0;  // float revnue in hop
  double float_revenue = 0;  // float revnue in $ = float_hop_rvn * vol - commision

  void print(const double hpx, const int64_t idx) noexcept;
  void update_float_revenue(const double hpx) noexcept;
  void reset() noexcept { memset((void*)(this), 0, sizeof(Position)); }
  bool reached_max_win_loss(const double hpx) noexcept;
  bool force_stop_win(const double hpx) noexcept;
  bool force_stop_loss(const double hpx) noexcept;
};

} // namespace Trade