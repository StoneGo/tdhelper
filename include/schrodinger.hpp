#pragma once
#include "energy.hpp"
#include "lines.hpp"
#include "mockif.hpp"
#include "spacetime.hpp"
#include "wave.hpp"
#include "wfw.hpp"
#include <cstdint>
#include <cstdio>
#include <memory>
#include <vector>

using case_t = uint32_t;

namespace Trade {

enum class CALCULATE_CLOSE_TYPE : uint8_t
{
  NONE = 0,
  FORCED = 1,
  ASAP = 2,
  NOT_BAD = 3,
  GOOD_TO_WIN = 4,
  LINE_1 = 5,
  LINE_2 = 6,
  PARTIALLY = 7,
};

enum HITED_CLOSE_TYPE : uint8_t
{
  HITED_CLOSE_NONE = 0,
  HITED_CLOSE_PARTIALLY = 1,
  HITED_CLOSE_ALL = 2,
};

struct OpenPrice
{
  dir_t d = 0;
  case_t cno = 0;
  bool good_for_now = false;
  double hp_base = 0;

  OpenPrice(const dir_t dir,
            const case_t case_no,
            const bool good,
            const double hpx) noexcept
    : d(dir)
    , cno(case_no)
    , good_for_now(good)
    , hp_base(hpx)
  {
  }
};

using OpenPricePtr = std::shared_ptr<OpenPrice>;
using OpenPriceVector = std::vector<OpenPricePtr>;

enum CLOSE_PRICE_TYPE : uint8_t
{
  CLOSE_PRICE_NONE = 0,
  CLOSE_PRICE_MIN_WIN = 1,     // CLOSE ALL of position
  CLOSE_PRICE_STOP_WIN = 2,    // CLOSE ALL
  CLOSE_PRICE_STOP_LOSS = 4,   // CLOSE ALL
  CLOSE_PRICE_GOOD_TO_WIN = 8, // CLOSE PART of position
};

using closed_t = uint8_t;

struct ClosePrice
{
  closed_t hited_close_type = HITED_CLOSE_NONE;
  closed_t hited_price_type = 0;
  closed_t set_these_prices = 0;

  double rvn_min_win = 0;
  double rvn_stop_win = 0;
  double rvn_stop_loss = 0;
  double rvn_good_to_win = 0;

  double rchd_min_win = 0;
  double rchd_stop_win = 0;
  double rchd_stop_loss = 0;
  double rchd_good_to_win = 0;

  double closed_rvn = 0;

  ClosePrice() = default;
  ClosePrice(const ClosePrice* const cp)
  {
    memcpy((void*)(this), cp, sizeof(ClosePrice));
  }

  bool set_good_to_win(const double good_to_win, const bool preset) noexcept;
  bool set_stop_loss(const double stop_loss, const bool preset) noexcept;
  bool set_stop_win(const double stop_win, const bool preset) noexcept;
  bool set_min_win(const double min_win, const bool preset) noexcept;

  bool set_prices(ClosePrice* const close_price) noexcept;

  void clear() noexcept;
  closed_t reached_stop_price(const double rvn) noexcept;
  // stop or reverse
  void print(const double hp_now) noexcept;
  void cancel(const closed_t cancel_close) noexcept;
  void cancel_part(const closed_t cancel_close) noexcept;
  void cancel_all(const closed_t cancel_close) noexcept;
};

using ClosePricePtr = std::shared_ptr<ClosePrice>;
using ClosePriceVector = std::vector<ClosePricePtr>;

enum class TURN_TIME_OUT_TYPE : uint8_t
{
  NONE = 0,
  WEAK = 1,
  STRONG = 2,
};

struct DealCase
{
  dir_t dir = 0;
  case_t case_no = 0;
  closed_t cancel_close = 0;
  OpenPricePtr open;
  ClosePricePtr close;

  DealCase() = default;
  DealCase(const dir_t d, const case_t cno)
    : dir(d)
    , case_no(cno)
  {
  }
  ~DealCase() {}

  void reset() noexcept
  {
    dir = 0;
    case_no = 0;
    cancel_close = 0;
    open.reset();
    close.reset();
  }

  void print() const noexcept
  {
    printf("Going To %d, case_no %u\n", dir, case_no);
  }

  const DealCase& operator=(const DealCase& dc) noexcept
  {
    dir = dc.dir;
    case_no = dc.case_no;
    cancel_close = dc.cancel_close;
    open = dc.open;
    close = dc.close;

    return dc;
  }
};

using DealCasePtr = std::shared_ptr<DealCase>;
using DealCaseVector = std::vector<DealCasePtr>;

struct Dir5
{
  dir_t wave = 0;
  dir_t ext = 0;
  dir_t mid = 0;
  dir_t ein = 0;
  dir_t g2 = 0;

  bool reversed(const Dir5& d5) noexcept
  {
    return (wave = -d5.wave && ext == -d5.ext && mid == -d5.mid &&
                   ein == -d5.ein && g2 == -d5.g2);
  }
};

enum class PET_NAME : uint8_t
{
  NONE = 0,
  TOM = 1,
  JERRY = 2,
  MICKEY = 3,
};

enum DIR_FACTOR_ITEM : uint32_t
{
  DFI_NONE = 0x00000000,
  DFI_G2_BOTH_DIR = 0x00000001,
  DFI_G2_HP_DELTA = 0x00000002,
  DFI_G2_DIR = 0x00000004,
  DFI_WFW_CLS_DIR = 0x00000008,
  DFI_WFW_VSS_DIR = 0x00000010,
  DFI_PDC_CONFUSED = 0x00000020,
  DFI_PDC_CURRENT_OPPOSITE = 0x00000040,
  DFI_PDC_CURRENT_SAMESIDE = 0x00000080,
  DFI_PDC_POSIBLE_OPPOSITE = 0x00000100,
  DFI_PDC_POSIBLE_SAMESIDE = 0x00000200,
  DFI_MID_DIR = 0x000000400,
  DFI_VSS_DIR = 0x000000800,
  DFI_SPP_CLS_DIR = 0x00001000,
  DFI_SPP_VSS_DIR = 0x00002000,
};

struct Incomming
{
  dir_t d_big_reverse = 0;
  bool im_needle = 0;
  bool im_big_needle = 0;
  int64_t idx_rvn_max = 0;
  int64_t idx_rvn_min = 0;
  time_t tm_rvn_max = 0;
  time_t tm_rvn_min = 0;
  int64_t count_win = 0;
  int64_t count_loss = 0;
  double rvn_max = 0;
  double hp_rvn_max = 0;
  double rvn_min = 0;
  double hp_rvn_min = 0;
  double rvn_total = 0;
  double rvn_single = 0;
  double hpx_average = 0;
  double max_win_rvn = 0;
  double max_loss_rvn = 0;

  double get_max_rvn_delta() const noexcept;
  void update_revenue(const double hp_now,
                      const dir_t dir,
                      const double avg_px,
                      const int8_t amount,
                      const time_t tm,
                      const int64_t idx) noexcept;
  double max_slope() const noexcept;
  double amp() const noexcept;
  bool big_reverse(const double hp_now, const time_t ts) noexcept;
  void copy(const Incomming* in)
  {
    memcpy((void*)(this), in, sizeof(Incomming));
  }

  void reset() noexcept { memset((void*)(this), 0, sizeof(Incomming)); }
  double try_rvn(const double hpx, const dir_t d) const noexcept
  {
    return ((hpx - hpx_average) * d);
  }
};

struct MarkPrice
{
  int8_t reached = false;
  double px = 0;

  void mark_price(const double hpx) noexcept
  {
    px = hpx;
    reached = (hpx <= 0) ? -1 : 0;
  }
};

using MarkPriceArray = MarkPrice[4];

struct OpenSea
{
  static const int8_t MAX_VOLUEM = 2;
  bool good_for_now = false;
  int64_t my_idx = 0;
  time_t my_tm = 0;
  int8_t amount_reached = 0;
  int8_t amount = 0;
  int max_amount = 0;
  double hp_base = 0;
  double average = 0;
  double hp_cur_open = 0;
  MarkPriceArray mpa;
  void init_open_sea(const int64_t idx, const time_t tm_now) noexcept;
  void add_price(const double hpx) noexcept;
  void print(const double hp_now) noexcept;
  void update_open_prices(const double hpx,
                          const dir_t d,
                          const bool good_for_now) noexcept;
  void reset_lines(const int max_amount) noexcept
  {
    amount = amount_reached = 0;
    if (max_amount > 4)
      this->max_amount = 4;
    else
      this->max_amount = max_amount;

    average = 0;
    mpa[0].reached = mpa[1].reached = mpa[2].reached = mpa[3].reached = -1;
  }
};

struct Pet
{
protected:
  dir_t dir = 0;

public:
  bool outed = false; // timer outed1
  bool waiting_for_line2 = false;
  case_t case_no = 0;
  int whoami = 0;
  int amout_to = 0;
  int64_t my_idx = 0;
  time_t my_tm = 0;
  Spacetime target;

  Dir5 d5;
  EnergyMark em;

public:
  dir_t get_dir() const noexcept { return (dir) ? dir : 0; }
  bool is_running() const noexcept { return (dir != 0); }

  void reset() noexcept { memset((void*)(this), 0, sizeof(Pet)); }
  void copy_from(const Pet* sdg) noexcept
  {
    memcpy((void*)(this), sdg, sizeof(Pet));
  }
  void print(const char* title,
             const int64_t idx,
             const double hp_now,
             const time_t t_now) noexcept;
  bool set_timer(const time_t tm, const time_t tm2) noexcept;
  bool update_lines(const Lines& l, const double hp_now) noexcept;
  Timer* get_timer() noexcept;
  Lines& get_limits_lines() noexcept;
  Spacetime* get_target() noexcept;

  uint8_t check_reached(const time_t t_now,
                        const double hp_now,
                        Lines& l_now,
                        const int64_t idx) noexcept;

  dir_t opposite() noexcept { return (-dir); }
  uint8_t reacheded() const noexcept;
  void clear_reacheded() noexcept;
  bool reached_line2() noexcept;
  bool reached_line1() noexcept;
  bool reached_oppo_line2() noexcept;
  bool reached_oppo_line1() noexcept;
  bool timer_outed() noexcept;
  bool current_timer_outed() noexcept;
  void update_timer(const time_t tm, const time_t tm2) noexcept;
  void erase_opposite_reached(const uint8_t rchd) noexcept;

  void run(const dir_t d,
           const time_t tm,
           const time_t tm2,
           const Lines& lines,
           const double hp_now,
           const time_t t_now,
           const int64_t idx,
           const Dir5& d5,
           const EnergyMark& emark,
           const int whoami) noexcept;
  void stop(const int64_t idx, const int64_t tm, const uint32_t cno) noexcept;

  void set_timer_outed() noexcept { outed = true; }
  bool d5_was_reversed(const Dir5& d5) noexcept;
  void update_g2(const WaitingForWrong& wfw, const Wave& wave) noexcept;
  bool should_renew_lines(const Lines& l,
                          const time_t t_now,
                          const double hp_now) noexcept;
  void renew_target(const Lines& lines,
                    const time_t tm,
                    const time_t t_now,
                    const double hp_now,
                    const int64_t idx) noexcept;
#if (ONLINE_DATA_COMM)
  int get_json(char* buf, const int buf_len, const char* name);
#endif // ONLINE_DATA_COMM
};

#define TO_MINUTES(tm) ((tm) ? (int((tm)-t_now_) / 60) : 0)

struct Schrodinger : public Pet
{
public:
  // bool big_reverse_needle = false;
  // bool big_reverse = false;
  uint32_t closed_part_times = 0;
  bool closed_all = false;
  // uint8_t waiting_for_stop = 0; // 0 for none; 1 for stop; 2 for reverse
  double base_open_hpx = 0;
  Mock* mock = nullptr;
  ClosePrice close_price;
  DealCasePtr dc;
  Incomming incomming;
  OpenSea os_entries;

  void set_deal_interface(Mock* mock_interface) noexcept
  {
    mock = mock_interface;
  }

  void run(const dir_t d,
           const time_t tm,
           const time_t tm2,
           const Lines& lines,
           const double hp_now,
           const time_t t_now,
           const int64_t idx,
           const Dir5& d5,
           const EnergyMark& emark,
           const DealCasePtr& dc,
           const int whoami) noexcept
  {
    Pet::run(d, tm, tm2, lines, hp_now, t_now, idx, d5, em, whoami);

    closed_part_times = 0;
    closed_all = false;
    base_open_hpx = 0;
    mock = nullptr;
    memset(&close_price, 0, sizeof(ClosePrice));
    memset(&incomming, 0, sizeof(Incomming));
    this->dc = dc;
    os_entries.init_open_sea(idx, hp_now);
  }

  bool is_delt() const noexcept { return (os_entries.amount_reached > 0); }

  void stop(const int64_t idx, const int64_t tm, const uint32_t cno) noexcept
  {
    Pet::stop(idx, tm, cno);
    this->reset();
  }
  uint8_t reached(const time_t t_now,
                  const double hp_now,
                  Lines& l_now,
                  const int64_t idx) noexcept;
  uint8_t hit_open_price(const int64_t idx,
                         const time_t t_now,
                         const double hp_now) noexcept;

  uint8_t open_price(const int64_t idx,
                     const time_t t_now,
                     const double hp_now) noexcept;

  void reset() noexcept
  {
    Incomming in;
    in.copy(&incomming);
    memset((void*)(this), 0, sizeof(Schrodinger));
    incomming.copy(&in);
  }
  void copy_from(const Schrodinger* sdg) noexcept
  {
    memcpy((void*)(this), sdg, sizeof(Schrodinger));
  }

  void print_revenue(const double hp_now, const time_t t_now) noexcept;

  void print(const char* title,
             const int64_t idx,
             const double hp_now,
             const time_t t_now) noexcept;

  dir_t get_price_dir(const double hp_now) noexcept;
  bool second_timer_outed(const time_t t_now) noexcept;
  void set_open_entries(const double hp_base, const int vol) noexcept;
  int get_durantion(const time_t t_now) noexcept;
  const DealCasePtr& get_deal_case() const noexcept { return dc; }
};

} // namespace Trade
