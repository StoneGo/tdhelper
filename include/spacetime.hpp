#pragma once
#include "lines.hpp"
#include <cstdint>
#include <ctime>
#include <memory>


namespace Trade {

struct Timer
{
  bool to = false; // timer out
  bool in_range = false;
  bool fto = false;
  time_t t = 0;

  bool timer_out(const time_t t_now) noexcept;
  void set_timer(const time_t tm) noexcept;
  bool in_timer_range(const time_t t_now) noexcept;
  bool far_timer_out(const time_t t_now) noexcept;

  Timer() noexcept {}

  Timer(const time_t tm) noexcept
    : t(tm)
  {
  }

  void reset() noexcept
  {
    to = false;
    in_range = false;
    fto = false;
    t = 0;
  }
};

struct Spacetime
{
  uint8_t reached = 0;

  Lines lines;
  Timer timer;
  Timer timer_2;

  void print(const char* title,
             const int64_t idx,
             const double hp_now,
             const time_t t_now) noexcept;
  void set_timer(const time_t tm, const time_t tm2) noexcept;
  void set_lines(const Lines& l) noexcept;
  bool update_lines(const dir_t d,
                    const Lines& l,
                    const double hp_now) noexcept;
  uint8_t reached_line_and_timer(const double hp_now,
                                 const time_t t_now) noexcept;
  uint8_t check_reached(const time_t t_now,
                        const double hp_now,
                        Lines& l_now,
                        const int64_t idx) noexcept;
  bool timer_outed() noexcept;
  void clear_reacheded() noexcept;
  uint8_t reacheded() const noexcept;
  void reset() noexcept { memset((void*)(this), 0, sizeof(Spacetime)); }
  void update_timer(const time_t tm, const time_t tm2) noexcept;
  bool second_timer_outed(const time_t tm) noexcept;
  void erase_opposite_reached(const dir_t d, const uint8_t rchd) noexcept;
};

struct PriceTime
{
  case_t case_no = 0;
  time_t timer = 0;
  double hp = 0;
};

using PriceTimePtr = std::shared_ptr<PriceTime>;

} // namespace