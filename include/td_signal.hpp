#pragma once

#include "dirs.hpp"
#include "lines.hpp"
#include "rapidjson/document.h"
#include "tick.h"
#include "wave.hpp"
#include "wfw.hpp"
#include <memory>
#include <stdint.h>
#include <unordered_map>
#include <vector>

using Document = rapidjson::Document;

#ifndef THIS_IS_MY_SIGN_FUN
#define THIS_IS_MY_SIGN_FUN
template<typename T>
inline constexpr int8_t
sign(T val)
{
  return (T(0) < val) - (val < T(0));
}
#endif // THIS_IS_MY_SIGN_FUN

namespace Trade {

using StrVector = std::vector<char*>;
using StrVecPtr = std::shared_ptr<StrVector>;
using SignalMap = std::unordered_map<int64_t, StrVecPtr>;

extern int
read_signal_from_file(const char* filepath, const time_t stm) noexcept;

extern void
clear_map() noexcept;

extern void
output_map() noexcept;

extern StrVecPtr
find_strings(const int64_t pos) noexcept;

extern int
ParseLines(Document& doc, Lines& ls, const char* line) noexcept;
extern int
ParseTimerOut(TimerOutVector& tov, Document& doc, const char* line) noexcept;
extern int
ParseWave(Document& doc, Wave& w, const char* line) noexcept;
extern int
ParseDirs(Document& doc, StageDirs& sd, const char* line) noexcept;
extern int
ParseWfw(Document& doc, WaitingForWrong& wfw, const char* line) noexcept;
extern int
ParseTick(Document& doc, TICK_DATA& tick, const char* line) noexcept;

extern void
print_timer_out(const int64_t idx) noexcept;

extern SignalMap sm;

#if (!STEP_BY_STEP)
extern int64_t
step_by_step() noexcept;
#endif // !STEP_BY_STEP

extern const char* version;

} // namespace Tradeß