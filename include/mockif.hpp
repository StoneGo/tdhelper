#pragma once

#include "position.hpp"
#include <cstdint>
#include <stdint.h>
#include <list>
#include <memory>
#include <vector>

namespace Trade {

enum PRICING_TYPE : uint8_t
{
  PRICING_TYPE_NONE = 0,
  PRICING_TYPE_ASAP = 1,
  PRICING_TYPE_LINE_1 = 2,
  PRICING_TYPE_LINE_2 = 3,
  PRICING_TYPE_SPOT_1 = 4,
  PRICING_TYPE_SPOT_2 = 5,
  PRICING_TYPE_IMMEDIATELY = 6,
  PRICING_TYPE_LIMIT = 7,
};

struct Order
{
  dir_t d = 0;      // -1 short, 1 long
  PRICING_TYPE ptype = PRICING_TYPE_NONE;
  int vol = 0;      // volume
  int64_t idx = 0;  // index of data
  double hpx = 0;   // price in hop  = price / hop
  void reset() noexcept { memset((void*)(this), 0, sizeof(Order)); }
};

using OrderPtr = std::shared_ptr<Order>;
using OrderList = std::list<OrderPtr>;

struct Deal
{
  int action = 0; // 0 for open, 1 for close
  dir_t d = 0;
  PRICING_TYPE ptype = PRICING_TYPE_NONE;
  int vol = 0;          // volume of thid deal  , 2
  int pos_vol = 0;      // volume in position   , old vol + new vol
  int64_t idx = 0; 
  double hpx = 0;
  double avg_hpx = 0;   // average price in hop
  double rvn_hop = 0;   // reverue in hop, float
  double revenue = 0;   // close revenue , closed
  double rvn_max_win = 0;   // max-win-revenue
  time_t tm_max_win = 0;
  int64_t idx_max_win = 0;
  double rvn_max_lost = 0;  // max-lost
  time_t tm_max_loss = 0;
  int64_t idx_max_loss = 0;

  void print() noexcept;
  void reset() noexcept { memset((void*)(this), 0, sizeof(Deal)); }
};

using DealPtr = std::shared_ptr<Deal>;
using DealVector = std::vector<DealPtr>;

struct Revenue
{
  double float_up = 0;     // float revenue of Long positions
  double float_dn = 0;     // float revenue of short positions
  double float_rvn = 0;    // all float revune
  double close_up = 0;     // closed
  double close_dn = 0;
  double close_rvn = 0;
  double all = 0;          // all revnue
  int64_t idx = 0;         // current index of data

  void print(const int64_t idx) noexcept;
  void reset() noexcept { memset((void*)(this), 0, sizeof(Revenue)); }
};


// mock interface to Exchange
class Mock
{
public:
  //
  // mock interface
  //
  OrderList oo_up;   // open-order for long
  OrderList oo_dn;   // open-order for short
  OrderList co_up;   // close-order for long
  OrderList co_dn;   // close-order for short

  DealVector dv;     // Recodes of all deal
  Revenue revenue;   // Real-time revenue

  Position pos_up;   // Long-Position
  Position pos_dn;   // Short-Position

public:
  //
  // mock interface
  //
  int mock_deal(const int64_t idx,
                const double hpx,
                const double ftime,
                int v[4]) noexcept;

  int mock_try_open(const dir_t d,
                    const double hpx,
                    const double ftime,
                    const int64_t idx) noexcept;

  int mock_try_close(const dir_t d,
                     const double hpx,
                     const double ftime,
                     const int64_t idx) noexcept;

  int mock_place_open_order(const dir_t d,
                            const double hpx,
                            const int vol,
                            const PRICING_TYPE ptpye,
                            const int64_t idx) noexcept;

  int mock_place_close_order(const dir_t d,
                             const double hpx,
                             const int vol,
                             const PRICING_TYPE ptpye,
                             const int64_t idx) noexcept;

  /// return amount of orders
  int mock_query_open_order(const dir_t d, const int64_t idx) noexcept;
  /// return amount of orders
  int mock_query_close_order(const dir_t d, const int64_t idx) noexcept;
  int mock_recall_all_open_order(const dir_t d, const int64_t idx) noexcept;
  int mock_recall_all_close_order(const dir_t d, const int64_t idx) noexcept;

  void mock_update_float_revenue(const double hpx) noexcept;

  bool mock_query_position(const dir_t d, int* vol, double* avg_hpx) noexcept;
  bool mock_query_revenue(const dir_t d,
                          double& float_revenue,
                          double& close_revenue) noexcept;
  Position& mock_got_position(const dir_t d) noexcept
  {
    return (d > 0) ? pos_up : pos_dn;
  }

  void mock_print_orders_() noexcept;

  Position* mock_get_position_(const dir_t d) noexcept
  {
    return (d > 0) ? &pos_up : &pos_dn;
  }
  OrderList* mock_get_order_list_(const dir_t d, const bool is_open) noexcept
  {
    if (is_open) {
      return (d > 0) ? &oo_up : &oo_dn;
    } else {
      return (d > 0) ? &co_up : &co_dn;
    }
  }

  void mock_show_deal_record() noexcept;
};

} // namespace Trade