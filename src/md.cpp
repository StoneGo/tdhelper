#include "dirs.hpp"
#include "energy.hpp"
#include "lines.hpp"
#include "make_deal.hpp"
#include "turn.hpp"
#if (ONLINE_DATA_COMM)
#include "mqtt_comm.hpp"
#endif // ONLINE_DATA_COMM
#include "position.hpp"
#include "schrodinger.hpp"
#include "td_signal.hpp"
#include "wfw.hpp"
#include <cstdint>
#include <cstdio>
#include <ctime>

#if (STONE_DEBUG || STEP_BY_STEP)
extern int64_t g_stop_idx;
extern int64_t g_start_idx;
#endif

namespace Trade {

bool
MakeDeal::dt_dir_same_spot_without_spp__(const dir_t d) noexcept
{
  bool yes =
    (!wave.solved_cls_spp && !wave.solved_vss_spp && wave.spot_dir == d &&
     wfw.g2_dir == d && d5.ext == d && d5.wave == d);
  return yes;
}

bool
MakeDeal::dt_dir_same_spp_dir__(const dir_t d) const noexcept
{
  bool yes =
    (wave.have_cls_spp && wave.have_vss_spp &&
     wave.solved_cls_spp == wave.solved_vss_spp && wave.solved_cls_spp == d);
  return yes;
}

bool
MakeDeal::dt_time_two_spp_to__(const dir_t d) const noexcept
{
  bool yes = (wfw.cls_dir == d && wfw.vss_dir == d && wfw.tm_cls_far < t_now_ &&
              wfw.tm_vss_far < t_now_);
  return yes;
}

bool
MakeDeal::dt_time_vss_spp_to_but_work__() const noexcept
{
  bool yes = (wfw.tm_vss_near < t_now_ && wfw.tm_vss_far < t_now_ &&
              t_now_ - wfw.tm_vss_near < TWO_HOURS &&
              t_now_ - wfw.tm_vss_far < ONE_HALF_HOUR);
  return yes;
}

bool
MakeDeal::dt_dir_nice_spp_dir__(const dir_t d) noexcept
{
  bool yes =
    (wave.have_cls_spp && wave.have_vss_spp && wave.solved_cls_spp == d &&
     wave.solved_vss_spp != -d && wfw.vss_dir == d);
  return yes;
}
bool
MakeDeal::dt_dir_same_wave_dir__(const dir_t d) noexcept
{
  bool yes = (d5.wave == d && (wfw.fs_dir == d || wave.spot_dir == d) &&
              wave.have_cls_spp && wave.solved_cls_spp == d && d5.ext == d);
  return yes;
}
bool
MakeDeal::dt_dir_same_g2_dir__(const dir_t d) noexcept
{
  bool yes = (wfw.g2_dir == d && wfw.g2_dir == d &&
              wfw.g2_time - ONE_HALF_HOUR > t_now_);
  return yes;
}

// OLD: 30
#define MIN_G2_SPOT_DELTA 0

bool
MakeDeal::dt_dir_g2_oppo_dir__(const dir_t d) noexcept
{
  return (((wfw.g2_hpx - hp_now_) * d > MIN_G2_SPOT_DELTA) &&
          (wfw.g2_time - ONE_HALF_HOUR > t_now_));
}
bool
MakeDeal::dt_dir_is_large_reflection__(const dir_t d) noexcept
{
  // bool yes = ((d < 0 && lines.ext_rgn.top && !lines.ext_rgn.btm) ||
  //             (d > 0 && !lines.ext_rgn.top && lines.ext_rgn.btm));
  // return yes;
  bool yes = (d && bear_dir == d);
  return yes;
}
bool
MakeDeal::dt_dir_same_mid_ein_dir__(const dir_t d) const noexcept
{
  bool yes = (d == d5.mid && d == d5.ein);
  return yes;
}
bool
MakeDeal::dt_dir_nice_region__(const dir_t d) noexcept
{
  auto rgn = lines.region(hp_now_);
  bool yes = ((d > 0 && rgn != LINE_REGION::ABOVE_2) ||
              (d < 0 && rgn != LINE_REGION::BELOW_2));
  return yes;
}

bool
MakeDeal::dt_time_g2_less_spp__(const dir_t d) noexcept
{
  // 1. same dir in wfw
  // 2. g2 < vss or g2 < cls
  bool yes = (wfw.g2_dir == d && wfw.cls_dir == d && wfw.vss_dir == d &&
              ((wfw.g2_time < wfw.tm_vss_far && wfw.tm_vss_far > t_now_) ||
               (wfw.g2_time < wfw.tm_cls_far && wfw.tm_cls_far > t_now_)));
  return yes;
}
bool
MakeDeal::dt_time_two_g2_first_to__(const dir_t d) noexcept
{
  bool yes = (std::abs(wfw.g2_type) == 2 && wfw.g2_dir == -d &&
              (LESS_ONE_HALF(wfw.g2_time) || dt_time_g2_less_spp__(d)));
  return yes;
}

bool
MakeDeal::dt_time_two_g2_first_good__(const dir_t d) noexcept
{
  bool yes = (std::abs(wfw.g2_type) == 2 && LONGER_ONE_HALF(wfw.g2_time) &&
              wfw.g2_dir == d);
  return yes;
}

bool
MakeDeal::dt_dir_vss_to_same_wave__(const dir_t d) noexcept
{
  bool yes = (wave.solved_cls_spp == d && wave.spot_dir == d && d5.ext == d &&
              wave.have_vss_spp && wave.solved_vss_spp &&
              LESS_AN_HOUR(wfw.tm_vss_far) && LESS_TWO_HOURS(wfw.tm_vss_near) &&
              LONGER_30M(wfw.tm_cls_near) && LONGER_AN_HOUR(wfw.tm_cls_far));
  return yes;
}

bool
MakeDeal::dt_dir_min_ein_opposite__(const dir_t d) noexcept
{
  bool yes = false;

  if (dirs.ein_dir == -d && dirs.mid_dir == -d && wave.have_cls_spp &&
      wave.solved_cls_spp == d && wfw.cls_dir == d && wave.have_vss_spp &&
      wave.solved_vss_spp == d && wfw.vss_dir == d && wave.spot_dir == d &&
      d5.ext == d) {
    if ((wfw.g2_dir == d) || dt_dir_g2_oppo_dir__(d)) {
      yes = true;
    }
  }
  return yes;
}

bool
MakeDeal::dt_dir_first_goto_dir__(const dir_t d) noexcept
{
  bool yes = (dirs.big_dir == d && wave.spot_dir != -d && wave.have_cls_spp &&
              wave.solved_cls_spp == d);
  return yes;
}
bool
MakeDeal::dt_dir_first_dir_with_good_tm__(const dir_t d) noexcept
{
  bool yes = (dt_dir_first_goto_dir__(d) && wfw.tm_cls_far >= ONE_HALF_HOUR);
  return yes;
}

bool
MakeDeal::dt_dir_weak_same_but_long_time__(const dir_t d) noexcept
{
  bool yes = (wfw.g2_dir == d && wfw.cls_dir == d && wfw.vss_dir == d &&
              LONGER_4_HOURS(wfw.g2_time) && LONGER_ONE_HALF(wfw.tm_vss_far) &&
              LONGER_ONE_HALF(wfw.tm_cls_far));
  return yes;
}

bool
MakeDeal::dt_dir_g2_to__(const dir_t d) noexcept
{
  bool yes = false;
  if (wfw.g2_dir == wfw.vss_dir && wfw.g2_dir == wfw.cls_dir &&
      (wfw.g2_time < wfw.tm_vss_far || wfw.g2_time < wfw.tm_cls_far) &&
      wfw.g2_dir == d) {
    yes = true;
  }
  return yes;
}

bool
MakeDeal::dt_dir_shapes_are_same__(const dir_t d) noexcept
{
  bool yes = false;
  if (wfw.g2_dir == d && wfw.vss_dir == d && wfw.g2_dir == d &&
      wave.spot_dir == d && dirs.big_dir == d)
    yes = true;
  return yes;
}

bool
MakeDeal::dt_dir_solved_one_spp__(const dir_t d) noexcept
{
  bool yes = (((wave.have_cls_spp && wave.have_vss_spp && wfw.vss_dir == d &&
                wfw.cls_dir == d) &&
               (wave.solved_cls_spp == d || wave.solved_vss_spp == d)) &&
              dirs.mid_dir != -d && dirs.ein_dir != -d &&
              (wfw.g2_dir == d || dt_dir_g2_oppo_dir__(d)));
  return yes;
}

bool
MakeDeal::dt_time_solved_one_spp__(const dir_t d) noexcept
{
  bool yes = dt_dir_solved_one_spp__(d) &&
             ((wave.have_cls_spp && LONGER_AN_HOUR(wfw.tm_cls_near)) ||
              (wave.have_vss_spp && LONGER_AN_HOUR(wfw.tm_vss_far)));
  return yes;
}

bool
MakeDeal::dt_dir_energe_without_spp__(const dir_t d) noexcept
{
  bool yes = false;
  if (!wave.solved_cls_spp && !wave.solved_vss_spp) {
    if (d5.wave == d && d5.ext == d && wfw.fs_dir == d && wfw.fs_type == 1) {
      if (d5.mid == d && d5.ein == d) {
        if (wfw.g2_dir == d || dt_dir_g2_oppo_dir__(d)) {
          yes = true;
        }
      }
    }
  }
  return yes;
}

bool
MakeDeal::dt_dir_sure_cls_spp_dir__(const dir_t d) const noexcept
{
  bool yes = false;
  if (bear_dir == d &&
      (dt_dir_same_mid_ein_dir__(d) || dt_dir_pdc_cur_goto__(d)) &&
      dt_time_vss_spp_to_but_work__() &&
      (wave.have_cls_spp && wave.solved_cls_spp == d) &&
      (wave.have_vss_spp &&
       (wave.solved_vss_spp == d || wave.solved_vss_spp == -d)))
    yes = true;
  return yes;
}

bool
MakeDeal::dt_dir_pdc_cur_goto__(const dir_t d) const noexcept
{
  bool yes = false;
  if (pdc.mid.cur_dir == d && pdc.ein.cur_dir == d)
    yes = true;
  return yes;
}

bool
MakeDeal::dt_dir_mid_zero_other_same__(const dir_t d) const noexcept
{
  bool yes = false;
  if (!dirs.mid_dir && dirs.ein_dir == d && wave.have_cls_spp &&
      wave.have_vss_spp && wave.solved_cls_spp == d &&
      wave.solved_vss_spp == d && wave.spot_dir == d)
    yes = true;
  return yes;
}

bool
MakeDeal::dt_dir_pdc_going_to_reverse__(const dir_t d) const noexcept
{
  bool yes = false;
  if (pdc.mid.cur_dir == -d && pdc.ein.cur_dir == -d &&
      pdc.ein.change_to_dir != d && pdc.mid.change_to_dir != d)
    yes = true;
  return yes;
}

bool
MakeDeal::dt_dir_one_spp_solved_dir__(const dir_t d) const noexcept
{
  bool yes = false;
  if (wfw.tm_vss_far < t_now_ && wave.have_vss_spp && wfw.tm_cls_far > t_now_ &&
      wave.have_cls_spp && wave.solved_cls_spp == d && bear_dir == d) {
    if (wfw.g2_dir == d && LONGER_ONE_HALF(wfw.g2_time) && wave.spot_dir == d) {
      if ((dirs.mid_dir == d || dirs.ein_dir == d) ||
          (pdc.one_possible_to_dir(d))) {
        yes = true;
      }
    }
  }
  return yes;
}

bool
MakeDeal::dt_dir_diff_mid_ein_same_spp__(const dir_t d) const noexcept
{
  bool yes = false;
  if (dirs.mid_dir && d && (dirs.mid_dir == -dirs.ein_dir) &&
      (d5.wave == d && d5.ext == d)) {
    // mid vs ein
    if (wave.have_cls_spp && wave.have_vss_spp && d == wave.solved_cls_spp &&
        wave.solved_cls_spp == wave.solved_vss_spp) {
      if (LONGER_30M(wfw.tm_cls_near) && LONGER_30M(wfw.tm_vss_near)) {
        yes = true;
      } else if (wfw.tm_vss_far - t_now_ > -ONE_HALF_HOUR &&
                 LONGER_ONE_HALF(wfw.tm_cls_near)) {
        yes = true;
      }
    }
  }
  return yes;
}

bool
MakeDeal::dt_dir_mid_ein_oppo_but_g2_to__(const dir_t d) const noexcept
{
  bool yes = false;
  dir_t nd = -d;
  if (nd && dirs.mid_dir == nd && dirs.ein_dir == nd && wfw.g2_dir == nd) {
    if (d5.wave == -nd && d5.ext == -nd && wave.have_cls_spp &&
        wave.solved_cls_spp == -nd && wave.solved_vss_spp != nd) {
      if (wfw.g2_time < wfw.tm_cls_near && LONGER_30M(wfw.tm_cls_near) &&
          LESS_ONE_HALF(wfw.g2_time)) {
        yes = true;
      }
    }
  }
  return yes;
}

case_t
MakeDeal::dir_zz_or_zzzz__(const dir_t d) const noexcept
{
  if (!wave.solved_cls_spp && !wave.solved_vss_spp) {
    if (!wave.have_cls_spp && !wave.have_vss_spp) {
      return 2;
    } else {
      return 1;
    }
  }
  return (0);
}

bool
MakeDeal::dt_dir_diff_cls_ein_g2_2type__(const dir_t d) const noexcept
{
  bool yes = 0;
  if (d5.wave == d && d5.ext == d && (d5.mid == -d || d5.ein == -d) &&
      wave.solved_cls_spp == d && wave.solved_vss_spp == -d) {
    if (pdc.mid.cur_dir == d && pdc.ein.cur_dir == d) {
      if (LESS_30MIN(wfw.tm_vss_far) && LONGER_30M(wfw.tm_cls_near)) {
        if ((wfw.g2_dir == d) ||
            (wfw.g2_type == 2 && less_than(wfw.g2_time, t_now_, AN_HOUR * 3))) {
          yes = true;
        }
      }
    }
  }
  return yes;
}

bool
MakeDeal::dt_dir_one_spp_and_first_and_g2__(const dir_t d) const noexcept
{
  bool yes = false;
  if (((wave.have_cls_spp && wave.solved_cls_spp == d &&
        !wave.solved_vss_spp) ||
       (wave.have_vss_spp && wave.solved_vss_spp == d &&
        !wave.solved_cls_spp)) &&
      (wfw.g2_dir == d ||
       (((wfw.g2_hpx - hp_now_) * d > MIN_G2_SPOT_DELTA) &&
        (wfw.g2_time - MINUTES_30 > t_now_)) ||
       (std::abs(wfw.g2_type) == 2)) &&
      LONGER_AN_HOUR(wfw.g2_time) && (wave.spot_dir == d || wfw.fs_dir == d) &&
      (d5.wave == d && d5.ext == d))
    yes = true;
  return yes;
}

bool
MakeDeal::dt_dir_zzzz_g2_and_first__(const dir_t d) const noexcept
{
  bool yes = false;
  if (!wave.solved_cls_spp && !wave.solved_vss_spp &&
      (wave.spot_dir == d || wfw.fs_dir == d) &&
      (wfw.g2_dir == d || ((wfw.g2_hpx - hp_now_) * d > MIN_G2_SPOT_DELTA)) &&
      LONGER_AN_HOUR(wfw.g2_time) && d5.wave == d && d5.ext == d)
    yes = true;
  return yes;
}

bool
MakeDeal::dt_dir_zzzz_and_no_g2__(const dir_t d) const noexcept
{
  bool yes = false;
  if (!wave.solved_cls_spp && !wave.solved_vss_spp &&
      (wave.spot_dir == d && wfw.fs_dir == d) && !wfw.g2_dir)
    yes = true;
  return yes;
}

bool
MakeDeal::dt_dir_zzzz_and_gz__() const noexcept
{
  return (!wave.have_cls_spp && !wave.solved_cls_spp && !wave.have_vss_spp &&
          !wave.solved_vss_spp && !wfw.g2_dir);
}

int
MakeDeal::lines_changed_() noexcept
{
  int updated = 0;
  if (Tom.get_dir()) {
    if (Tom.update_lines(lines, hp_now_)) {
      //
      // TODO Update Entry Price
      // TODO Update Close Price
      //
      updated |= 1;
    }
  }

  if (Jerry.get_dir() && Jerry.update_lines(lines, hp_now_)) {
    updated |= 2;
  }

  if (mickey.get_dir() && mickey.update_lines(lines, hp_now_)) {
    updated |= 4;
  }

  if (swing.swing) {
    if (swing.update_lines(lines, hp_now_)) {
      //
      // TODO Update Entry Price
      // TODO Update Close Price
      //
      updated |= 8;
    }
  }

  return updated;
}

uint8_t
MakeDeal::dt_perfect_timing(const dir_t d) noexcept
{
  uint8_t a = 0;
  if (dirs.big_dir == d && wave.spot_dir == d && wfw.cls_dir == d &&
      wfw.vss_dir == d && wfw.tm_cls_far - t_now_ >= ONE_HALF_HOUR &&
      wfw.tm_vss_far - t_now_ >= MINUTES_30) {
    if (wfw.g2_dir == d && wfw.g2_time >= TWO_HOURS) {
      a = 1;
    } else if (dirs.mid_dir == d && dirs.ein_dir == d) {
      a = 2;
    } else if ((dirs.mid_dir == d || dirs.ein_dir == d) && wfw.g2_dir == d &&
               wfw.g2_time >= TWO_HOURS) {
      a = 3;
    }
  }
  return a;
}

uint8_t
MakeDeal::dt_good_timing(const dir_t d) noexcept
{
  uint8_t a = 0;
  if (d5.wave == d && d5.ext == d &&
      (wave.solved_cls_spp == d && wave.solved_vss_spp == d) &&
      (wfw.tm_cls_far - t_now_ >= ONE_HALF_HOUR) &&
      (wfw.tm_vss_far - t_now_ >= ONE_HALF_HOUR)) {
    if (wfw.g2_dir == d && wfw.g2_time >= TWO_HOURS) {
      a = 1;
    } else if (dirs.mid_dir == d && dirs.ein_dir == d) {
      a = 2;
    } else if ((dirs.mid_dir == d || dirs.ein_dir == d) && wfw.g2_dir == d &&
               wfw.g2_time >= TWO_HOURS) {
      a = 3;
    }
  }
  return a;
}

case_t
MakeDeal::dt_very_good_timing__(const dir_t d) noexcept
{
  auto very_good_timing__ = [&] {
    return (wfw.tm_vss_far < t_now_ && wfw.tm_vss_far + TWO_HOURS > t_now_ &&
            wfw.tm_cls_far - AN_HOUR >= t_now_ &&
            wfw.tm_cls_near - MINUTES_10 > t_now_);
  };
  auto vgt2_both_longer_one_hour__ = [&]() {
    return (LONGER_AN_HOUR(wfw.tm_vss_far) && LONGER_AN_HOUR(wfw.tm_cls_far));
  };
  auto vgt3_1__ = [&](const dir_t d) {
    bool yes = false;
    if (wave.have_cls_spp && wave.have_vss_spp &&
        LONGER_TWO_HOURS(wfw.g2_time) && LONGER_15M(wfw.tm_vss_far) &&
        LONGER_ONE_HALF(wfw.tm_cls_far)) {
      if (wfw.g2_dir == d) {
        if (wave.solved_cls_spp == d && wave.solved_vss_spp == d)
          yes = true;
        else if (!wave.solved_vss_spp && wfw.cls_dir == wfw.vss_dir)
          yes = true; // test 9, g2207034 2207034
      }
    }
    return yes;
  };

  auto vgt3_2__ = [&](const dir_t d) {
    bool yes = false;
    if (wave.have_cls_spp && wave.have_vss_spp && wave.solved_cls_spp == d &&
        wave.solved_vss_spp == d && dirs.big_dir == d && wave.spot_dir == d &&
        dirs.mid_dir == d && dirs.ein_dir == d) {
      if (LONGER_15M(wfw.tm_cls_far) && LONGER_15M(wfw.tm_vss_far)) {
        // But In her other hand, its 45, 30
        yes = true; // test 3, g2153353
      }
    }
    return yes;
  };

  auto vgt4_g2_was_to__ = [&](const dir_t d) {
    bool yes = false;
    if (wfw.g2_dir == -d && LESS_AN_HOUR(wfw.g2_time)) {
      if (wfw.cls_dir == d && LONGER_30M(wfw.tm_cls_near) &&
          LONGER_AN_HOUR(wfw.tm_cls_far)) {
        if (wave.solved_cls_spp == d && wave.solved_vss_spp == d)
          yes = true;
      }
    }
    return yes;
  };

  auto vgt5_2g2_1st_g2_good__ = [&](const dir_t d) {
    bool yes = false;
    if (wave.have_cls_spp && wave.have_vss_spp && wave.solved_cls_spp == d &&
        wave.solved_vss_spp == d) {
      // same-spp-dir to d
      if (wave.spot_dir == d && d5.wave == d && d5.ext == d) {
        // save-wave-dir to d
        if (dt_time_two_g2_first_good__(d)) {
          if (LONGER_4_HOURS(wfw.g2_time) && LONGER_15M(wfw.tm_vss_far)) {
            yes = true;
          }
        }
      }
    }
    return yes;
  };

  auto vgt6_vss_long_long_time_ago = [&](const dir_t d) {
    return dt_dir_vss_to_same_wave__(d);
  };

  auto vgt7_ein_mid_opposite = [&](const dir_t d) {
    if (dt_dir_min_ein_opposite__(d)) {
      return (LONGER_AN_HOUR(wfw.tm_vss_far) &&
              LONGER_ONE_HALF(wfw.tm_cls_far));
    }
    return false;
  };

  auto vgt8_one_spp = [&](const dir_t d) {
    return dt_time_solved_one_spp__(d);
  };
  auto vgt9_without_spp = [&](const dir_t d) {
    if (dt_dir_energe_without_spp__(d)) {
      if (LONGER_15M(wfw.fs_time))
        return true;
    }
    return false;
  };

  case_t cno = 0;
  if (very_good_timing__())
    cno |= 1;
  if (vgt2_both_longer_one_hour__())
    cno |= 2;
  if (vgt3_1__(d))
    cno |= 4;
  if (vgt3_2__(d))
    cno |= 8;
  if (vgt4_g2_was_to__(d))
    cno |= 16;
  if (vgt5_2g2_1st_g2_good__(d))
    cno |= 32;
  if (vgt6_vss_long_long_time_ago(d))
    cno |= 64;
  if (vgt7_ein_mid_opposite(d))
    cno |= 128;
  if (vgt8_one_spp(d))
    cno |= 256;
  if (vgt9_without_spp(d))
    cno |= 512;
  return cno;
}

bool
MakeDeal::calculate_energy__(const dir_t d, EnergyMark& em) noexcept
{
  // std::unordered_set<int64_t> sss = {
  //   2926350, 2926482, 2926692, 2926800, 2927256
  // };
  // if (sss.find(idx__) != sss.end()) {
  //   step_by_step();
  // }

  auto old_case = em.force_case_no();
  auto old_state = em.get_state();

  em.dir = d;
  em.zz_or_zzzz = dir_zz_or_zzzz__(d);
  if (em.zz_or_zzzz) {
    em.state = (em.zz_or_zzzz == 1) ? ENERGY_STATE::ZZ : ENERGY_STATE::ZZZZ;
  }
  em.same_spp_dir = dt_dir_same_spp_dir__(d);
  em.same_wave_dir = dt_dir_same_wave_dir__(d);
  em.large_reflection = dt_dir_is_large_reflection__(d);
  em.same_g2_dir = dt_dir_same_g2_dir__(d);
  em.g2_oppo_dir = dt_dir_g2_oppo_dir__(d);
  em.same_mid_ein_dir = dt_dir_same_mid_ein_dir__(d);
  em.nice_region = dt_dir_nice_region__(d);
  em.spot_without_spp = dt_dir_same_spot_without_spp__(d);
  em.two_g2_1st_to = dt_time_two_g2_first_to__(d);
  em.two_g2_1st_good = dt_time_two_g2_first_good__(d);
  em.vss_to_same_wave = dt_dir_vss_to_same_wave__(d);
  em.min_ein_opposite = dt_dir_min_ein_opposite__(d);
  em.force_solved_one_spp = dt_dir_solved_one_spp__(d);
  em.time_solved_one_spp = dt_time_solved_one_spp__(d);
  em.energe_without_spp = dt_dir_energe_without_spp__(d);
  em.timing_case = dt_very_good_timing__(d);
  em.sure_vss_spp_dir = dt_dir_sure_cls_spp_dir__(d);
  em.mid_zero_other_same = dt_dir_mid_zero_other_same__(d);
  em.one_spp_solved_dir = dt_dir_one_spp_solved_dir__(d);
  em.diff_mid_ein_same_spp = dt_dir_diff_mid_ein_same_spp__(d);
  em.mid_ein_oppo_but_g2_to = dt_dir_mid_ein_oppo_but_g2_to__(d);
  em.diff_cls_ein_g2_2type = dt_dir_diff_cls_ein_g2_2type__(d);
  em.one_spp_and_first_and_g2 = dt_dir_one_spp_and_first_and_g2__(d);
  em.zzzz_g2_and_first = dt_dir_zzzz_g2_and_first__(d);
  em.zzzz_and_no_g2 = dt_dir_zzzz_and_no_g2__(d);
  em.state = em.get_state();

  em.energy_case();
  return (old_case != em.force_case_no() || old_state != em.state);
}

const double min_stop_win [[maybe_unused]] = 5.88;
// const double min_safe_stop_win = -10;

int8_t
MakeDeal::check_energy_dir() noexcept
{
  auto up_case = em_up.energy_case();
  auto dn_case = em_dn.energy_case();
  dir_t en_dir = EnergyDir::energy_dir(up_case, dn_case);
  int8_t ed_changed = 0;
  if (en_dir && !edir.dir) {
    ed_changed = 1;
  } else {
    ed_changed = edir.changed(en_dir, up_case, dn_case);
  }
  if (ed_changed) {
    edir.dir = en_dir;
    edir.up_case = up_case;
    edir.dn_case = dn_case;
    edir.idx = idx__;
    edir.tm = t_now_;
    edir.hpx = hp_now_;

#if (ONLINE_DATA_COMM)
    // send_signal_energy_dir_changed();
#endif // ONLINE_DATA_COMM
  }

  return ed_changed;
}

time_t
MakeDeal::get_weak_timer___(const dir_t d) noexcept
{
  time_t timer = wfw.get_weak_timer(wave, dirs, d, t_now_);

  if (timer <= t_now_)
    timer = TIMER(MINUTES_45);

  return timer;
}

time_t
MakeDeal::get_strong_timer___(const dir_t d) noexcept
{
  time_t timer = 0;
  timer = wfw.get_timer(wave, d, t_now_);
  if (!timer || timer < t_now_ + MINUTES_30)
    timer = TIMER(MINUTES_45);
  return timer;
}

time_t
MakeDeal::weak_timer___(const dir_t d) noexcept
{
  time_t t = turns.get_weak_timer(d, t_now_);
  if ((t < t_now_ + 300) || (t > t_now_ + 90 * 60))
    t = TIMER(MINUTES_45);

  return t;
}

time_t
MakeDeal::strong_timer___(const dir_t d) noexcept
{
  time_t t = turns.get_strong_timer(d, t_now_);
  if (t < t_now_ + 300)
    t = TIMER(MINUTES_45);
  // t = get_strong_timer___(d);
  return t;
}

bool
MakeDeal::its_good_timing_for_dir__(const dir_t d, const time_t tm) noexcept
{
  return LONGER_30M(tm);
}

bool
MakeDeal::intuitive_wave_risk__() const noexcept
{
  return (d5.wave != Tom.get_dir() || d5.ext != Tom.get_dir());
}

#if (ONLINE_DATA_COMM)
int
MakeDeal::send_signal_energy_dir_changed() noexcept
{
  auto& l = (swing.swing) ? swing.target.lines : lines;
  if (edir.dir && l.nup && l.ndn) {
    double u1 = 0, u2 = 0, d1 = 0, d2 = 0;
    if (l.nup > 0)
      u1 = l.hp_up_1;
    if (l.nup > 1)
      u2 = l.hp_up_2;
    else
      u2 = u1;
    if (l.ndn > 0)
      d1 = l.hp_dn_1;
    if (l.ndn > 1)
      d2 = l.hp_dn_2;
    else
      d2 = d1;
    const char* format = R"({"t":52,"i":%ld,"d":%d,"s":%d,"set":%ld,)"
                         R"("u1":%.02lf,"u2":%.02lf,"d1":%.02lf,"d2":%.02lf,)"
                         R"("uc":%u,"dc":%u})";
    char buf[1024];
    snprintf(buf,
             1024,
             format,
             t_now_,
             edir.dir,
             swing.swing,
             swing.target.timer.t,
             u1,
             u2,
             d1,
             d2,
             edir.up_case,
             edir.dn_case);

    mqtt_send_message(buf);
  }
  return (0);
}

int
MakeDeal::send_signal_pets_was_changed() noexcept
{
  char buf_tom[1024];
  int len_tom = 0;
  char buf_jerry[1024];
  int len_jerry = 0;
  char buf_mikey[1024];
  int len_mikey = 0;

  if (Tom.is_running()) {
    len_tom = Tom.get_json(buf_tom, 1024, "Tom");
  }
  if (Jerry.is_running()) {
    len_jerry = Jerry.get_json(buf_jerry, 1024, "Jerry");
  }
  if (mickey.is_running()) {
    len_mikey = mickey.get_json(buf_mikey, 1024, "mickey");
  }

  // "{\"type\":51,\"pets\":[{\"type\":50,\"name\":\"Tom\",\"dir\":1,\"line1\":1,\"line2\":1,\"to\":1,\"tm\":1697997030666,\"up1\":31000,\"up2\":31100,\"dn1\":30100,\"dn2\":29800}]}";
  char buf[4096];
  strcpy(buf, "{\"type\":51,\"pets\":[");
  if (len_tom)
    strncat(buf, buf_tom, sizeof(buf) - strlen(buf) - 1);
  if (len_jerry) {
    if (len_tom)
      strcat(buf, ",");
    strncat(buf, buf_jerry, sizeof(buf) - strlen(buf) - 1);
  }
  if (len_mikey) {
    if (len_tom || len_jerry)
      strcat(buf, ",");
    strncat(buf, buf_mikey, sizeof(buf) - strlen(buf) - 1);
  }
  strcat(buf, "]}");

  mqtt_send_message(buf);

  return (0);
}
#endif // #if (ONLINE_DATA_COMM)

uint16_t
MakeDeal::basic_data_changed(const int8_t signal_probe,
                             const time_t tm_signal) noexcept
{
  event_t event = 0;
  // code block
  // --------------------------------------------------------------
  // Begin: the basic data was changed
  if (dirs_was_changed__()) {
    event |= EVENT_5DIRS_CHANGED;
  }

  dirs.is_possible_to_change(pdc.mid, pdc.ein, t_now_);

  if (signal_probe & 2) { // Dirs changed
  }
  if (calculate_energy__(1, em_up)) {
  }
  if (calculate_energy__(-1, em_dn)) {
  }

  if (wfw.fs_work_changed(t_now_)) {
  }

  if (signal_probe & 1) { // lines changed
    lines_changed_();
  }

  if (lines.limit_reached(hp_now_)) {
  }

  if (signal_probe & 4) { // wave changed
  }

  if (signal_probe & 16) { // WFW changed
    if (Tom.get_dir() && Tom.get_dir() == wfw.g2_dir &&
        Tom.d5.g2 == -wfw.g2_dir) {
      Tom.update_g2(wfw, wave);
    }
  }

  big_dir_long_long_time__();

  return event;
}

#if (!ONLINE_DATA_COMM)
int
MakeDeal::parse_signal_line(const char* line,
                            event_t& event,
                            int8_t& signal_probe,
                            bool& start_trade,
                            int8_t& test_signal,
                            time_t& tm) noexcept
{
  int ret = 0;
  Document doc;
  if (doc.Parse(line).HasParseError()) {
    printf("Error at %s\n", line);
    ret = -1111;
  }

  int type = 0;

  if (doc.HasMember("t")) {
    type = doc["t"].GetInt();
    // printf("Type %d ", type);
  } else {
    printf("Error type at %s\n", line);
    ret = -1112;
  }

  if (doc.HasMember("i")) {
    tm = time_t(doc["i"].GetInt64());
  } else {
    printf("Error time at %s\n", line);
    ret = -1113;
  }

  switch (type) {
    case 1: {
      ret = ParseLines(doc, lines, line);
      // lines.print(idx__, hp_now_);
      signal_probe |= 1;
    } break;
    case 10: {
      // StageDirs sd;
      ret = ParseDirs(doc, dirs, line);
      dirs.check_zero_dir();
      // dirs.print(td[i].ftime, wave.dir, idx__);
      signal_probe |= 2;
    } break;
    case 11: {
      // Wave w;
      ret = ParseWave(doc, wave, line);
      // wave.print(dirs.vss_dir, dirs.ein_dir, idx__);
      signal_probe |= 4;
    } break;
    case 12:
      break;
    case 13:
      ret = ParseWfw(doc, wfw, line);
      // wfw.print(t_now_);
      signal_probe |= 16;
      break;
  }
  if (ret) {
    // ++errs;
    printf("Error at Type %d, err %d. %s\n", type, ret, line);
    return ret;
  }

  test_signal |= signal_probe;
  // all += sv->size();

  start_trade = (test_signal >= 7);
  //
  // Get signal  -- end
  //
  return ret;
}
#endif // !ONlINE

int
MakeDeal::hited_tom_open_or_stop_price(event_t& event) noexcept
{
  if (Tom.is_running()) {
    auto hited = Tom.hit_open_price(idx__, t_now_, hp_now_);
    if (hited) {
      event |= EVENT_TOM_REACHED_PRICE;
    }

    Tom.incomming.update_revenue(hp_now_,
                                 Tom.get_dir(),
                                 Tom.os_entries.average,
                                 Tom.os_entries.amount_reached,
                                 t_now_,
                                 idx__);

    auto hited_close_type [[maybe_unused]] =
      Tom.close_price.reached_stop_price(Tom.incomming.rvn_single);
    if (hited_close_type) {
      event |= EVENT_TOM_REACHED_CLOSE_HPX;
    }
  }
  return (0);
}

int
MakeDeal::reached(event_t& event) noexcept
{
  lines.limit_reached(hp_now_);
  // dd_bd.durantion(dd_bd.get_first_dir(), t_now_);

  turns.is_timer_outed(1, t_now_);
  turns.is_timer_outed(-1, t_now_);

  if (turns.first()) {
    if (turns.first()->i_am_important(hp_now_)) {
      if (important_turn_dir != turns.first()->dir) {
        important_turn_dir = turns.first()->dir;
        event |= EVENT_IMPORTANT_TURN;
      }
    }
  }

  r_tom = 0;
  r_jerry = 0;
  r_mickey = 0;
  r_swing = 0;

  int ret = 0;
  if (Tom.is_running()) {

    bool big_reverse_changed = Tom.incomming.big_reverse(hp_now_, t_now_);
    if (big_reverse_changed) {
      if (Tom.incomming.d_big_reverse) {
        printf("!!! <<< Big Needle %d Warning >>> %ld %ld!!!\n",
               Tom.incomming.d_big_reverse,
               t_now_,
               idx__);
        event |= EVENT_BIG_NEEDLE_OCCURED;
      } else {
        printf("<<< Big Needle Canceled >>> %ld %ld\n", t_now_, idx__);
        event |= EVENT_BIG_NEEDLE_DISAPPEARED;
      }
    }

    r_tom = Tom.reached(t_now_, hp_now_, lines, idx__);
    if (r_tom) {
      if (r_tom & LIMIT_REACHED::TIMER_OUT) {
        time_t tm = strong_timer___(Tom.get_dir());
        Tom.update_timer(tm, tm);
      }

      printf("TO %d: (S%d, T%d) ", Tom.amout_to, dir_turns_space, tjm_dir);

      print_reached("Tom", Tom.get_dir(), r_tom, idx__);
      // print_reached("Total Reached", Tom.get_dir(), Tom.reacheded(),
      // idx__);

      event |= EVENT_TOM_REACHED;
    }
  }

  if (Jerry.get_dir()) {
    auto hited = Jerry.open_price(idx__, t_now_, hp_now_);
    if (hited) {
    }

    Jerry.incomming.update_revenue(hp_now_,
                                   Jerry.get_dir(),
                                   Jerry.os_entries.average,
                                   Jerry.os_entries.amount_reached,
                                   t_now_,
                                   idx__);

    r_jerry = Jerry.reached(t_now_, hp_now_, lines, idx__);
    if (r_jerry) {
      printf("TO %d: ", Jerry.amout_to);
      print_reached("Jerry", Jerry.get_dir(), r_jerry, idx__);
      // print_reached("Total Reached", Jerry.get_dir(), Jerry.reacheded(),
      // idx__);
      event |= EVENT_JERRY_REACHED;

      if (r_jerry & LIMIT_REACHED::TIMER_OUT) {
        jerry_to_amount++;
        auto tm = weak_timer___(Jerry.get_dir());
        // time_t tom_tm = Tom.target.timer.t;
        // if (tm > tom_tm)
        //   tm = MINUTES_45 + t_now_;
        Jerry.update_timer(tm, tm);
      }
      if (Jerry.reached_line2() && jerry_reached < 2) {
        jerry_reached = 2;
      } else if (Jerry.reached_line1() && jerry_reached < 1) {
        jerry_reached = 1;
      }

      ret = tjm_jerry_reached(r_jerry);
    }
  }

  if (mickey.get_dir()) {
    auto hited = mickey.open_price(idx__, t_now_, hp_now_);
    if (hited) {
    }

    dir_t br = mickey.incomming.big_reverse(hp_now_, t_now_);
    if (br && br == -mickey.get_dir()) {
      event |= EVENT_BIG_NEEDLE_OCCURED;
    }

    r_mickey = mickey.reached(t_now_, hp_now_, lines, idx__);
    if (r_mickey) {
      if (r_mickey & LIMIT_REACHED::TIMER_OUT) {
        tjm_add_action_mickey_stop(10000);
      } else {
        mickey.erase_opposite_reached(r_mickey);
      }

      printf("TO %d: ", mickey.amout_to);
      print_reached("Mickey", mickey.get_dir(), r_mickey, idx__);
      print_reached(
        "Total Reached", mickey.get_dir(), mickey.reacheded(), idx__);
      if (mickey.reached_line1() || mickey.timer_outed()) {
        event |= EVENT_MICKEY_REACHED;
      }
    }

    mickey.incomming.update_revenue(hp_now_,
                                    mickey.get_dir(),
                                    mickey.os_entries.average,
                                    mickey.os_entries.amount_reached,
                                    t_now_,
                                    idx__);
  }

  if (swing.swing) {
    r_swing = swing.reached(t_now_, hp_now_, lines, idx__);
    if (r_swing) {
      if (swing.set_swing_dir(r_swing)) {
        event |= EVENT_SWING_DIR_CHANGED;
      }
      if (r_swing & LIMIT_REACHED::TIMER_OUT) {
        swing.reset();
      } else {
        // if (swing.dir_swing)
        //   swing.target.erase_opposite_reached(-swing.dir_swing, r_swing);
      }
    }
  }

  return ret;
}

int
MakeDeal::test_dirs(event_t& event) noexcept
{
  if (!turns.very_good())
    return (0);

  if (turns.change_turn(t_now_)) {
    event |= EVENT_TURN_CHANGED;
  }

  bool tss_changed = turns.turn_time_status_changed(t_now_, Tom.get_dir());
  if (tss_changed) {
    if (dd_tts.new_dir_maker(
          dir_t(turns.retrive_turn_time_status()), t_now_, hp_now_)) {
      event |= EVENT_TTS_CHANGED;
    }
  }

  dir_t ts_dir = turns.space_dir(t_now_, hp_now_);
  if (dir_turns_space != ts_dir) {
    if (ts_dir) {
      dir_turns_space = ts_dir;
      bool changed = dd_sd.new_dir_maker(ts_dir, t_now_, hp_now_);
      if (changed) {
        tjm_dir = tjm_estimate_dir(tjm_cno);
        event |= EVENT_SPACE_DIR_CHANGED;
      }
    }
  }

  if (!dd_sd.is_swing_dir(t_now_)) {
    if (!space_dir_is_stable) {
      space_dir_is_stable = true;
      event |= EVENT_SPACE_DIR_STABLE;
    }
  } else {
    space_dir_is_stable = false;
  }

  return (0);
}

int
MakeDeal::energy_battle(event_t& event, const time_t tm_signal) noexcept
{
  int ret = 0;
  int8_t ed_changed [[maybe_unused]] = check_energy_dir();
  auto zz_zz = check_zzzz();
  if (zz_zz < 0) {
    return -12123;
  } else if (zz_zz == 1) {
    event |= EVENT_SWING_CHANGED;
    auto& l = (Tom.is_running() ? Tom.target.lines : lines);
    dir_t d = l.get_ext_hit_dir(lines.ext_rgn);
    if (d) {
      swing.reset();
      swing.set(idx__, hp_now_, t_now_, lines);
      swing.dir_swing = d;
      event |= EVENT_SWING_DIR_CHANGED;
    }
  }

  int8_t ed_dir_changed [[maybe_unused]] = 0;
  if (edir.dir != dd_em.get_first_dir()) {
#if ONLINE_DATA_COMM
    if (dd_em.new_dir_maker(edir.dir, t_now_, hp_now_))
#else
    if (dd_em.new_dir_maker(edir.dir, tm_signal, hp_now_))
#endif
      event |= EVENT_ENERGY_DIR_CHANGED;
  }

  if (dirs.big_dir != dd_bd.get_first_dir()) {
    if (dirs.big_dir && dd_bd.new_dir_maker(dirs.big_dir, dirs.tm, hp_now_)) {
      event |= EVENT_BIG_DIR_CHANGED;
    }
  }

  ft_up = fight_the_side_(1, false);
  ft_dn = fight_the_side_(-1, false);

  {
    dir_t ft_dir = 0;
    if (ft_up > ft_dn)
      ft_dir = 1;
    else if (ft_up < ft_dn) {
      ft_dir = -1;
    }

    if (ft_dir) {
      if (dd_ft.get_first_dir() != ft_dir) {
        if (dd_ft.new_dir_maker(ft_dir, t_now_, hp_now_)) {
          event |= EVENT_FIGHT_DIR_CHANGED;
        }
      }
    }
  }

  {
    const dir_t pdc_dir = pdc.current_dir();
    const dir_t pdc_ppp_dir_1 = get_probability_dir_();
    const dir_t pdc_ppp_dir_2 = get_no_mid_ein_dir_();

    dir_t d = 0;
    if (pdc_dir)
      d = pdc_dir;

    if (pdc_ppp_dir_2) {
      d = pdc_ppp_dir_2;
    } else if (pdc_ppp_dir_1) {
      d = pdc_ppp_dir_1;
    }

    if (d != dd_pdc.get_first_dir() &&
        dd_pdc.new_dir_maker(d, t_now_, hp_now_)) {
      event |= EVENT_PDC_DIR_CHANGED;
    }
  }

  return ret;
}

int
MakeDeal::check_ced_change(event_t& event) noexcept
{
  dir_t ced = fight_strong_compound_energy_dir();
  if (ced && ced != dd_ced.get_first_dir()) {
    if (dd_ced.new_dir_maker(ced, t_now_, hp_now_)) {
      if (ced != the_dir) {
        the_dir = ced;
        event |= EVENT_CE_DIR_CHANGED;

        if (Tom.is_running()) {
          // printf("\033[48;5;168m  ~~~~~ The Dir = %d ~~ %.02lf ~ %.02lf ~ "
          //        "g%ld ~~\033[0m\n",
          //        the_dir,
          //        dd_ced.get_hp_delta(),
          //        (hp_now_ - Tom.incomming.max_win_hpx),
          //        idx__);
        } else {
          // printf(
          //   "\033[48;5;168m  ~~~~~ The Dir = %d ~~ %.02lf ~ g%ld
          //   ~~\033[0m\n", the_dir, dd_ced.get_hp_delta(), idx__);
        }

        auto get_timer = [&](const time_t t1, const time_t t2) -> time_t {
          // auto min_1 = TO_MINUTES(t1);
          // auto min_2 = TO_MINUTES(t2);
          // time_t t = 0;
          // if (min_1 <= 180 && min_2 <= 180)
          //   t = std::max(t1, t2);
          // else
          //   t = std::min(t1, t2);
          auto t = time_t(double(std::max(t1, t2) - t_now_) * 0.75 + t_now_);
          return t;
        };

        time_t tup = get_timer(get_strong_timer___(1), get_weak_timer___(1));
        time_t tdn = get_timer(get_strong_timer___(-1), get_weak_timer___(-1));
        if (turns.new_turn(the_dir, idx__, t_now_, hp_now_, tup, tdn)) {
          // turns.front()->print(idx__, t_now_, hp_now_);
#if (SAVE_STOP_IDX_TO_FILE)
          if (save_stop_idx) {
            int t1 = TO_MINUTES(tup);
            int t2 = TO_MINUTES(tdn);
            int t[8] = { 0 };
            t[0] = t1;
            t[1] = t2;
            t[2] = idx__ + (t[0] * 60);
            t[3] = idx__ + (t[1] * 60);
            save_stop_idx_to_file(idx__,
                                  dd_ced.get_hp_delta(),
                                  the_dir,
                                  t,
                                  dd_bd.get_first_dir(),
                                  swing.swing,
                                  swing.dir_swing,
                                  hp_now_,
                                  lines);
          }
#endif // #if (SAVE_STOP_IDX_TO_FILE)

          if (Tom.is_running()) {
            if (Jerry.is_running()) {
              time_t tm = weak_timer___(Jerry.get_dir());
              if (Jerry.timer_outed() || tm < Jerry.target.timer.t) {
                // time_t tom_tm = Tom.target.timer.t;
                // if (tm > tom_tm)
                //   tm = MINUTES_45 + t_now_;
                Jerry.update_timer(tm, tm);
              }
            }
          } // check Tom&Jerry timer
        }   // new turn item
      }     // EVENT_CE_DIR_CHANGED
    }       // new ced dir was accepted
  }         // CED dir was changed

  return (0);
}

dir_t
MakeDeal::get_no_mid_ein_dir_() const noexcept
{
  auto get_pd_ = [&](const dir_t d) -> dir_t {
    if (d5.wave == d && d5.ext == d && dd_ft.get_first_dir() == d &&
        dd_em.get_first_dir() == d && !wave.solved_cls_spp &&
        !wave.solved_vss_spp) {

      if (pdc.no_mid_ein())
        return d;
    }
    return (0);
  };

  auto u = get_pd_(1);
  auto d = get_pd_(-1);
  if (u && !d) {
    return 1;
  } else if (!u && d) {
    return -1;
  }
  return (0);
}

dir_t
MakeDeal::get_probability_dir_() const noexcept
{
  auto get_pd_ = [&](const dir_t d) -> dir_t {
    if (d5.wave == d && d5.ext == d &&
        ((wave.solved_cls_spp == d && !wave.solved_vss_spp) ||
         (!wave.solved_cls_spp && d == wave.solved_vss_spp)) &&
        (wave.spot_dir == d || wfw.fs_dir == d) && dd_ft.get_first_dir() == d &&
        dd_em.get_first_dir() == d) {
      if (pdc.one_possible_to_dir(d))
        return d;
    } else if (d5.wave == d && d5.ext == d && wave.solved_cls_spp == d &&
               wave.solved_vss_spp && (wave.spot_dir == d || wfw.fs_dir == d) &&
               dd_ft.get_first_dir() == d && dd_em.get_first_dir() == d) {
      if (pdc.one_possible_to_dir(d))
        return d;
    } else if (swing.swing && d5.wave == d && d5.ext == d &&
               pdc.one_possible_to_dir(d)) {
      return d;
    }
    return (0);
  };

  if (get_pd_(1) && !get_pd_(-1)) {
    return 1;
  } else if (!get_pd_(1) && get_pd_(-1)) {
    return -1;
  }
  return (0);
}

#if (STONE_DEBUG || STEP_BY_STEP)
std::string event_show_type = "eee";
std::string tjm_run_type = "atjm"; // "mtjm";
#endif

int
MakeDeal::fighting(const event_t event) noexcept
{
  int ret = 0;

#if (STONE_DEBUG || STEP_BY_STEP)
  bool quit = false;
#endif // (STONE_DEBUG || STEP_BY_STEP)

  if (event) {
    if (!dd_bd.long_dir && dd_bd.get_first() &&
        dd_bd.get_first_durantion(t_now_) > 170) {
      dd_bd.long_dir = dd_bd.get_first_dir();
      dd_bd.long_dur = dd_bd.get_first_durantion(t_now_);
    }

#if (STONE_DEBUG || STEP_BY_STEP || ONLINE_DATA_COMM)
    printf("Event %u 0x%x\n", event, event);
    show_params();
#endif // (STONE_DEBUG || STEP_BY_STEP || ONLINE_DATA_COMM)

#if (STONE_DEBUG || STEP_BY_STEP)
    bool pause = false;
    if (event_show_type == "eb" || event_show_type == "esb") {
      if (event & EVENT_BIG_DIR_CHANGED) {
        pause = true;
      }
    }

    if ((event_show_type == "es" || event_show_type == "esb") &&
        ((event & EVENT_SWING_DIR_CHANGED) && swing.dir_swing)) {
      pause = true;
    }

    if ((event_show_type == "e3d")) {
      if (event & (EVENT_CE_DIR_CHANGED | EVENT_BIG_DIR_CHANGED |
                   EVENT_SWING_DIR_CHANGED))
        pause = true;
    }

    if ((event_show_type == "ece") && (event & EVENT_CE_DIR_CHANGED)) {
      pause = true;
    }

    if (event_show_type == "etr" && event & EVENT_TOM_REACHED)
      pause = true;

    if (event_show_type == "ectj" &&
        event &
          (EVENT_CE_DIR_CHANGED | EVENT_TOM_REACHED | EVENT_JERRY_REACHED))
      pause = true;

    if (event_show_type == "ejr" && (event & EVENT_JERRY_REACHED))
      pause = true;

    if (event_show_type == "emr" && (event & EVENT_MICKEY_REACHED))
      pause = true;

    if (event_show_type == "epr" &&
        (event & (EVENT_TOM_REACHED | EVENT_TOM_REACHED_PRICE |
                  EVENT_JERRY_REACHED | EVENT_MICKEY_REACHED))) {
      pause = true;
    }

    if (event_show_type == "etts" && (event & EVENT_TTS_CHANGED)) {
      pause = true;
    }

    if (event_show_type == "erp" &&
        (event & (EVENT_TOM_REACHED_PRICE | EVENT_TOM_REACHED_CLOSE_HPX |
                  EVENT_TOM_REACHED_FORCED))) {
      pause = true;
    }

    if (event_show_type == "eall") {
      pause = true;
    }

    if (event_show_type == "esd" && (event & EVENT_SPACE_DIR_CHANGED))
      pause = true;
    if (event_show_type == "estj" &&
        (event &
         (EVENT_SPACE_DIR_CHANGED | EVENT_TOM_REACHED | EVENT_JERRY_REACHED)))
      pause = true;

    if ((event & EVENT_BIG_NEEDLE_OCCURED) && event_show_type == "ebn")
      pause = true;

    if (event_show_type == "eee" &&
        (event &
         (EVENT_BIG_NEEDLE_OCCURED | EVENT_BIG_DIR_CHANGED |
          EVENT_SPACE_DIR_STABLE | EVENT_TOM_REACHED | EVENT_TOM_REACHED_PRICE |
          EVENT_JERRY_REACHED | EVENT_MICKEY_REACHED))) {
      pause = true;
    }

    if (event == EVENT_TOM_REACHED_PRICE ||
        event == EVENT_TOM_REACHED_CLOSE_HPX ||
        event == EVENT_TOM_REACHED_FORCED) {
      pause = false;
    }

    if (event_show_type == "eno") {
      pause = false;
    }

    if (pause) {
      if (idx__ >= g_stop_idx) {
        if (step_by_step() < 0) {
          quit = true;
        }
      }
    }
#endif // (STONE_DEBUG || STEP_BY_STEP)

    if (!ret) {
#if (STEP_BY_STEP)
      if (tjm_run_type == "atjm")
        ret = tjm_framework(event);
#else
      ret = tjm_framework(event);
#endif // STEP_BY_STEP
    }
  }

#if (STONE_DEBUG || STEP_BY_STEP)
  if (quit)
    ret = -10;
#endif

  return ret;
}

int
MakeDeal::tjm_hited_something(const event_t event) noexcept
{
  int ret = 0;

  case_t case_no = 0;
  dv_get_dir.clear();
  tjm_cno = 0;
#if (STONE_DEBUG || STEP_BY_STEP)
  auto old_tjm_dir = tjm_dir;
#endif

  tjm_dir = tjm_estimate_dir(case_no);
#if (STONE_DEBUG || STEP_BY_STEP)
  auto a = tjm_solved_spp_dir_with_timer(1);
  auto b = tjm_solved_spp_dir_with_timer(-1);
  if (tjm_dir != old_tjm_dir) {
    printf(
      "***************** TJM DIR %d <== %d , ab %d %d ***************** %ld\n",
      tjm_dir,
      dir_turns_space,
      a,
      b,
      idx__);
  }
#endif

  if (event & (EVENT_TOM_REACHED_CLOSE_HPX | EVENT_TOM_REACHED_FORCED)) {
    ret |= tjm_tom_reached_close_price(event);
  }

  if (event & EVENT_TOM_REACHED_PRICE) {
    ret |= tjm_tom_reached_open_price(event);
  }

  if (event & EVENT_TOM_REACHED) {
    const auto d = Tom.get_dir();
    if ((d > 0 && (r_tom & (LIMIT_REACHED::UP_1 | LIMIT_REACHED::UP_2))) ||
        (d < 0 && (r_tom & (LIMIT_REACHED::DN_1 | LIMIT_REACHED::DN_2)))) {
      ret |= tjm_tom_reached_limit_lines(r_tom);
    } else if ((d < 0 &&
                (r_tom & (LIMIT_REACHED::UP_1 | LIMIT_REACHED::UP_2))) ||
               (d > 0 &&
                (r_tom & (LIMIT_REACHED::DN_1 | LIMIT_REACHED::DN_2)))) {
      ret |= tjm_tom_reached_opposite_limit_lines(r_tom);
    }
  }

  if (!ret && (event & EVENT_JERRY_REACHED)) {
  }

  if (!ret && (event & (EVENT_MICKEY_REACHED))) {
  }

  return ret;
}

int
MakeDeal::tjm_framework(const event_t event) noexcept
{
  int ret = 0;

  case_t case_no = 0;
  dv_get_dir.clear();
  tjm_cno = 0;
#if (STONE_DEBUG || STEP_BY_STEP)
  auto old_tjm_dir = tjm_dir;
#endif

  tjm_dir = tjm_estimate_dir(case_no);
#if (STONE_DEBUG || STEP_BY_STEP)
  auto a = tjm_solved_spp_dir_with_timer(1);
  auto b = tjm_solved_spp_dir_with_timer(-1);
  if (tjm_dir != old_tjm_dir) {
    printf("***************** TJM DIR %d <== %d , ab %d %d *****************\n",
           tjm_dir,
           dir_turns_space,
           a,
           b);
  }
#endif

  ret |= tjm_make_deal(event);

  if (!ret && (event & EVENT_5DIRS_CHANGED)) {
    if (Tom.is_running() && Tom.get_dir() == -bear_dir) {
      ret |= tjm_make_deal(event);
    }
  }

  if (!ret && (event & (EVENT_TOM_REACHED))) {
    //
    // Case 30001, 30002: calculate the good_to_win and the min_win
    //
    if (limit_reached_this_side(Tom.get_dir(), r_tom)) {
      if (limit_reached_line2(Tom.get_dir(), r_tom)) {
        ret |= tjm_add_action_set_good_to_win(30001);
      } else if (limit_reached_line1(Tom.get_dir(), r_tom) &&
                 Tom.timer_outed()) {
        //
        // good-to-win
        //
        ret |= tjm_add_action_set_good_to_win(30003);
      }
    } else if (limit_reached_this_side(-Tom.get_dir(), r_tom)) {
      //
      // forced-close
      //
      ret |= tjm_add_action_set_stop_loss(30005);
    }
  }

  if (!ret && (event & EVENT_JERRY_REACHED)) {
    //
    // Case 30001, 30002: calculate the good_to_win and the min_win
    //
    if (limit_reached_this_side(Jerry.get_dir(), r_jerry)) {
      if (limit_reached_line2(Jerry.get_dir(), r_jerry)) {
        ret |= tjm_add_action_set_good_to_win(30002);
      } else if (limit_reached_line1(Jerry.get_dir(), r_jerry) &&
                 Jerry.timer_outed()) {
        //
        // good-to-win
        //
        ret |= tjm_add_action_set_good_to_win(30004);
      }
    } else if (limit_reached_this_side(-Jerry.get_dir(), r_jerry)) {
      //
      // forced-close
      //
      ret |= tjm_add_action_set_stop_loss(30006);
    }
  }

  if (!ret && (event & (EVENT_MICKEY_REACHED))) {
  }

  return ret;
}

int
MakeDeal::time_battle(event_t& event) noexcept
{
  return test_dirs(event);
}

case_t
MakeDeal::tjm_tom_reached_limit_lines(const uint8_t tom_reached
                                      [[maybe_unused]]) noexcept
{
  // if (Tom.stop_price.rvn_force_stop < 8) {
  //   Tom.stop_price.rvn_force_stop = 8;
  //   printf("Update The Forced Stop Revunue to 8\n");
  // }

  Tom.erase_opposite_reached(tom_reached);
  return (0);
}

case_t
MakeDeal::tjm_tom_reached_opposite_limit_lines(
  const uint8_t tom_reached) noexcept
{
  show_params();
  return (0);
}

} // namespace Trade
