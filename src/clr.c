#include <stdio.h>

int main(void) {
   printf("1. \033[1;31mThis is red text\033[0m\n");
   printf("2, \033[2;31mThis is red text\033[0m\n");
   printf("3. \033[3;31mThis is red text\033[0m\n");
   printf("4. \033[4;31mThis is red text\033[0m\n");
   printf("5. \033[5;31mThis is red text\033[0m\n");
   printf("6. \033[6;31mThis is red text\033[0m\n");
   printf("7. \033[7;31mThis is red text\033[0m\n");
   printf("8. \033[8;31mThis is red text\033[0m\n");
   printf("9, \033[9;31mThis is red text\033[0m\n");
   printf("10. \033[10;31mThis is red text\033[0m\n");
   printf("11. \033[11;31mThis is red text\033[0m\n");
   printf("12. \033[12;31mThis is red text\033[0m\n");
   printf("13. \033[13;31mThis is red text\033[0m\n");
   printf("14. \033[14;31mThis is red text\033[0m\n");
   printf("15. \033[15;31mThis is red text\033[0m\n");
   printf("16, \033[16;31mThis is red text\033[0m\n");
   printf("17. \033[17;31mThis is red text\033[0m\n");
   printf("18. \033[18;31mThis is red text\033[0m\n");
   printf("19. \033[19;31mThis is red text\033[0m\n");
   printf("20. \033[20;31mThis is red text\033[0m\n");
   printf("21. \033[21;31mThis is red text\033[0m\n");
   printf("22. \033[22;31mThis is red text\033[0m\n");
   printf("23, \033[23;31mThis is red text\033[0m\n");
   printf("24. \033[24;31mThis is red text\033[0m\n");
   printf("25. \033[25;31mThis is red text\033[0m\n");
   printf("26. \033[26;31mThis is red text\033[0m\n");
   printf("27. \033[27;31mThis is red text\033[0m\n");
   printf("28. \033[28;31mThis is red text\033[0m\n");
   printf("29. \033[29;31mThis is red text\033[0m\n");

   printf("\033[;32mGreen Text\033[0m\n");
   printf("\033[4;33mYellow underlined text\033[0m\n");
   printf("\033[;34mBlue text\033[0m\n");

   return (0);
}