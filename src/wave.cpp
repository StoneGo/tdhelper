#include "wave.hpp"
#include <cstdio>

namespace Trade {

void
Wave::print(const dir_t mid, const dir_t ein, const int64_t idx) noexcept
{
  printf("\033[38;5;12m-- W --\033[0m %d <-- %d %d | %d %d | spp %d %d, %d %d "
         "| %d %d %d %d | g%ld\n",
         dir,
         prev_dir,
         spot_dir,
         mid,
         ein,
         solved_cls_spp,
         solved_vss_spp,
         have_cls_spp,
         have_vss_spp,
         spp_cls_slope,
         spp_vss_slope,
         spp_cls_type,
         spp_vss_type,
         idx);
}

bool
Wave::good_score(const dir_t d) noexcept
{
  bool good = false;
  return good;
}

bool
Wave::solved_dir(const dir_t d) noexcept
{
  bool good = ((have_cls_spp && solved_cls_spp == d && !have_vss_spp) ||
               (have_vss_spp && solved_vss_spp == d && !have_cls_spp) ||
               ((have_cls_spp && solved_cls_spp == d) &&
                (have_vss_spp && solved_vss_spp == d)));
  return good;
}

bool
Wave::half_solved_dir() noexcept
{
  bool good = (have_cls_spp && solved_cls_spp && have_vss_spp &&
               solved_vss_spp && solved_cls_spp == -solved_vss_spp);
  return good;
}

dir_t
Wave::resolve_no_cls_vss_to_dir(const time_t tm_vss_near,
                                const time_t tm_vss_far,
                                const time_t t_now,
                                const dir_t vss_dir) noexcept
{
  dir_t d = dir;
  if (!have_cls_spp && have_vss_spp && (tm_vss_near - t_now < 0) &&
      (tm_vss_far - t_now < 0) && (vss_dir == spot_dir))
    d = spot_dir;
  return d;
}

} // namespace Trade