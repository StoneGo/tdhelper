#include "tjm.hpp"
#include "dirs.hpp"
#include "energy.hpp"
#include "lines.hpp"
#include "make_deal.hpp"
#include "schrodinger.hpp"
#include "turn.hpp"
#include "wfw.hpp"
#include <climits>
#include <cstdint>
#include <memory>

namespace Trade {

TjmActionVector tjm;
static time_t cur_tjm_timer = 0;

bool
add_tjm_action(const dir_t d,
               const PET_ACTION pa,
               const DealCasePtr& dc,
               const time_t ts) noexcept
{
  if (ts >= cur_tjm_timer || !cur_tjm_timer) {
    for (const auto& a : tjm) {
      if (a->d == d && a->pet_action == pa)
        return false;
    }
    auto action = std::make_shared<TjmAction>();
    action->d = d;
    action->pet_action = pa;
    action->dc = dc;
    tjm.push_back(action);

    cur_tjm_timer = ts;
    return true;
  } else {
    return false;
  }
}

bool
verify_tjm_action() noexcept
{
  for (int i = 0; i < (int)(tjm.size()); ++i) {
    for (int j = i + 1; j < (int)(tjm.size()); ++j) {
      if (tjm[i]->d == -tjm[j]->d && tjm[i]->pet_action == tjm[j]->pet_action) {
        return false;
      }
    }
  }
  return true;
}

void
clear_tjm_action() noexcept
{
  tjm.clear();
}

/* ---------------------------------------------------------------------------------
int
MakeDeal::tjm_do_deal_(const event_t event, DealCase& dc) noexcept
{
  auto tcur = turns.front();

  if (!turns.tt_up || !turns.tt_dn || !tcur)
    return (0);

  int time_up = tjm_check_time__(1);
  int time_dn = tjm_check_time__(-1);
  // int space_up = tjm_check_space__(1);
  // int space_dn = tjm_check_space__(-1);

  const dir_t d = dd_ced.get_first_dir();
  int this_time = 0;
  int oppo_time = 0;

  if (d > 0) {
    this_time = (time_up) ? SPACETIME_THIS_TIME_TO : SPACETIME_THIS_TIME_GOOD;
    oppo_time = (time_dn) ? SPACETIME_OPPO_TIME_TO : SPACETIME_OPPO_TIME_GOOD;
  } else {
    this_time = (time_dn) ? SPACETIME_THIS_TIME_TO : SPACETIME_THIS_TIME_GOOD;
    oppo_time = (time_up) ? SPACETIME_OPPO_TIME_TO : SPACETIME_OPPO_TIME_GOOD;
  }

  bool this_space_good = tjm_check_space__(d);

  dc.reset();
  const dir_t turn_dir = turns.front()->dir;
  // ???
  // dir = BEAR-dir
  // ???
  if (this_time & SPACETIME_THIS_TIME_GOOD) {
    if (oppo_time & SPACETIME_OPPO_TIME_TO) {
      //
      // Time GOOD
      //
      if (this_space_good) {
        dc.case_no = 10000;
        if (turn_dir == d) {
          // Turn Dir GooD
          dc.case_no += 1000;
        } else if (turn_dir == -d) {
          dc.case_no += 2000;
        }
      }
    } else if (oppo_time & SPACETIME_OPPO_TIME_GOOD) {
      //
      // Time AmBiGoUs
      //
      if (this_space_good) {
        dc.case_no = 20000;
        //
        // Space Good
        //
        dc.case_no += 1000;
        if (turn_dir == d) {
          // Turn Dir GooD
          dc.case_no += 100;
        } else if (turn_dir == -d) {
          dc.case_no += 200;
        }
      }
    }
  } else {
    step_by_step();
  }

  int ret = 0;
  if (dc.case_no) {
    dc.dir = d;

    if (Tom.is_running()) {
      if (Tom.get_dir() == -d) {
        if (Jerry.is_running()) {
          if (Jerry.get_dir() == d) {
            // Tom And Jerry
            // Tom go to -d
            // Jerry go to d
          } else {
            // Tom And Jerry
            // And Both go to -d
          }
        } else {
          ret |= fight_jerry_run(d);
        }
      } else {
        if (Jerry.is_running()) {
          // Tom And Jerry
          // Tom go to d
          // Jerry go to ...
        }
      }
    } else {
      ret |= fight_tom_run(d);
    }
  }

  return ret;
}
--------------------------------------------------------------------- */

int
MakeDeal::tjm_jerry_reached(const uint8_t event) noexcept
{
  if (tjm_bad_turns())
    return (0);

  int ret = 0;
  return ret;
}

STRONG_LEVEL
MakeDeal::tjm_retive_strong_level(const dir_t d, case_t& cno) noexcept
{
  STRONG_LEVEL sl = STRONG_LEVEL::UNKNOW;
  if (dd_bd.get_first_dir() == -d && dd_bd.long_dir == -d) {
    if (swing.swing) {
      if (swing.dir_swing == -d) {
        sl = STRONG_LEVEL::LINE_OPPO;
        cno = 11;
      } else {
        // CHOOSE what else ???
        sl = STRONG_LEVEL::LINE_OPPO;
        cno = 12;
      }
    } else {
      sl = STRONG_LEVEL::LINE_1;
      cno = 10;
    }
  } else if (dd_bd.get_first_dir() == d && dd_bd.long_dir == d) {
    if (swing.swing) {
      if (swing.dir_swing == d) {
        sl = STRONG_LEVEL::LINE_2_2;
        cno = 21;
      } else {
        sl = STRONG_LEVEL::LINE_2_2;
        cno = 22;
      }
    } else {
      sl = STRONG_LEVEL::LINE_2;
      cno = 20;
    }
  }
  return sl;
}

bool
MakeDeal::tjm_bad_turns()
{
  auto tup = turns.tt_up;
  auto tdn = turns.tt_dn;
  auto cur = turns.first();

  return (!tup || !tdn || !cur);
}

time_t
MakeDeal::tjm_retrive_timer(const dir_t d) noexcept
{
  auto tt = turns.get_this_turn(d);
  time_t timer = 0;
  if (tt) {
    timer = (d > 0) ? tt->to_up(t_now_) : tt->to_dn(t_now_);

    if (timer < t_now_) {
      if (turns.first()->dir == d || turns.first()->fact_dir(d) == d) {
      }
    }
  }
  return timer;
}

uint8_t
MakeDeal::tjm_check_space_level(const dir_t d) noexcept
{
  uint8_t level = 0;
  if (tjm_good_entry_region(d))
    ++level;
  if (swing.swing)
    ++level;
  if (swing.dir_swing)
    ++level;
  if (dd_bd.long_dir == d)
    ++level;
  else if (dd_bd.get_second_dir() == d && dd_bd.get_second_durantion() > 90)
    ++level;
  if (dd_bd.get_first_dir() == d)
    ++level;
  if (dd_bd.get_first_dir() == d && dd_bd.long_dir == d)
    ++level;
  if (tjm_good_line_region(d))
    ++level;

  if (!swing.swing) {
    if (tjm_good_entry_region(d))
      ++level;
    if (tjm_good_line_region(d))
      ++level;
  }

  return level;
}

bool
MakeDeal::tjm_good_entry_region(const dir_t d) noexcept
{
  auto& l = fight_get_the_lines__();
  return l.good_entry_region(d, hp_now_);
}

bool
MakeDeal::tjm_good_line_region(const dir_t d) noexcept
{
  auto& l = fight_get_the_lines__();
  return l.good_line_region(d, hp_now_);
}

uint8_t
MakeDeal::tjm_tom_dont_need_reached_line2(const dir_t d) noexcept
{
  uint8_t case_no = 0;
  return case_no;
}

uint8_t
MakeDeal::tjm_tom_should_stop(const dir_t d) noexcept
{
  uint8_t case_no = 0;
  return case_no;
}

uint8_t
MakeDeal::tjm_huge_inflection(const dir_t d) noexcept
{
  if (!Tom.is_running())
    return (0);

  uint8_t case_no = 0;
  return case_no;
}

dir_t
MakeDeal::tjm_is_break_2lines() noexcept
{
  dir_t d = 0;
  return d;
}

uint16_t
MakeDeal::tjm_should_stop_jerry_as_good(const dir_t d) noexcept
{
  int ret = 0;

  // 1. Jerry is running
  // 2. Jerry is timer-outed
  if (Jerry.is_running()) {
    if (Jerry.timer_outed()) {
      // 3. the first turn has same dir with Tom
      // 4. the hpx-delta of first turn has same dir with Tom
      // 5. the hpx-delta of first turn is large
      if (turns.first()->dir == Tom.get_dir() &&
          sign(turns.first()->hp_delta) == Tom.get_dir() &&
          sign(turns.first()->im_large)) {
        ret = 1;
      }
    }
  }

  return ret;
}

uint8_t
MakeDeal::tjm_tom_reached_something() noexcept
{
  uint8_t case_no = 0;
  if (Tom.timer_outed()) {
    if (Tom.reached_line2()) {
      case_no = 1;
    } else if (Tom.reached_line1()) {
      if (tjm_tom_dont_need_reached_line2(Tom.get_dir())) {
        case_no = 2;
      }
    }
  } else {
    //
    // Something WE need close/reverse
    //
  }

  return case_no;
}

bool
MakeDeal::tjm_confused_turn_timer(const dir_t d) noexcept
{
  auto up = turns.get_this_turn(1);
  auto dn = turns.get_this_turn(-1);

  if (up->t_up.t < t_now_) {
    if (turns.first()->dir == 1 || turns.first()->hp_delta > 0)
      up = turns.first();
  }

  if (dn->t_dn.t < t_now_) {
    if (turns.first()->dir == -1 || turns.first()->hp_delta < 0)
      dn = turns.first();
  }

  bool up_up_large = false;
  bool up_dn_large = false;
  if (TO_MINUTES(up->t_up.t) > 180 && TO_MINUTES(up->t_dn.t) < 60)
    up_up_large = true;
  else if (TO_MINUTES(up->t_up.t) < 60 && TO_MINUTES(up->t_dn.t) > 180)
    up_dn_large = true;

  bool dn_up_large = false;
  bool dn_dn_large = false;
  if (TO_MINUTES(dn->t_up.t) > 180 && TO_MINUTES(dn->t_dn.t) < 60)
    dn_up_large = true;
  else if (TO_MINUTES(dn->t_up.t) < 60 && TO_MINUTES(dn->t_dn.t) > 180)
    dn_dn_large = true;

  return ((up_up_large && dn_dn_large) || (up_dn_large && dn_up_large));
}

static const char* str_close_type[] = {
  "None",        // 0
  "Min-Win",     // 1
  "Stop-Win",    // 2
  "Stop-Loss",   // 3
  "Good-To-Win", // 4
};

static int
close_cno_to_price_type(const case_t cno) noexcept
{
  int idx = 0;
  switch (cno) {
    case 30001:
    case 30002:
    case 30003:
    case 30004:
      // CLOSE_PRICE_GOOD_TO_WIN;
      idx = 4;
      break;
    case 30005:
    case 30006:
    case 30007:
      // Stop-Loss
      idx = 3;
      break;
    case 30010:
      idx = 1;
      break;
    default:
      break;
  }
  return idx;
}

/*
int
MakeDeal::tjm_add_action_set_close_part(const case_t cno) noexcept
{
  printf("Add Close part of %d position, rvn %.02lf, cno %u, for %s\n",
         Tom.get_dir(),
         Tom.closed_price.closed_rvn,
         cno,
         str_close_type[close_cno_to_price_type(cno)]);
  auto dc = std::make_shared<DealCase>();
  dc->dir = Tom.get_dir();
  dc->case_no = cno;
  dc->close = std::make_shared<ClosePrice>();

  bool good = false;
  if (cno == 30001 || cno == 30002) {
    good = tjm_cal_close_for_good_to_win(cno, dc->close);
    tjm_cal_close_for_min_to_win(cno, dc->close);
  }

  // dc->stop->set(&Tom.closed_price, cno);
  if (good && add_tjm_action(Tom.get_dir(), PET_ACTION::SET_STOP_PRICE, dc,
t_now_)) return PET_ACTION::CLOSE_PART; else return PET_ACTION::PET_NONE;
}

int
MakeDeal::tjm_add_action_set_close_all(const case_t cno) noexcept
{
  printf("Add Close All %d position, rvn %.02lf, cno %u, for %s\n",
         Tom.get_dir(),
         Tom.closed_price.closed_rvn,
         cno,
         str_close_type[close_cno_to_price_type(cno)]);
  auto dc = std::make_shared<DealCase>();
  dc->dir = Tom.get_dir();
  dc->case_no = cno;
  dc->close = std::make_shared<ClosePrice>();

  bool good = false;
  if (cno >= 30005 || cno == 30006 || cno == 30007) {
    tjm_cal_close_forced(cno, 0, false, dc->close);
  }

  if (good && add_tjm_action(dc->dir, PET_ACTION::SET_STOP_PRICE, dc, t_now_))
    return PET_ACTION::CLOSE_ALL;
  else
    return PET_ACTION::PET_NONE;
}
*/

int
MakeDeal::tjm_add_action_set_good_to_win(const case_t cno) noexcept
{
  printf("Add Close part of %d, good-to-win, cno %u\n", Tom.get_dir(), cno);
  auto dc = std::make_shared<DealCase>();
  dc->dir = Tom.get_dir();
  dc->case_no = cno;
  dc->close = std::make_shared<ClosePrice>();

  bool good = tjm_cal_close_for_good_to_win(cno, dc->close);
  // tjm_cal_close_for_min_to_win(cno, dc->close);
  if (good &&
      add_tjm_action(Tom.get_dir(), PET_ACTION::SET_CLOSE_PART, dc, t_now_))
    return PET_ACTION::CLOSE_PART;
  else
    return PET_ACTION::PET_NONE;
}

int
MakeDeal::tjm_add_action_set_stop_loss(const case_t cno) noexcept
{
  printf("Add Close All %d Forced-Stop, cno %u\n", Tom.get_dir(), cno);
  auto dc = std::make_shared<DealCase>();
  dc->dir = Tom.get_dir();
  dc->case_no = cno;
  dc->close = std::make_shared<ClosePrice>();

  bool good = tjm_cal_close_forced(cno, 0, false, dc->close);

  if (good && add_tjm_action(dc->dir, PET_ACTION::SET_CLOSE_ALL, dc, t_now_))
    return PET_ACTION::CLOSE_ALL;
  else
    return PET_ACTION::PET_NONE;
}

int
MakeDeal::tjm_add_action_set_min_to_win(const case_t cno) noexcept
{
  printf("Add Close All %d Min-To-Win, cno %u\n", Tom.get_dir(), cno);
  auto dc = std::make_shared<DealCase>();
  dc->dir = Tom.get_dir();
  dc->case_no = cno;
  dc->close = std::make_shared<ClosePrice>();

  bool good = tjm_cal_close_for_min_to_win(cno, dc->close);

  if (good && add_tjm_action(dc->dir, PET_ACTION::SET_CLOSE_PART, dc, t_now_))
    return PET_ACTION::CLOSE_ALL;
  else
    return PET_ACTION::PET_NONE;
}

int
MakeDeal::tjm_add_action_set_stop_win(const case_t cno) noexcept
{
  printf("Add Close Part of %d Stop-Win, cno %u\n", Tom.get_dir(), cno);
  auto dc = std::make_shared<DealCase>();
  dc->dir = Tom.get_dir();
  dc->case_no = cno;
  dc->close = std::make_shared<ClosePrice>();

  bool good = tjm_cal_close_for_stop_win(cno, dc->close);

  if (good && add_tjm_action(dc->dir, PET_ACTION::SET_CLOSE_PART, dc, t_now_))
    return PET_ACTION::CLOSE_PART;
  else
    return PET_ACTION::PET_NONE;
  return (0);
}

int
MakeDeal::tjm_add_action_cancel_seted_close(const case_t cno,
                                            const closed_t close_type) noexcept
{
  auto dc = std::make_shared<DealCase>();
  dc->dir = Tom.get_dir();
  dc->case_no = cno;
  dc->cancel_close = close_type;

  if (add_tjm_action(dc->dir, PET_ACTION::SET_CANCEL_CLOSE, dc, t_now_))
    return PET_ACTION::SET_CANCEL_CLOSE;
  return PET_ACTION::PET_NONE;
}

bool
MakeDeal::tjm_space_is_good(const dir_t d) const noexcept
{
  bool yes = false;
  if (d > 0) {
    if (lines.nup == 2) {
      return (hp_now_ - lines.hp_up_1 < 15 && hp_now_ - lines.hp_up_2 < 30);
    }
  } else {
    if (lines.ndn == 2) {
      return (hp_now_ - lines.hp_dn_1 > 15 && hp_now_ - lines.hp_dn_2 > 30);
    }
  }
  return yes;
}

case_t
MakeDeal::tjm_tom_reached_limit() noexcept
{
  case_t cno = 0;
  if (Tom.amout_to > 0 && Tom.reached_line2()) {
    cno = 1;
  } else if (Tom.amout_to > 0 && Tom.reached_line1()) {
    if (!tjm_should_reache_line2(Tom.get_dir())) {
      cno = 2;
    }
  } else if (Tom.amout_to > 1 && Tom.get_durantion(t_now_) > 180) {
    cno = 3;
  }
  return cno;
}

int
MakeDeal::tjm_run_jerry(const case_t cno) noexcept
{
  int ret = 0;
  // TODO select a timer for Jerry
  if (!Jerry.is_running() || Jerry.get_dir() != Tom.get_dir()) {
    ret = tjm_add_action_jerry_run(Tom.get_dir(), cno);
  }
  return ret;
}

case_t
MakeDeal::tjm_should_run_jerry() noexcept
{
  auto reached_cno = tjm_tom_reached_limit();
  case_t cno = 0;
  if (tjm_cno & TJM_SOLVED_SPP_DIR_WITH_TIMER) {
    if (reached_cno) {
      cno = (tjm_cno << 2) | reached_cno; // 19
    }
  } else {
    if (Tom.incomming.d_big_reverse &&
        Tom.incomming.d_big_reverse == -Tom.get_dir()) {
      // !! Big Reverse !!
      cno = 32;
    } else if (Tom.incomming.rvn_single < CLOSE_REVENUE_LOSS_ONE_THIRD) {
      // !! Loss 1/3 of 1000 !!
      cno = 64;
    }
  }
  return cno;
}

case_t
MakeDeal::tjm_check_ambiguous_dirs(const dir_t timer_dir,
                                   const dir_t space_dir,
                                   const case_t oppo_cno) noexcept
{

  if (!timer_dir)
    return (0);

  case_t cno = 0;
  //
  // 1. turns going up and going down.
  //
  if (timer_dir == -10) {
    cno |= 1;
  }

  // 1. CED = -d
  // 2. Big Dir = -d
  // 3. pdc = 0000
  if (dd_ced.get_first_dir() == -timer_dir &&
      dd_bd.get_first_dir() != timer_dir && pdc.no_mid_ein()) {

    // 4. ced is swing
    // 5. force is swing
    // 6. enery is swing
    if (dd_ced.is_swing_dir(t_now_) && dd_ft.is_swing_dir(t_now_) &&
        dd_em.is_swing_dir(t_now_)) {
      cno |= 2;
    }

    // 4. CLS is opposite
    // OR
    // 5. VSS is opposite
    if ((wave.solved_cls_spp == -timer_dir &&
         wave.solved_vss_spp != timer_dir &&
         wfw.tm_cls_near - t_now_ > MINUTES_30) ||
        (wave.solved_vss_spp == -timer_dir &&
         wave.solved_cls_spp != timer_dir &&
         wfw.tm_vss_far - t_now_ > MINUTES_30)) {
      cno |= 4;
    }
  }

  // 1. CED = -d
  // 2. FT = -d
  // 3. pdc = 0000
  if (dd_ced.get_first_dir() == -timer_dir &&
      dd_ft.get_first_dir() == -timer_dir && pdc.no_mid_ein()) {
    // 4. CLS is opposite
    // OR
    // 5. VSS is opposite
    if ((wave.solved_cls_spp == -timer_dir &&
         wave.solved_vss_spp != timer_dir &&
         wfw.tm_cls_near - t_now_ > MINUTES_30) ||
        (wave.solved_vss_spp == -timer_dir &&
         wave.solved_cls_spp != timer_dir &&
         wfw.tm_vss_far - t_now_ > MINUTES_30)) {
      cno |= 8;
    }
  }

  dir_t turns_space_dir = turns.space_dir(t_now_, hp_now_);
  if (!turns_space_dir || turns_space_dir == -timer_dir) {
    cno |= 16;
  }

  if (timer_dir != space_dir && timer_dir && timer_dir != -10 && space_dir) {
    cno |= 32;
  }

  return cno;
}

case_t
MakeDeal::tjm_timer_dir_is_opposite(const dir_t d) noexcept
{
  case_t c_oppo = 0;
  if (d == swing.dir_swing &&
      (dd_bd.is_swing_dir(t_now_) ||
       (d == dd_bd.first_dir() && d == dd_bd.long_dir)) &&
      tjm_space_is_good(-d) && dd_ced.long_time_is(-d, t_now_) &&
      dd_ft.long_time_is(-d, t_now_) && dd_em.long_time_is(-d, t_now_)) {

    // 1. D = swing
    // 2. D = big-dir and long-big-dir
    // 3. -D space is good
    // 4. -D = CED
    // 5. -D = FT
    // 6. -d = EM

    // 3-30 g15057550 15057550

    c_oppo |= 1;
  }

  if (swing.dir_swing == -d && dd_bd.weak_big_dir(-d, t_now_) &&
      dd_ced.get_first_dir() == -d && dd_ced.get_first_durantion(t_now_) > 40 &&
      (lines.good_line_region(-d, hp_now_) ||
       lines.good_entry_region(d, hp_now_))) {
    // 16   g25057197 25057197

    // 1. -D = swing
    // 2. -D = ced
    // 3. good line region
    c_oppo |= 2;
  }

  if (dd_ced.get_first_dir() == -d) {
    if (dd_bd.get_first_dir() == -d) {
      // 1. CED = -d
      // 2. Big Dir = -d
      // But NOT
      // 3. pdc = 0000
      // 4. ced is swing
      // 5. force is swing
      // 6. enery is swing

      if (!(pdc.no_mid_ein() && dd_ced.is_swing_dir(t_now_) &&
            dd_ft.is_swing_dir(t_now_) && dd_em.is_swing_dir(t_now_))) {
        c_oppo |= 4;
      }
    }
  }

  if (d == -10 && swing.swing && dd_bd.is_swing_dir(t_now_)) {
    c_oppo |= 8;
  }

  if (d5.wave == -d && d5.ext == -d) {
    if ((dd_bd.get_first_dir() != d || dd_bd.is_swing_dir(t_now_) ||
         swing.dir_swing == -d) &&
        dd_ced.get_first_dir() == -d &&
        ((d < 0 && em_up.force_case > 1000) ||
         (d > 0 && em_dn.force_case > 1000)) &&
        dd_em.get_first_dir() == -d &&
        Tom.incomming.max_win_rvn - Tom.incomming.max_loss_rvn > 30) {

      if ((wave.solved_cls_spp == -d && wave.solved_vss_spp != d &&
           wfw.tm_cls_near - t_now_ > MINUTES_45) ||
          (wave.solved_vss_spp == -d && wave.solved_cls_spp != d &&
           wfw.tm_vss_far - t_now_ > MINUTES_45)) {
        c_oppo |= 16;
      } else if ((wave.solved_cls_spp == -d && wave.solved_vss_spp == d &&
                  wfw.tm_cls_near - t_now_ > MINUTES_45 &&
                  wfw.tm_vss_far < t_now_) ||
                 (wave.solved_vss_spp == -d && wave.solved_cls_spp == d &&
                  wfw.tm_vss_far - t_now_ > MINUTES_45 &&
                  wfw.tm_cls_near < t_now_)) {
        c_oppo |= 16;
      }
    }
  }

  // if (swing.swing && d5.wave == -d && d5.ext == -d) {
  //   if (dd_ced.get_first_dir() == -d && dd_ced.get_first_dir() == -d &&
  //       ((d < 0 && em_up.force_case > 1000) ||
  //        (d > 0 && em_dn.force_case > 1000)) &&
  //       dd_em.get_first_dir() == -d) {
  //     if ((wave.solved_cls_spp == -d && wave.solved_vss_spp != d &&
  //          wfw.tm_cls_near - t_now_ > MINUTES_45) ||
  //         (wave.solved_vss_spp == -d && wave.solved_cls_spp != d &&
  //          wfw.tm_vss_far - t_now_ > MINUTES_45)) {
  //       c_oppo |= 32;
  //     } else if ((wave.solved_cls_spp == -d && wave.solved_vss_spp == d &&
  //                 wfw.tm_cls_near - t_now_ > MINUTES_45 &&
  //                 wfw.tm_vss_far < t_now_) ||
  //                (wave.solved_vss_spp == -d && wave.solved_cls_spp == d &&
  //                 wfw.tm_vss_far - t_now_ > MINUTES_45 &&
  //                 wfw.tm_cls_near < t_now_)) {
  //       c_oppo |= 32;
  //     }
  //   }
  // }

  return c_oppo;
}

bool
MakeDeal::tjm_should_reache_line2(const dir_t d) noexcept
{
  case_t cno = 0;
  if (dd_bd.is_swing_dir(t_now_)) {
    if (dd_bd.first_dir() == d && dd_bd.long_dir == -d) {
      cno = 3;
    } else if (dd_bd.first_dir() != d && dd_bd.long_dir == -d) {
      cno = 4;
    } else if (dd_bd.first_dir() == d && dd_bd.long_dir == d) {
      cno = 5;
    } else if (dd_bd.first_dir() != d && dd_bd.long_dir == d) {
      cno = 6;
    }
  } else {
    if (dd_bd.first_dir() == d && dd_bd.long_dir == d) {
      cno = 1;
    } else if (dd_bd.first_dir() != -d && dd_bd.long_dir == d) {
      cno = 2;
    } else if (dd_bd.first_dir() == d) {
      cno = 7;
    }
  }

  return (cno != 0);
}

int
MakeDeal::tjm_check_prerequisite(const dir_t deal_dir) noexcept
{
  if (d5.wave != deal_dir && d5.ext != deal_dir)
    return (0);

  auto space_level = tjm_check_space_level(deal_dir);
  if (space_level > 0) {
    double hp_base = 0;
    auto good = tjm_get_entry_price(deal_dir, hp_base);

    if (good)
      return 1;
  }

  return 0;
}

bool
MakeDeal::tjm_get_entry_price(const dir_t deal_dir, double& hp_base) noexcept
{
  // if (-deal_dir == dd_bd.first_dir() && !dd_bd.is_swing_dir(t_now_) &&
  //     -deal_dir == dd_bd.long_dir) {
  //   hp_base = hp_now_ - deal_dir * 20;
  // } else {
  //   hp_base = hp_now_;
  // }

  hp_base = hp_now_;
  return true;
}

bool
MakeDeal::tjm_get_entry_price_from_lines(const dir_t deal_dir,
                                         const Lines& lines,
                                         double& hp_base) noexcept
{
  hp_base = 0;
  if ((swing.swing == deal_dir && swing.reached_line2(-deal_dir)) ||
      (-deal_dir == dd_bd.get_first_dir() && -deal_dir == dd_bd.long_dir)) {
    if (deal_dir > 0) {
      // check down-line
      if (swing.target.lines.hp_dn_2 < hp_now_ - 20) {
        hp_base = swing.target.lines.hp_dn_2;
      } else {
        hp_base = hp_now_ - 20;
      }
    } else {
      // check up-line
      if (swing.target.lines.hp_up_2 > hp_now_ + 20) {
        hp_base = swing.target.lines.hp_up_2;
      } else {
        hp_base = hp_now_ + 20;
      }
    }
  } else {
    hp_base = hp_now_;
  }

  if (std::abs(hp_base - Tom.os_entries.hp_base) > 4.9) {
    printf(
      "!!! The Base of Price is %.02lf For Dir %d !!!\n", hp_base, deal_dir);
    return true;
  } else {
    return false;
  }
}

// int
// MakeDeal::tjm_set_stop_price(const CALCULATE_STOP_TYPE hpx_type,
//                              const PET_NAME pet,
//                              const STOP_TYPE stop_type,
//                              const case_t cno) noexcept
// {
//   return (0);
// }

#define FORCED_HP_PRICE -150
#define CLOSE_FOR_NOT_BAD 8

int
MakeDeal::tjm_add_action_close_part(
  const ClosePrice* const close_price) noexcept
{
  auto dc = std::make_shared<DealCase>(Tom.get_dir(), 50001);
  dc->close = std::make_shared<ClosePrice>(close_price);
  if (add_tjm_action(0, PET_ACTION::CLOSE_PART, dc, t_now_)) {
    return PET_ACTION::CLOSE_PART;
  } else {
    return PET_ACTION::PET_NONE;
  }
}

int
MakeDeal::tjm_add_action_close_all(const ClosePrice* const close_price) noexcept
{
  auto dc = std::make_shared<DealCase>(Tom.get_dir(), 50001);
  dc->close = std::make_shared<ClosePrice>(close_price);
  if (add_tjm_action(0, PET_ACTION::CLOSE_ALL, dc, t_now_)) {
    return PET_ACTION::CLOSE_ALL;
  } else {
    return PET_ACTION::PET_NONE;
  }
}

int
MakeDeal::tjm_tom_reached_close_price(const event_t event) noexcept
{
  int ret = 0;

  auto stop_type = Tom.close_price.hited_close_type;
  //
  // if I want to close, then
  // add action
  //
  if (stop_type & HITED_CLOSE_ALL) {
    //
    // TODO should I close all position ??
    //
  } else if (stop_type & HITED_CLOSE_PARTIALLY) {
    ret |= tjm_add_action_close_part(&Tom.close_price);
  }

  return ret;
}

int
MakeDeal::tjm_tom_reached_open_price(const event_t event) noexcept
{
  int ret = 0;

  if (tjm_dir == Tom.get_dir() || tjm_dir == 2) {
    Tom.open_price(idx__, t_now_, hp_now_);
    Tom.incomming.update_revenue(hp_now_,
                                 Tom.get_dir(),
                                 Tom.os_entries.average,
                                 Tom.os_entries.amount_reached,
                                 t_now_,
                                 idx__);
    tjm_action_tom_open(Tom.get_dir(), Tom.os_entries.hp_cur_open);
  } else {
    //
    // CHOOSE Should Stop ??
    //
    tjm_add_action_tom_stop(TJM_CASENO_REACHED_OPEN_BUT_OPPOSITE);
  }
  return ret;
}

dir_t
MakeDeal::tjm_good_dir_case(case_t& good_case_no) noexcept
{
  dir_t dir = 0;
  if ((swing.dir_swing == wfw.cls_dir) &&
      (wfw.tm_cls_near - t_now_ > MINUTES_30) &&
      (swing.dir_swing = wfw.vss_dir) &&
      wave.solved_cls_spp != -swing.dir_swing &&
      dd_ced.get_first_dir() == swing.dir_swing &&
      swing.reached_line2(-swing.dir_swing) &&
      dd_em.get_first_dir() == swing.dir_swing &&
      dd_ft.get_first_dir() == swing.dir_swing &&
      turns.most_weak_time(swing.dir_swing) &&
      swing.target.lines.good_entry_region(swing.dir_swing, hp_now_)) {
    dir = swing.dir_swing;
    good_case_no = 7000;
  }

  auto good_dir = [&](const dir_t d) noexcept -> dir_t {
    dir_t td = 0;
    if (turns.first()->dir == d || sign(turns.first()->hp_delta) == d) {
      if ((wave.solved_cls_spp == d && wave.solved_vss_spp != -d &&
           wfw.tm_cls_near - t_now_ > MINUTES_45) ||
          (wave.solved_vss_spp == d && wave.solved_cls_spp != -d &&
           wfw.tm_vss_far - t_now_ > MINUTES_45)) {
        td = d;
        good_case_no = 7100;
      } else if ((wave.solved_cls_spp == d && wave.solved_vss_spp == -d &&
                  wfw.tm_cls_near - t_now_ > MINUTES_45 &&
                  wfw.tm_vss_far < t_now_) ||
                 (wave.solved_vss_spp == d && wave.solved_cls_spp == -d &&
                  wfw.tm_vss_far - t_now_ > MINUTES_45 &&
                  wfw.tm_cls_near < t_now_)) {
        td = d;
        good_case_no = 7101;
      }
    }
    return td;
  };

  auto d_up = good_dir(1);
  auto d_dn = good_dir(-1);
  dir_t my_dir = 0;
  if ((d_up && d_dn) || (!d_up && !d_dn)) {
    my_dir = 0;
  } else {
    my_dir = d_up ? 1 : -1;
  }

  if (dir && my_dir) {
    if (dir != my_dir)
      dir = 0;
  } else if (!dir && !my_dir) {
    dir = 0;
  } else {
    dir = (dir ? dir : my_dir);
  }

  return dir;
}

DIR_TYPE
MakeDeal::tjm_estimate_dir(case_t& case_no) noexcept
{
  if (!dir_turns_space)
    return DIR_NONE;

  auto a = tjm_ced_is_going_to(dir_turns_space);
  auto b = tjm_force_is_going_to(dir_turns_space);
  auto c = tjm_swing_is_going_to(dir_turns_space);
  auto d = tjm_big_dir_is_going_to(dir_turns_space);
  auto a_oppo = tjm_ced_is_going_to(-dir_turns_space);
  auto b_oppo = tjm_force_is_going_to(-dir_turns_space);
  auto c_oppo = tjm_swing_is_going_to(-dir_turns_space);
  auto d_oppo = tjm_big_dir_is_going_to(-dir_turns_space);

  if (a && b && c && !d && !a_oppo && !b_oppo && (c_oppo == DIR_SWING) &&
      d_oppo) {
    //
    // - case 1:
    //   - ced, force, swing is goting to dir_turns_space
    //   - big_dir is going to the opposite
    // * TEST 16,
    dv_get_dir.emplace_back(dir_turns_space, 1);
  } else if (!a && !b && (c_oppo == DIR_SWING) && d && a_oppo && b_oppo &&
             c_oppo && !d_oppo) {
    //
    // - case 2:
    //   - ced, force, swing is goting to -dir_turns_space
    //   - big_dir is going to the dir_turns_space
    //
    dv_get_dir.emplace_back(-dir_turns_space, 2);
  }

  if (tjm_solved_spp_dir_with_timer(dir_turns_space)) {
    dv_get_dir.emplace_back(dir_turns_space, TJM_SOLVED_SPP_DIR_WITH_TIMER);
  }

  if (pdc.no_mid_ein()) {
    dv_get_dir.emplace_back(dir_turns_space, 8);
  }

  if (dt_dir_zzzz_and_gz__()) {
  }

  if (!wave.solved_cls_spp && wfw.tm_vss_far < t_now_ &&
      wfw.tm_vss_near < t_now_ && swing.swing) {
    dv_get_dir.emplace_back(dir_turns_space, 16);
  }

  if (dd_sd.is_swing_dir(t_now_)) {
    if (!dd_bd.is_swing_dir(t_now_)) {
      if (dd_bd.get_first_dir() == -dir_turns_space) {
        dv_get_dir.emplace_back(0, 32);
      }
    }
  } else {
    dv_get_dir.emplace_back(dir_turns_space, 64);
  }

  //
  // Check All space dir
  //

  DIR_TYPE dt = DIR_NONE;
  if (dv_get_dir.empty())
    return dt;

  const dir_t d_first = dv_get_dir.front().d;
  for (const auto& dir : dv_get_dir) {
    if (d_first != dir.d)
      dt = DIR_SWING;
  }

  if (!dt) {
    dt = (d_first > 0) ? DIR_UP : DIR_DN;

    if (dt) {
      for (const auto& dir : dv_get_dir) {
        tjm_cno |= dir.cno;
      }
    }
  }

  return dt;
}

DIR_TYPE
MakeDeal::tjm_ced_is_going_to(const dir_t d) noexcept
{
  DIR_TYPE dt = DIR_NONE;
  if (dd_ced.is_swing_dir(t_now_)) {
    dt = DIR_SWING;
  } else if (dd_ced.get_first_dir() == d) {
    dt = (d > 0) ? DIR_UP : DIR_DN;
  }

  return dt;
}

DIR_TYPE
MakeDeal::tjm_force_is_going_to(const dir_t d) noexcept
{
  DIR_TYPE dt_ft = DIR_NONE;
  if (dd_ft.is_swing_dir(t_now_)) {
    dt_ft = DIR_SWING;
  } else if (dd_ft.get_first_dir() == d) {
    dt_ft = (d > 0) ? DIR_UP : DIR_DN;
  }

  DIR_TYPE dt_em = DIR_NONE;
  if (dd_em.is_swing_dir(t_now_)) {
    dt_em = DIR_SWING;
  } else if (dd_em.get_first_dir() == d) {
    dt_em = (d > 0) ? DIR_UP : DIR_DN;
  }

  if (dt_ft == dt_em)
    return dt_ft;
  else
    return DIR_NONE;
}

DIR_TYPE
MakeDeal::tjm_swing_is_going_to(const dir_t d) noexcept
{
  DIR_TYPE dt = DIR_NONE;
  if (swing.swing) {
    if (swing.dir_swing == d)
      dt = (d > 0) ? DIR_UP : DIR_DN;
    else
      dt = DIR_SWING;
  }

  return dt;
}

DIR_TYPE
MakeDeal::tjm_big_dir_is_going_to(const dir_t d) noexcept
{
  DIR_TYPE dt = DIR_NONE;
  if (dd_bd.is_swing_dir(t_now_)) {
    dt = DIR_SWING;
  } else if (dd_bd.get_first_dir() == d) {
    dt = (d > 0) ? DIR_UP : DIR_DN;
  }

  return dt;
}

bool
MakeDeal::tjm_good_entry_price(const dir_t d,
                               double& hp_base,
                               case_t& case_no) noexcept
{
  auto good_opposite_line2 = [&](const Lines& l) noexcept -> bool {
    //
    // CHECK the opposite line 2, MAX LOST
    //
    bool y = false;
    if (d > 0) {
      // -------------------------------------------- up line 2
      //
      // -------------------------------------------- up line 1
      //
      //
      // ** hp_now_   +++
      // --------------|----------------------------- dn line 1
      //               | MAX_OPENING_LOSS_LINE_2
      // --------------+----------------------------- dn line 2
      //
      // (** hp_now_ here is good also)
      if (l.ndn == 2 && (hp_now_ - l.hp_dn_2 < MAX_OPENING_LOSS_LINE_2)) {
        y = true;
      }
    } else {
      //
      // d < 0
      //
      // (** hp_now_ here is good also)
      // --------------+----------------------------- up line 2
      //               | MAX_OPENING_LOSS_LINE_2
      // --------------+----------------------------- up line 1
      // ** hp_now_   +++
      //
      //
      // -------------------------------------------- dn line 1
      //
      // -------------------------------------------- dn line 2
      //
      if (l.nup == 2 && (l.hp_up_2 - hp_now_ < MAX_OPENING_LOSS_LINE_2)) {
        y = true;
      }
    }
    return y;
  };

  auto good_tjm_dir_case = [&]() noexcept -> bool {
    //
    // The Wave.solved_spp_dir Or solved_cls_dir has explicit dir and timer
    // to retrive this, call the function tjm_solved_spp_dir_with_timer
    //
    return (tjm_cno & TJM_SOLVED_SPP_DIR_WITH_TIMER);
  };

  auto reached_the_opposite_line2 = [&]() noexcept -> bool {
    //
    // d < 0
    // --------------*----------------------------- up line 2
    //               Hited the opposite line2
    // --------------*----------------------------- up line 1
    //
    // -------------------------------------------- dn line 1
    // -------------------------------------------- dn line 2
    //
    return (Tom.is_running() && ((d < 0 && (r_tom & LIMIT_REACHED::UP_2)) ||
                                 (d > 0 && (r_tom & LIMIT_REACHED::DN_2))));
  };

  auto have_line2 = [](const dir_t d, const Lines& l) noexcept -> bool {
    return ((d > 0 && l.nup) || (d < 0 && l.ndn));
  };

  case_no = 0;
  const auto& l = (swing.swing) ? swing.target.lines : lines;
  if (good_opposite_line2(l)) {
    case_no |= PRICE_CNO_GOOD_OPPOSITE_LINE2;
  }

  if (good_tjm_dir_case()) {
    case_no |= 2;
  }

  if (reached_the_opposite_line2()) {
    case_no |= 4;
  }

  if (!case_no && have_line2(d, l) && tjm_should_reache_line2(d) &&
      swing.swing) {
    case_no |= PRICE_CNO_NOT_GOOD_BUT_GOOD;
  }

  bool yes = (case_no != 0);
  if (yes)
    hp_base = hp_now_;

  return yes;
}

int
MakeDeal::tjm_make_deal(const event_t event [[maybe_unused]]) noexcept
{
  int ret = 0;
  if (TJM_ONE_DIR(tjm_dir)) {
    if (Tom.is_running()) {
      if ((Tom.get_dir() == -tjm_dir) || (event & EVENT_BIG_NEEDLE_OCCURED)) {
        ret = tjm_jerry_jerry(-Tom.get_dir());
      } else {
        ret = tjm_try_set_close_price(tjm_dir);
      }
    } else {
      double hp_base = 0;
      case_t hp_base_cno = 0;
      auto good_price = tjm_good_entry_price(tjm_dir, hp_base, hp_base_cno);
      if (good_price) {
        ret =
          tjm_add_action_tom_run(tjm_dir, tjm_cno, hp_base, hp_base_cno, true);
      }
    }
  } else if (TJM_TWO_DIR(tjm_dir)) {
    ret = tjm_jerry_jerry(-Tom.get_dir());
  } else {
    ret = tjm_try_set_close_price(tjm_dir);
  }

  return ret;
}

int
MakeDeal::tjm_jerry_jerry(const dir_t d) noexcept
{
  // Current direction is not going to d
  // so, we ignore it
  if (!EVENT_BIG_NEEDLE_OCCURED && d5.wave != d && d5.ext != d) {
    return (0);
  }

  int jret = 0;
  if (Jerry.is_running()) {
    if (Jerry.get_dir() == -d) {
      auto cno = tjm_tom_should_stop_or_reverse(d);
      if (cno) {
        if (cno < 10000) {
          // jret = tjm_add_action_jerry_stop(cno);
          jret = tjm_add_action_set_stop(cno);
        } else {
          // go to -Tom.dir
          // jret = tjm_add_action_jerry_stop(cno);
          jret = tjm_add_action_set_reverse(cno);
        }
      } else {
        // BigNeedle is against Tom
        if (Tom.incomming.d_big_reverse == d) {
          if (dirs.big_dir != Tom.get_dir()) {
            tjm_add_action_set_stop_loss(30007);
          } else {
            tjm_add_action_set_min_to_win(30010);
          }
        }
      }
    } else {
      // ERROR
      if (Jerry.get_dir() != Tom.get_dir()) {
        printf("Err, Jerry not to Tom! -> %d %d -> %ld\n",
               Tom.get_dir(),
               Jerry.get_dir(),
               idx__);
      }
    }
  } else {
    //
    // RUN JERRY
    //
    // 0. the deal-dir was opposite of Tom
    // 1. Tom reached line1 and to
    // 2. set the force-stop
    //
    auto jerry_cno = tjm_should_run_jerry();
    if (jerry_cno) {
      jret = tjm_run_jerry(jerry_cno);
    }
  }

  return jret;
}

bool
MakeDeal::tjm_solved_spp_dir_with_timer(const dir_t d) noexcept
{
  bool yes = false;
  if (d5.wave == d && d5.ext == d) {
    if (wave.solved_cls_spp == d && wave.solved_vss_spp == d &&
        wfw.tm_cls_near - t_now_ >= MINUTES_30 &&
        wfw.tm_vss_far - t_now_ >= MINUTES_30) {
      yes = true;
    } else if ((wave.solved_cls_spp == d && wave.solved_vss_spp != -d &&
                wfw.tm_cls_near - t_now_ >= MINUTES_30) ||
               (wave.solved_vss_spp == d && wave.solved_cls_spp != -d &&
                wfw.tm_vss_far - t_now_ >= MINUTES_30)) {
      yes = true;
    } else if ((wave.solved_cls_spp == d && wave.solved_vss_spp == -d &&
                wfw.tm_cls_near - t_now_ >= MINUTES_30 &&
                wfw.tm_vss_far < t_now_) ||
               (wave.solved_vss_spp == d && wave.solved_cls_spp == -d &&
                wfw.tm_vss_far - t_now_ >= MINUTES_30 &&
                wfw.tm_cls_near < t_now_)) {
      yes = true;
    }
  }
  return yes;
}

int
MakeDeal::tjm_try_set_close_price(const dir_t d) noexcept
{
  return (0);
}

void
MakeDeal::tjm_jerry_reset() noexcept
{
  Jerry.clear_reacheded();
  if (Jerry.target.timer.t > Tom.target.timer.t)
    Jerry.update_timer(Tom.target.timer.t, Tom.target.timer.t);
}

case_t
MakeDeal::tjm_tom_should_stop_or_reverse(const dir_t d) noexcept
{
  // Verify the direction of Tom and jeery
  if (d == Tom.get_dir() || d == Jerry.get_dir() ||
      Tom.get_dir() != Jerry.get_dir()) {
    // wrong dir
    return (0);
  }

  case_t cno = 0;
  if (Jerry.timer_outed()) {
    if (tjm_should_reache_line2(-d)) {

    } else {
      if (Tom.get_durantion(t_now_) >= (60 * 4)) {
        if (tjm_solved_spp_dir_with_timer(d)) {
          if (tjm_should_reache_line2(d)) {
            //
            // 1. Jerry Timer Outed
            // 2. Tom Running Durantion > 4 Hours
            // 3. Confirmed Forecast Direction And Timer
            // 4. The Big-Dir is d that was opposite of Tom
            //
            cno = 1;
          }
        }
      }
    }
  }

  if (cno)
    cno += 10000;

  //
  // 10000: reverse the Tom
  //

  return cno;
}

bool
MakeDeal::tjm_estimate_tolerate() noexcept
{
  //
  //
  // IMPORTANT If Jerry is NOT running, Then Something wrong.
  //
  //
  if (!Tom.is_running() || !Jerry.is_running())
    return false;

  auto should_check_stop_price = [&]() {
    bool should_update = false;
    if (tjm_dir == -Tom.get_dir()) {
      if (Tom.incomming.rvn_single < CLOSE_REVENUE_TOLERATE_REVENUE) {
        should_update = true;
      }
    } else {
      if (Tom.incomming.rvn_single < CLOSE_REVENUE_LOSS_ONE_THIRD) {
        should_update = true;
      }
    }

    return should_update;
  };

  bool updated = false;
  if (should_check_stop_price()) {
    //
    //
    //
  }
  return updated;
}

bool
MakeDeal::tjm_cal_close_forced(const case_t cno,
                               const case_t hp_base_cno,
                               const bool just_run,
                               const ClosePricePtr& close_price) noexcept
{
  if (tjm_cno & TJM_SOLVED_SPP_DIR_WITH_TIMER) {
    //
    // TODO Calculate The  price
    //
  } else {
    close_price->set_stop_loss(CLOSE_REVENUE_FOOL_FORCED, true);
  }
  return true;
}

bool
MakeDeal::tjm_cal_close_for_good_to_win(
  const case_t cno,
  const ClosePricePtr& close_price) noexcept
{
  Lines* l = nullptr;
  dir_t d = 0;
  if (cno == 30001 || cno == 30002) {
    if (cno == 30001) {
      l = &Tom.target.lines;
      d = Tom.get_dir();
    } else {
      l = &Jerry.target.lines;
      d = Jerry.get_dir();
    }
  } else {
    return false;
  }

  double hp_to_win = 50;
  if (d > 0) {
    if (l->hp_up_2 < (hp_now_ + 10)) {
      hp_to_win = hp_now_ + 10;
    } else {
      hp_to_win = l->hp_up_2;
    }
  } else {
    if (l->hp_dn_2 > (hp_now_ - 10)) {
      hp_to_win = hp_now_ - 10;
    } else {
      hp_to_win = l->hp_dn_2;
    }
  }

  //
  // TODO Calculate The good-to-win price
  //

  close_price->set_good_to_win(Tom.incomming.try_rvn(hp_to_win, Tom.get_dir()),
                               true);

  return true;
}

bool
MakeDeal::tjm_cal_close_for_min_to_win(
  const case_t cno,
  const ClosePricePtr& close_price) noexcept
{

  if (Tom.incomming.rvn_single > CLOSE_REVENUE_MIN_TO_WIN) {
    double rvn_min_win = CLOSE_REVENUE_MIN_TO_WIN;

    //
    // TODO Calculate The min-to-win price
    //

    close_price->set_min_win(rvn_min_win, true);
    return true;
  }

  return false;
}

bool
MakeDeal::tjm_cal_close_for_stop_win(const case_t cno,
                                     const ClosePricePtr& close_price) noexcept
{
  return false;
}

} // namespace Trade
