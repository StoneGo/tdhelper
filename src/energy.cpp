#include "energy.hpp"
#include "lines.hpp"
#include <cstdint>
#include <cstdio>
#include <sys/types.h>

namespace Trade {

void
EnergyChanged::print() const noexcept
{
  printf(" ~~~ Changed %d <== %u %u %u, sum %u\n",
         changed,
         time_amount,
         force_amount,
         g2_amount,
         all_amout);
}

uint8_t
EnergyChanged::change_case_no() const noexcept
{
  if (!changed)
    return (0);

  uint8_t case_no = 0;
  if (all_changed())
    case_no = 1;
  else if (time_force_big_changed())
    case_no = 2;
  else if (time_amount && force_amount)
    case_no = 3;

  return case_no;
}

bool
EnergyChanged::all_changed() const noexcept
{
  return (time_amount && force_amount && g2_amount);
}

bool
EnergyChanged::time_force_big_changed() const noexcept
{
  return (time_amount >= 2 && force_amount >= 3);
}

uint32_t
EnergyMark::energy_case() noexcept
{
  force_case = direction_case();
  if (force_case && have_timer())
    force_case += 1000;
  return force_case;
}

bool
EnergyMark::have_timer() const noexcept
{
  return (timing_case > 0);
}

uint8_t
EnergyMark::have_force() const noexcept
{
  return (force_case > 0);
}

uint32_t
EnergyMark::direction_case() const noexcept
{
  uint32_t case_no = 0;
  if (same_spp_dir && same_wave_dir && same_mid_ein_dir) {
    if (same_g2_dir) {
      case_no = 1;
    } else if (two_g2_1st_to) {
      case_no = 2;
    } else {
      case_no = 3;
    }
  }
  if (!case_no) {
    if (same_spp_dir && same_mid_ein_dir && same_g2_dir) {
      case_no = 4;
    } else if (same_wave_dir && large_reflection && same_mid_ein_dir) {
      if (same_g2_dir) {
        case_no = 5;
      } else if (two_g2_1st_to) {
        case_no = 6;
      } else if (g2_oppo_dir) {
        case_no = 55;
      }
    } else if (same_spp_dir && large_reflection && same_mid_ein_dir) {
      if (same_g2_dir) {
        case_no = 7;
      } else if (g2_oppo_dir) {
        case_no = 8;
      } else if (two_g2_1st_to) {
        case_no = 9;
      }
    }
  }

  if (!case_no) {
    if (same_spp_dir && same_wave_dir && two_g2_1st_good) {
      case_no = 10;
    }
  }

  if (!case_no) {
    if (vss_to_same_wave && same_mid_ein_dir) {
      case_no = 11;
    }
  }

  if (!case_no) {
    if (min_ein_opposite) {
      case_no = 12;
    }
  }

  if (!case_no) {
    if (force_solved_one_spp) {
      case_no = 13;
    }
  }

  if (!case_no) {
    if (energe_without_spp) {
      case_no = 14;
    }
  }

  if (!case_no) {
    if (sure_vss_spp_dir) {
      case_no = 15;
    }
  }

  if (!case_no) {
    if (mid_zero_other_same)
      case_no = 16;
  }

  if (!case_no) {
    if (one_spp_solved_dir) {
      case_no = 17;
    }
  }

  if (!case_no) {
    if (diff_mid_ein_same_spp) {
      case_no = 18;
    }
  }

  if (!case_no) {
    if (mid_ein_oppo_but_g2_to) {
      case_no = 19;
    }
  }

  if (!case_no) {
    if (diff_cls_ein_g2_2type) {
      case_no = 20;
    }
  }

  if (!case_no) {
    if (same_spp_dir && same_wave_dir && large_reflection &&
        (same_g2_dir || g2_oppo_dir) && nice_region && vss_to_same_wave)
      case_no = 21;
  }

  if (!case_no) {
    if (same_wave_dir && large_reflection && (same_g2_dir || g2_oppo_dir) &&
        nice_region)
      case_no = 22;
  }

  if (!case_no) {
    if (one_spp_and_first_and_g2)
      case_no = 23;
  }

  if (!case_no) {
    if (zzzz_g2_and_first)
      case_no = 24;
  }

  if (!case_no) {
    if (zzzz_and_no_g2)
      case_no = 25;
  }
 
  return case_no;
}

ENERGY_STATE
EnergyMark::get_state() const noexcept
{
  ENERGY_STATE s = ENERGY_STATE::NONE;
  if (have_timer() && have_force())
    s = ENERGY_STATE::ENERGY;
  else if (have_force())
    s = ENERGY_STATE::FORCE;
  else
    s = ENERGY_STATE::NOFORCE;
  return s;
}

BATTLE_RESULT
EnergyMark::battle(const EnergyMark& em_b) const noexcept
{
  uint8_t a = (uint8_t)(get_state());
  uint8_t b = (uint8_t)(em_b.get_state());

  BATTLE_RESULT result = BATTLE_RESULT::NONE;
  if (b == uint8_t(ENERGY_STATE::ZZ) || b == uint8_t(ENERGY_STATE::ZZZZ)) {
    result = BATTLE_RESULT::UNKNOW;
  } else {
    if (a > b)
      result = BATTLE_RESULT::STRONG;
    else if (a == b)
      result = BATTLE_RESULT::ALIKE;
    else
      result = BATTLE_RESULT::WEAK;
  }

  return result;
}

bool
EnergyMark::energy_changed(const EnergyMark& em_b,
                           EnergyChanged& ec) const noexcept
{
  ec.reset();

  auto amout_diff_bit = [](auto diff) {
    uint8_t amount = 0;
    while (diff) {
      if (diff & 0x8000)
        ++amount;
      diff = diff << 1;
    }
    return amount;
  };
  ec.time_amount = amout_diff_bit(timing_case ^ em_b.timing_case);

  if (same_spp_dir && !em_b.same_spp_dir) { // B
    ++ec.force_amount;
  }
  if (same_wave_dir && !em_b.same_wave_dir) { // C
    ++ec.force_amount;
  }
  if (same_mid_ein_dir && !em_b.same_mid_ein_dir) { // G
    ++ec.force_amount;
  }
  if (force_solved_one_spp && !em_b.force_solved_one_spp) { // L
    ++ec.force_amount;
  }

  if (same_g2_dir && !em_b.same_g2_dir) { // E
    ++ec.g2_amount;
  }
  if (g2_oppo_dir && !em_b.g2_oppo_dir) { // F
    ++ec.g2_amount;
  }
  if (two_g2_1st_to && !em_b.two_g2_1st_to) { // K
    ++ec.g2_amount;
  }
  ec.all_amout = ec.time_amount + ec.force_amount + ec.g2_amount;

  ec.changed =
    ((cool_force() != em_b.cool_force()) ||
     (ec.time_amount >= 1 && ec.force_amount >= 1 && ec.g2_amount >= 1) ||
     (ec.all_amout >= 3));
#if (STONE_DEBUG)
  ec.print();
#endif // STONE_DEBUG

  return ec.changed;
}

void
EnergyMark::print(const char* title) noexcept
{
  auto dir_str = [](const dir_t dd) {
    return (dd > 0) ? "+1" : (dd == 0 ? "00" : "-1");
  };
  auto b = same_spp_dir;
  auto c = same_wave_dir;
  auto d = large_reflection;
  auto e = same_g2_dir;
  auto f = g2_oppo_dir;
  auto g = same_mid_ein_dir;
  auto i = nice_region;
  auto j = spot_without_spp;
  auto k = two_g2_1st_to;
  auto o = two_g2_1st_good;
  auto l = force_solved_one_spp;
  // auto m = time_solved_one_spp;

  printf("EM%s, Dir%s, A%u, B%d, C%d, G%d, L%d, E%d, F%d, K%d, O%d, D%d, I%d, "
         "J%d, --> %u\n",
         title,
         dir_str(dir),
         timing_case,
         b,
         c,
         g,
         l,
         e,
         f,
         k,
         o,
         d,
         i,
         j,
         force_case);
}


} // namespace Trade
