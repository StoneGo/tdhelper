#include "schrodinger.hpp"
#include "lines.hpp"
#include "wfw.hpp"
#include <cmath>
#include <cstdint>
#include <cstdio>
#include <cstring>
#include <ctime>

extern double g_hop;

namespace Trade {

const char*
sdg_name(const int8_t whoami)
{
  return (whoami == 1 ? "Tom" : (whoami == 2 ? "Jerry" : "Mickey"));
}

const uint8_t
sdg_color(const int8_t whoami)
{
  return (whoami == 1 ? 49 : (whoami == 2 ? 11 : 177));
}

double
Incomming::get_max_rvn_delta() const noexcept
{
  return (max_win_rvn - rvn_single);
}

void
Incomming::update_revenue(const double hp_now,
                          const dir_t dir,
                          const double avg_px,
                          const int8_t amount,
                          const time_t tm,
                          const int64_t idx) noexcept
{
  hpx_average = avg_px;
  if (amount > 0) {
    rvn_single = (hp_now - avg_px) * dir - 5;
    if (rvn_single > 0)
      ++count_win;
    else
      ++count_loss;
    rvn_total = rvn_single * amount;
    if (rvn_max == 0)
      rvn_max = rvn_min = rvn_single;

    if (rvn_single > rvn_max) {
      rvn_max = rvn_single;
      hp_rvn_max = hp_now;
      tm_rvn_max = tm;
      idx_rvn_max = idx;

      if (rvn_single > 0) {
        max_win_rvn = rvn_single;
      }
    }

    if (rvn_single < rvn_min) {
      rvn_min = rvn_single;
      hp_rvn_min = hp_now;
      tm_rvn_min = tm;
      idx_rvn_min = idx;

      if (rvn_single < 0) {
        max_loss_rvn = rvn_single;
      }
    }
  }
}

double
Incomming::max_slope() const noexcept
{
  if (tm_rvn_min > tm_rvn_max) {
    return (rvn_max - rvn_min) / (tm_rvn_min - tm_rvn_max);
  } else {
    return (rvn_max - rvn_min) / (tm_rvn_max - tm_rvn_min);
  }
}

double
Incomming::amp() const noexcept
{
  return (rvn_max - rvn_min);
}

bool
Incomming::big_reverse(const double hp_now, const time_t ts) noexcept
{
  dir_t d_reverse = 0;
  bool b_needle = false, b_big_needle = false;
  if (std::abs(rvn_max - rvn_single) > 25) {
    d_reverse = sign(hp_rvn_min - hp_rvn_max);
  }

  b_needle = (d_reverse && (ts - tm_rvn_max < 300)); // 5 minutes
  b_big_needle =                                     // 5 < time <= 15
    (d_reverse && (ts - tm_rvn_max > 300) && (ts - tm_rvn_max <= 60 * 15));

  dir_t db = 0;
  if (d_reverse && (b_needle || b_big_needle)) {
    db = d_reverse;
  }

  bool changed = false;
  if (db != d_big_reverse) {
    changed = true;
    d_big_reverse = db;
  }

  return changed;
}

// -----------------------------------------------------------------------
//
// Stop Price
//
// -----------------------------------------------------------------------
void
ClosePrice::clear() noexcept
{
  memset((void*)(this), 0, sizeof(ClosePrice));
}

#define ACCEPTED_CLOSE_PRICE_DELTA 5

bool
ClosePrice::set_good_to_win(const double good_to_win,
                            const bool preset) noexcept
{
  if (std::abs(rvn_good_to_win - good_to_win) > ACCEPTED_CLOSE_PRICE_DELTA) {
    rvn_good_to_win = good_to_win;
    set_these_prices |= CLOSE_PRICE_GOOD_TO_WIN;

    printf("<< Signal >> %sSet Close Price Good-to-Win %.02lf\n",
           ((preset) ? "Pre-" : ""),
           good_to_win);
    return true;
  }
  return false;
}

bool
ClosePrice::set_stop_loss(const double stop_loss, const bool preset) noexcept
{
  if (std::abs(rvn_stop_loss - stop_loss) > ACCEPTED_CLOSE_PRICE_DELTA) {
    rvn_stop_loss = stop_loss;
    set_these_prices |= CLOSE_PRICE_STOP_LOSS;

    printf("<< Signal >> %sSet Close Price Stop-Loss %.02lf\n",
           ((preset) ? "Pre-" : ""),
           stop_loss);
    return true;
  }
  return false;
}

bool
ClosePrice::set_stop_win(const double stop_win, const bool preset) noexcept
{
  if (std::abs(rvn_stop_win - stop_win) > ACCEPTED_CLOSE_PRICE_DELTA) {
    rvn_stop_win = stop_win;
    set_these_prices |= CLOSE_PRICE_STOP_WIN;

    printf("<< Signal >> %sSet Close Price Stop-Win %.02lf\n",
           ((preset) ? "Pre-" : ""),
           stop_win);
    return true;
  }
  return false;
}

bool
ClosePrice::set_min_win(const double min_win, const bool preset) noexcept
{
  if (std::abs(rvn_min_win - min_win) > ACCEPTED_CLOSE_PRICE_DELTA) {
    rvn_min_win = min_win;
    set_these_prices |= CLOSE_PRICE_MIN_WIN;
    printf("<< Signal >> %sSet Close Price Min-Win %.02lf\n",
           ((preset) ? "Pre-" : ""),
           min_win);
    return true;
  }
  return false;
}

bool
ClosePrice::set_prices(ClosePrice* const close_price) noexcept
{
  if (!close_price)
    return false;

  if (close_price->set_these_prices & CLOSE_PRICE_STOP_WIN) {
    set_stop_win(close_price->rvn_stop_win, false);
  }

  if (close_price->set_these_prices & CLOSE_PRICE_STOP_LOSS) {
    set_stop_loss(close_price->rvn_stop_loss, false);
  }

  if (close_price->set_these_prices & CLOSE_PRICE_MIN_WIN) {
    set_min_win(close_price->rvn_min_win, false);
  }

  if (close_price->set_these_prices & CLOSE_PRICE_GOOD_TO_WIN) {
    set_good_to_win(close_price->rvn_good_to_win, false);
  }

  set_these_prices = close_price->set_these_prices = (closed_t)(0);
  return true;
}

void
ClosePrice::cancel(const closed_t cancel_close) noexcept
{
  if (cancel_close & CLOSE_PRICE_STOP_WIN) {
    rvn_stop_win = 0;
  }

  if (cancel_close & CLOSE_PRICE_STOP_LOSS) {
    rvn_stop_loss = 0;
  }

  if (cancel_close & CLOSE_PRICE_MIN_WIN) {
    rvn_min_win = 0;
  }

  if (cancel_close & CLOSE_PRICE_GOOD_TO_WIN) {
    rvn_good_to_win = 0;
  }
}

void
ClosePrice::cancel_part(const closed_t cancel_close) noexcept
{
  if (cancel_close & (CLOSE_PRICE_GOOD_TO_WIN | CLOSE_PRICE_MIN_WIN)) {
    if (cancel_close & CLOSE_PRICE_MIN_WIN) {
      rvn_min_win = 0;
    }

    if (cancel_close & CLOSE_PRICE_GOOD_TO_WIN) {
      rvn_good_to_win = 0;
    }
  }
}

void
ClosePrice::cancel_all(const closed_t cancel_close) noexcept
{
  if (cancel_close == (CLOSE_PRICE_STOP_WIN | CLOSE_PRICE_STOP_LOSS)) {
    if (cancel_close & CLOSE_PRICE_STOP_WIN) {
      rvn_stop_win = 0;
    }

    if (cancel_close & CLOSE_PRICE_STOP_LOSS) {
      rvn_stop_loss = 0;
    }
  }
}

closed_t
ClosePrice::reached_stop_price(const double rvn) noexcept
{
  hited_close_type = HITED_CLOSE_NONE;
  hited_price_type = 0;

  if (rvn_stop_loss != 0) {
    if (rvn < rvn_stop_loss && std::abs(rvn - rchd_stop_loss) > 5) {
      hited_price_type |= CLOSE_PRICE_STOP_LOSS;
      rchd_stop_loss = rvn;
      hited_close_type |= HITED_CLOSE_ALL;
    }
  }

  if (rvn_min_win != 0) {
    if (rvn < rvn_min_win && rvn > 0 && std::abs(rvn - rchd_min_win) > 5) {
      hited_price_type |= CLOSE_PRICE_MIN_WIN;
      rchd_min_win = rvn;
      hited_close_type |= HITED_CLOSE_PARTIALLY;
    }
  }

  if (rvn_stop_win != 0) {
    if (rvn > rvn_stop_win && std::abs(rvn - rvn_stop_win) > 5) {
      hited_price_type |= CLOSE_PRICE_STOP_WIN;
      rchd_stop_win = rvn;
      hited_close_type |= HITED_CLOSE_ALL;
    }
  }

  if (rvn_good_to_win != 0) {
    if (rvn > rvn_good_to_win && std::abs(rvn - rvn_good_to_win) > 5) {
      hited_price_type |= CLOSE_PRICE_GOOD_TO_WIN;
      rchd_good_to_win = rvn;
      if (hited_close_type != HITED_CLOSE_ALL)
        hited_close_type |= HITED_CLOSE_PARTIALLY;
    }
  }

  if (hited_price_type)
    closed_rvn = rvn;

  return hited_price_type;
}

void
ClosePrice::print(const double hp_now) noexcept
{
  printf("StopPrice, min %.02lf max %.02lf L %.02lf G %.02lf, now %.02lf\n",
         rvn_min_win,
         rvn_stop_win,
         rvn_stop_loss,
         rvn_good_to_win,
         hp_now);
}

// -----------------------------------------------------------------------
//
// OpenSea
//
// -----------------------------------------------------------------------
void
OpenSea::init_open_sea(const int64_t idx, const time_t tm_now) noexcept
{
  memset((void*)(this), 0, sizeof(OpenSea));
  my_idx = idx;
  my_tm = tm_now;
  mpa[0].reached = mpa[1].reached = mpa[2].reached = mpa[3].reached = -1;
}

void
OpenSea::add_price(const double hpx) noexcept
{
  // TODO
  // TODO:
  //
  //
  if (good_for_now) {
    if (amount < MAX_VOLUEM) {
      if (mpa[amount].reached != 1) {
        mpa[amount].px = hpx;
        mpa[amount].reached = 0;
      }
      ++amount;
    }
  } else {
    if (amount < MAX_VOLUEM + 1) {
      if (mpa[amount].reached != 1) {
        mpa[amount].px = hpx;
        mpa[amount].reached = 0;
      }
      ++amount;
    }
  }
}

void
OpenSea::print(const double hp_now) noexcept
{
  if (amount > 0) {
    printf("open-sea: ");
    for (int8_t i = 0; i < MAX_VOLUEM; ++i) {
      if (mpa[i].reached >= 0) {
        printf("%d: (%.02lf, %d), ", i, mpa[i].px, mpa[i].reached);
      }
    }
    printf("\n");
  }
}

void
OpenSea::update_open_prices(const double hpx,
                            const dir_t d,
                            const bool good) noexcept
{
  hp_base = hpx;
  good_for_now = good;
  add_price(hpx);
  if (d > 0) {
    add_price(mpa[0].px - 25);
    if (!good)
      add_price(mpa[1].px - 25);
    // add_price(mpa[2].px - 25);
  } else {
    add_price(mpa[0].px + 25);
    if (!good)
      add_price(mpa[1].px + 25);
    // add_price(mpa[2].px + 25);
  }
}

// void
// OpenSea::update_lines_no_swing(const Lines& l,
//                                const double px,
//                                const dir_t d) noexcept
// {
// }

// void
// OpenSea::update_lines_when_swing(const Lines& l,
//                                  const double px,
//                                  const dir_t d) noexcept
// {
//   if (d > 0) {
//     amount = 0;
//     if (l.ndn > 1) {
//       // TODO:
//       // TODO: i did not check the right consentrant
//       // TODO: it means, now - line < someting
//       // TODO:
//       if (px - l.hp_dn_1 >= 2) {
//         add_price(l.hp_dn_1);
//       } else {
//         add_price(px - 10);
//       }

//       if (l.hp_dn_2 - mpa[0].px <= 20) {
//         add_price(l.hp_dn_2);
//       } else {
//         add_price(mpa[0].px - 25);
//       }

//       add_price(mpa[1].px - 25);
//       add_price(mpa[2].px - 25);
//     } else if (l.ndn > 0) {
//       if (px - l.hp_dn_1 >= 2) {
//         add_price(l.hp_dn_1);
//       } else {
//         add_price(px - 10);
//         add_price(mpa[0].px - 25);
//         add_price(mpa[1].px - 25);
//         add_price(mpa[2].px - 25);
//       }
//     } else {
//       add_price(px - 10);
//       add_price(mpa[0].px - 25);
//       add_price(mpa[1].px - 25);
//       add_price(mpa[2].px - 25);
//     }
//   } else {
//     if (l.nup > 1) {
//       // TODO:
//       // TODO: i did not check the right consentrant
//       // TODO: it means, now - line > someting
//       // TODO:
//       if (px - l.hp_up_1 <= 2) {
//         add_price(l.hp_up_1);
//       } else {
//         add_price(px + 10);
//       }

//       if (l.hp_up_2 - mpa[0].px >= 20) {
//         add_price(l.hp_up_2);
//       } else {
//         add_price(mpa[0].px + 25);
//       }

//       add_price(mpa[1].px + 25);
//       add_price(mpa[2].px + 25);
//     } else if (l.nup > 0) {
//       if (px - l.hp_up_1 >= 2) {
//         add_price(l.hp_up_1);
//       } else {
//         add_price(px + 10);
//         add_price(mpa[0].px + 25);
//         add_price(mpa[1].px + 25);
//         add_price(mpa[2].px + 25);
//       }
//     } else {
//       add_price(px + 10);
//       add_price(mpa[0].px + 25);
//       add_price(mpa[1].px + 25);
//       add_price(mpa[2].px + 25);
//     }
//   }
// }

// -----------------------------------------------------------------------
//
// Pet
//
// -----------------------------------------------------------------------
void
Pet::print(const char* title,
           const int64_t idx,
           const double hp_now,
           const time_t t_now) noexcept
{
  if (dir) {
    const char* name = sdg_name(whoami);
    const uint8_t color = sdg_color(whoami);
    printf("\033[38;5;%um %s is running to %d: (%ld %ld mins) | since %ld(%ld) "
           "| case no %u\033[0m\n",
           color,
           title,
           dir,
           ((target.timer.t - t_now) / 60),
           ((target.timer_2.t - t_now) / 60),
           ((t_now - my_tm) / 60),
           my_idx,
           case_no);
    target.lines.print(idx, hp_now);
    char buf[64];
    snprintf(buf, 64, "\033[38;5;%um %s: \033[0m", color, name);
    print_reached(buf, dir, target.reached, idx);
    // em.print(name);
  }
}

bool
Pet::set_timer(const time_t tm, const time_t tm2) noexcept
{
  target.set_timer(tm, tm2);
  return true;
}

bool
Pet::update_lines(const Lines& l, const double hp_now) noexcept
{
  auto changed = target.update_lines(1, l, hp_now);
  auto c2 = target.update_lines(-1, l, hp_now);
  return (c2 or changed);
}

Timer*
Pet::get_timer() noexcept
{
  return &target.timer;
}

Lines&
Pet::get_limits_lines() noexcept
{
  return target.lines;
}

Spacetime*
Pet::get_target() noexcept
{
  return &target;
}

uint8_t
Pet::check_reached(const time_t t_now,
                   const double hp_now,
                   Lines& l_now,
                   const int64_t idx) noexcept
{
  if (is_running()) {
    uint8_t rchd = 0;
    rchd = target.check_reached(t_now, hp_now, l_now, idx);
    if (rchd & LIMIT_REACHED::TIMER_OUT)
      ++amout_to;

    return rchd;
  }

  return (0);
}

void
Pet::update_timer(const time_t tm, const time_t tm2) noexcept
{
  target.update_timer(tm, tm2);
}

uint8_t
Pet::reacheded() const noexcept
{
  return target.reached;
}

void
Pet::clear_reacheded() noexcept
{
  target.reached = 0;
}

bool
Pet::reached_line2() noexcept
{
  bool rchd = 0;
  if (dir > 0) {
    rchd = (target.lines.nup > 1 && (target.reached & LIMIT_REACHED::UP_2));
  } else {
    rchd = (target.lines.ndn > 1 && (target.reached & LIMIT_REACHED::DN_2));
  }
  return rchd;
}

bool
Pet::reached_line1() noexcept
{
  bool rchd = 0;
  if (dir > 0) {
    rchd = (target.lines.nup > 0 &&
            target.reached & (LIMIT_REACHED::UP_1 | LIMIT_REACHED::UP_2));
  } else {
    rchd = (target.lines.ndn > 0 &&
            target.reached & (LIMIT_REACHED::DN_1 | LIMIT_REACHED::DN_2));
  }
  return rchd;
}

bool
Pet::reached_oppo_line1() noexcept
{
  bool rchd = 0;
  if (dir < 0) {
    rchd = (target.lines.nup > 0 &&
            target.reached & (LIMIT_REACHED::DN_1 | LIMIT_REACHED::DN_2));
  } else {
    rchd = (target.lines.ndn > 0 &&
            target.reached & (LIMIT_REACHED::UP_1 | LIMIT_REACHED::UP_2));
  }
  return rchd;
}

bool
Pet::reached_oppo_line2() noexcept
{
  bool rchd = 0;
  if (dir < 0) {
    rchd = (target.lines.nup > 1 && (target.reached & LIMIT_REACHED::UP_2));
  } else {
    rchd = (target.lines.ndn > 1 && (target.reached & LIMIT_REACHED::DN_2));
  }
  return rchd;
}

bool
Pet::timer_outed() noexcept
{
  return (amout_to > 0);
}

bool
Pet::current_timer_outed() noexcept
{
  return (target.reached & LIMIT_REACHED::TIMER_OUT);
}

void
Pet::stop(const int64_t idx,
          const int64_t tm [[maybe_unused]],
          const uint32_t cno [[maybe_unused]]) noexcept
{
  if (whoami) {
    const uint8_t color = sdg_color(whoami);
    const char* name = sdg_name(whoami);
    printf("\033[38;5;%um %s has stoped. begin %ld, stop %ld, CNO %u\033[0m\n",
           color,
           name,
           my_idx,
           idx,
           cno);
    reset();
  }
}

void
Pet::run(const dir_t d,
         const time_t tm,
         const time_t tm2,
         const Lines& lines,
         const double hp_now,
         const time_t t_now,
         const int64_t idx,
         const Dir5& d5,
         const EnergyMark& emark,
         const int whoami) noexcept
{
  reset();
  this->whoami = whoami;
  dir = d;
  this->d5 = d5;
  em = emark;
  set_timer(tm, tm2);
  update_lines(lines, hp_now);
  my_tm = t_now;
  my_idx = idx;
  target.reached = 0;

  auto name = sdg_name(whoami);
  print(name, idx, hp_now, t_now);
}

bool
Pet::d5_was_reversed(const Dir5& d5) noexcept
{
  return (this->d5.wave == -d5.wave && this->d5.ext == -d5.ext &&
          this->d5.g2 == -d5.g2 &&
          (this->d5.mid == -d5.mid || this->d5.ein == -d5.ein));
}

void
Pet::update_g2(const WaitingForWrong& wfw, const Wave& wave) noexcept
{
  d5.g2 = wfw.g2_dir;
}

bool
Pet::should_renew_lines(const Lines& l,
                        const time_t t_now,
                        const double hp_now) noexcept
{
  auto good_space_for_new_lines = [&]() {
    auto new_space = (dir > 0) ? (l.hp_up_2 - hp_now) : (hp_now - l.hp_dn_2);
    auto l2_delta = (dir > 0) ? (l.hp_up_2 - target.lines.hp_up_2)
                              : (target.lines.hp_dn_2 - l.hp_dn_2);
    bool yes =
      (new_space >= 9.9999) && (l2_delta >= 9.9999) && (l2_delta <= 40.0000);
    return yes;
  };

  if (!good_space_for_new_lines())
    return false;

  bool renew = false;
  if (timer_outed()) {
    if (dir > 0) {
      if (reached_line2()) {
        if (l.nup > 1 && (l.hp_up_2 - target.lines.hp_up_2 > 24.9999))
          renew = true;
      }
    } else {
      if (reached_line2()) {
        if (l.ndn > 1 && (target.lines.hp_dn_2 - l.hp_dn_2 > 24.9999))
          renew = true;
      }
    }
  }
  return renew;
}

void
Pet::erase_opposite_reached(const uint8_t rchd) noexcept
{
  if (dir) {
    target.erase_opposite_reached(dir, rchd);
  }
}

void
Pet::renew_target(const Lines& lines,
                  const time_t tm,
                  const time_t t_now,
                  const double hp_now,
                  const int64_t idx) noexcept
{
  target.reached = 0;
  target.lines = lines;
  target.set_timer(tm, 0);

  printf(
    "\033[38;5;200m ========= Cat renew Target %d: %ld mins ======== \033[0m",
    dir,
    (tm - t_now) / 60);
  target.lines.print(idx, hp_now);
}

#if (ONLINE_DATA_COMM)
int
Pet::get_json(char* buf, const int buf_len, const char* name)
{
  memset(buf, 0, buf_len);
  const char* format = "{\"type\":50,\"name\":\"%s\",\"dir\":%d,\"line1\":%d,"
                       "\"line2\":%d,\"to\":%"
                       "d,\"tm\":%ld,\"up1\":%.02lf,\"up2\":%.02lf,\"dn1\":%."
                       "02lf,\"dn2\":%.02lf}";
  bool reached_l1 = reached_line1();
  bool reached_l2 = reached_line2();
  bool to = timer_outed();

  double u1 = 0, u2 = 0, d1 = 0, d2 = 0;
  if (target.lines.nup > 0)
    u1 = target.lines.hp_up_1;
  if (target.lines.nup > 1)
    u2 = target.lines.hp_up_2;
  else
    u2 = u1;

  if (target.lines.ndn > 0)
    d1 = target.lines.hp_dn_1;
  if (target.lines.ndn > 1)
    d2 = target.lines.hp_dn_2;
  else
    d2 = d1;

  snprintf(buf,
           buf_len,
           format,
           name,
           dir,
           reached_l1,
           reached_l2,
           to,
           target.timer.t,
           u1,
           u2,
           d1,
           d2);
  int len = strlen(buf);
  return len;
}
#endif // ONLINE

#define MINUTES(tm, t_now) ((tm) ? (int((tm) - (t_now)) / 60) : 0)

// -----------------------------------------------------------------------
//
// Schrodinger
//
// -----------------------------------------------------------------------
void
Schrodinger::print_revenue(const double hp_now, const time_t t_now) noexcept
{
  if (dir) {
    const uint8_t color = sdg_color(whoami);
    // const char* rvn_str = (incomming.rvn_total > 0) ? "Won" : "Lost";
    // const char* rvn_str = (incomming.rvn_total > 0) ? "W" : "L";
    const char* name = sdg_name(whoami);
    printf("\033[38;5;%um %s %d, hit%d, %.02lfh (w%.02lf %ld, l%.02lf %ld) T "
           "%d Du %d TO %d CN %u\033[0m\n",
           color,
           name,
           dir,
           os_entries.amount_reached,
           incomming.rvn_single,
           incomming.rvn_max,
           incomming.idx_rvn_max,
           incomming.rvn_min,
           incomming.idx_rvn_min,
           MINUTES(target.timer.t, t_now),
           int(t_now - my_tm) / 60,
           amout_to,
           case_no);

    print_reached(sdg_name(whoami), dir, target.reached, 0);
  }
}

uint8_t
Schrodinger::reached(const time_t t_now,
                     const double hp_now,
                     Lines& l_now,
                     const int64_t idx) noexcept
{
  if (is_running()) {
    uint8_t rchd = 0;
    rchd = target.check_reached(t_now, hp_now, l_now, idx);
    if (rchd & LIMIT_REACHED::TIMER_OUT)
      ++amout_to;

    return rchd;
  }

  return (0);
}

void
Schrodinger::print(const char* title,
                   const int64_t idx,
                   const double hp_now,
                   const time_t t_now) noexcept
{
  // Pet::print(title, idx, hp_now, t_now);
  // if (dir) {
  //   // os_entrance.print(hp_now);
  //   // os_exit.print(hp_now);
  //   printf("TO times %u\n", amout_to);
  //   print_revenue(hp_now);
  // }
  if (dir) {
    print_revenue(hp_now, t_now);
    target.lines.print(idx, hp_now);
  }
}

uint8_t
Schrodinger::hit_open_price(const int64_t idx,
                            const time_t t_now,
                            const double hp_now) noexcept
{
  uint8_t rchd = 0;
  if (os_entries.amount_reached < OpenSea::MAX_VOLUEM) {
    for (int8_t i = 0; i < OpenSea::MAX_VOLUEM; ++i) {
      if (!os_entries.mpa[i].reached) {
        if (int(dir) * (int(hp_now) - int(os_entries.mpa[i].px)) <= 0) {
          rchd = i + 1;
          break;
        }
      }
    }
  }

  return rchd;
}

uint8_t
Schrodinger::open_price(const int64_t idx,
                        const time_t t_now,
                        const double hp_now) noexcept
{
  uint8_t rchd = 0;
  if (os_entries.amount_reached < OpenSea::MAX_VOLUEM) {
    double sum = 0;
    int8_t amount = 0;
    int8_t old_amount = os_entries.amount_reached;
    for (int8_t i = 0; i < OpenSea::MAX_VOLUEM; ++i) {
      if (!os_entries.mpa[i].reached) {
        if (int(dir) * (int(hp_now) - int(os_entries.mpa[i].px)) <= 0) {
          os_entries.mpa[i].px = hp_now;
          sum += hp_now;
          os_entries.hp_cur_open = hp_now;
          ++amount;
          os_entries.mpa[i].reached = 1;
        }
      } else if (os_entries.mpa[i].reached == 1) {
        sum += os_entries.mpa[i].px;
        ++amount;
      }
    }

    if (old_amount < amount) {
      rchd = LIMIT_REACHED::ENTERED;
      os_entries.amount_reached = amount;
      os_entries.average = sum / double(amount);
    }
  }

  return rchd;
}

dir_t
Schrodinger::get_price_dir(const double hp_now) noexcept
{
  dir_t d = 0;
  return d;
}

bool
Schrodinger::second_timer_outed(const time_t t_now) noexcept
{
  return target.second_timer_outed(t_now);
}

void
Schrodinger::set_open_entries(const double hp_base,
                              const int vol [[maybe_unused]]) noexcept
{
  os_entries.update_open_prices(hp_base, get_dir(), true);
}

int
Schrodinger::get_durantion(const time_t t_now) noexcept
{
  return int(t_now - my_tm) / 60;
}

} // namespace Trade
