#include "make_deal.hpp"
#include <cstdio>
#if (ONLINE_DATA_COMM)
#include "mqtt_comm.hpp"
#include <csignal>
#else
#include "td_signal.hpp"
#include "tick.h"
#endif // UDP
#include <cstdint>
#include <stdint.h>

/*
static const char *subdata[] = {
    "rb888-20100104-4580000-5580000.bin",   // 1
    "rb888-20151008-4580000-5580000.bin",   // 2
    "l9888-20151008-2400000-3400000.bin",   // 3
    "zn888-20151008-30800000-31800000.bin", // 4
    "au888-20151008-4970000-5970000.bin",   // 5
    "rb888-20151008-15000000-16000000.bin", // 6
    "cu888-20151008-26200000-27200000.bin", // 7
    "i9888-20151008-18970000-19970000.bin", // 8
    "j9888-20151008-17880000-18880000.bin", // 9
    "rb888-20151008-14500000-15500000.bin", // 10
    "rb888-20151008-14200000-15200000.bin", // 11
    "bt888_20201125-20210301.bin",          // 12
    "rb888_tick-20200506-20201015.bin",     // 13
    "et888_20201125-20210301.bin",          // 14
    "bt888-20201124-20210210.bin",          // 15
    "eth888-20210707-20210906.bin",         // 16
    "xch888-20210718-20210907.bin",         // 17
    "bt888-20201125-20210929.bin",          // 18
    "et888-20201125-20210929.bin",          // 19
    "et888-20201125-20210929.bin",          // 20
    "bt888_tick.bin",                       // 21
    "bt888-reshaped.bin",                   // 22
    "bt888-20210718-20211228.bin",          // 23
    "d-online-20230525-20230627.tick",      // 24
};
*/

#if (!ONLINE_DATA_COMM)
extern int64_t g_stop_idx;
extern int64_t g_start_idx;
#endif // STONE_DEBUG

#if (ONLINE_DATA_COMM)
extern int g_run;
extern int g_sock;
void
sighandler(int sig)
{
  g_run = 0;
  close(g_sock);
  g_sock = 0;
  printf("exit with signal %d\n", sig);
}
#endif // ONLINE_DATA_COMM

int
main(int argc, char** argv)
{
#if (ONLINE_DATA_COMM)
  if (argc != 2) {
    printf("Usage <udp port for receiving>\n");
    return (0);
  }

  signal(SIGINT, sighandler);
  signal(SIGILL, sighandler);
  signal(SIGQUIT, sighandler);
  signal(SIGSTOP, sighandler);
  signal(SIGTERM, sighandler);

  // mqtt_init(argv[2], argv[3]);
  g_run = 1;

  int ret = mqtt_init();
  if (ret) {
    printf("Can not start mqtt client thread to send message\n");
  }

  set_mqtt_ready();

  /*
  mosquitto_pub -h 192.168.0.168 -p 45789 -t 'usen/puupeteer/prdctline' -m
  "{\"type\":51,\"pets\":[{\"type\":50,\"name\":\"Jerry\",\"dir\":1,\"line1\":1,\"line2\":0,\"to\":0,\"tm\":1697991030666,\"up1\":31500,\"up2\":31700,\"dn1\":30100,\"dn2\":29800},
  {\"type\":50,\"name\":\"Tom\",\"dir\":1,\"line1\":1,\"line2\":1,\"to\":1,\"tm\":1697997030666,\"up1\":31000,\"up2\":31100,\"dn1\":30100,\"dn2\":29800},
  {\"type\":50,\"name\":\"mickey\",\"dir\":1,\"line1\":1,\"line2\":1,\"to\":1,\"tm\":1697985030666,\"up1\":31500,\"up2\":31700,\"dn1\":30100,\"dn2\":29800}]}"
  // Tom and Jerry
  mosquitto_pub -h 192.168.0.168 -p 45789 -t 'usen/puupeteer/prdctline' -m
  "{\"type\":51,\"pets\":[{\"type\":50,\"name\":\"Tom\",\"dir\":1,\"line1\":1,\"line2\":1,\"to\":1,\"tm\":1697997030666,\"up1\":31000,\"up2\":31100,\"dn1\":30100,\"dn2\":29800},
  {\"type\":50,\"name\":\"Jerry\",\"dir\":1,\"line1\":1,\"line2\":0,\"to\":0,\"tm\":1697991030666,\"up1\":31500,\"up2\":31700,\"dn1\":30100,\"dn2\":29800}]}"
  // Tom and mickey
  mosquitto_pub -h 192.168.0.168 -p 45789 -t 'usen/puupeteer/prdctline' -m
  "{\"type\":51,\"pets\":[{\"type\":50,\"name\":\"Tom\",\"dir\":1,\"line1\":1,\"line2\":1,\"to\":1,\"tm\":1697997030666,\"up1\":31000,\"up2\":31100,\"dn1\":30100,\"dn2\":29800},
  {\"type\":50,\"name\":\"mickey\",\"dir\":1,\"line1\":1,\"line2\":1,\"to\":1,\"tm\":1697985030666,\"up1\":31500,\"up2\":31700,\"dn1\":30100,\"dn2\":29800}]}"
  // Tom only
  mosquitto_pub -h 192.168.0.168 -p 45789 -t 'usen/puupeteer/prdctline' -m
  "{\"type\":51,\"pets\":[{\"type\":50,\"name\":\"Tom\",\"dir\":1,\"line1\":1,\"line2\":1,\"to\":1,\"tm\":1697997030666,\"up1\":31000,\"up2\":31100,\"dn1\":30100,\"dn2\":29800}]}"
  sleep(10);
  const char* msg_1 =
  "{\"type\":51,\"pets\":[{\"type\":50,\"name\":\"Jerry\",\"dir\":1,\"line1\":1,\"line2\":0,\"to\":0,\"tm\":1697991030666,\"up1\":31500,\"up2\":31700,\"dn1\":30100,\"dn2\":29800},
  {\"type\":50,\"name\":\"Tom\",\"dir\":1,\"line1\":1,\"line2\":1,\"to\":1,\"tm\":1697997030666,\"up1\":31000,\"up2\":31100,\"dn1\":30100,\"dn2\":29800},
  {\"type\":50,\"name\":\"mickey\",\"dir\":1,\"line1\":1,\"line2\":1,\"to\":1,\"tm\":1697985030666,\"up1\":31500,\"up2\":31700,\"dn1\":30100,\"dn2\":29800}]}";
  mqtt_send_message(msg_1);
  sleep(10);
  const char* msg_2 =
  "{\"type\":51,\"pets\":[{\"type\":50,\"name\":\"Tom\",\"dir\":1,\"line1\":1,\"line2\":1,\"to\":1,\"tm\":1697997030666,\"up1\":31000,\"up2\":31100,\"dn1\":30100,\"dn2\":29800},
  {\"type\":50,\"name\":\"Jerry\",\"dir\":1,\"line1\":1,\"line2\":0,\"to\":0,\"tm\":1697991030666,\"up1\":31500,\"up2\":31700,\"dn1\":30100,\"dn2\":29800}]}";
  mqtt_send_message(msg_2);
  sleep(10);
  const char* msg_3 =
  "{\"type\":51,\"pets\":[{\"type\":50,\"name\":\"Tom\",\"dir\":1,\"line1\":1,\"line2\":1,\"to\":1,\"tm\":1697997030666,\"up1\":31000,\"up2\":31100,\"dn1\":30100,\"dn2\":29800},
  {\"type\":50,\"name\":\"mickey\",\"dir\":1,\"line1\":1,\"line2\":1,\"to\":1,\"tm\":1697985030666,\"up1\":31500,\"up2\":31700,\"dn1\":30100,\"dn2\":29800}]}";
  mqtt_send_message(msg_3);
  sleep(10);
  const char* msg_4 =
  "{\"type\":51,\"pets\":[{\"type\":50,\"name\":\"Tom\",\"dir\":1,\"line1\":1,\"line2\":1,\"to\":1,\"tm\":1697997030666,\"up1\":31000,\"up2\":31100,\"dn1\":30100,\"dn2\":29800}]}";
  mqtt_send_message(msg_4);
  sleep(10);
  */

  Trade::md.make_deal(short(atoi(argv[1])));
  Trade::md.show_revenue();

  mqtt_release();

#else // ONELINE_DATA_COMM

    // !! NOT ONELINE_DATA_COMM !!

#if (STONE_DEBUG || STEP_BY_STEP)
  if (argc != 7) {
    printf(
      "Usage %s <tick_filepath> <signal_filepath> <spos> <epos> <stop_idx> "
      "<start-idx>\n",
      argv[0]);
    return (0);
  }
#else
  if (argc != 8) {
    printf(
      "Usage %s <tick_filepath> <signal_filepath> <spos> <epos> <stop_idx> "
      "<start-idx> <ss|noss>\n",
      argv[0]);
    return (0);
  }
  int save_stop_idx = 0;
  if (strcmp(argv[7], "ss") == 0)
    save_stop_idx = 1;
  Trade::md.set_save_stop_idx(save_stop_idx);
#endif //(STONE_DEBUG || STEP_BY_STEP)

#if (STONE_DEBUG)
  printf("Debug Version,%s\n", Trade::version);
#else
  printf("Release Version, %s\n", Trade::version);
#endif

  size_t size;
  int64_t spos = std::stoul(argv[3]);
  int64_t epos = std::stoul(argv[4]);

  g_stop_idx = std::stod(argv[5]);
  g_start_idx = std::stod(argv[6]);

  // READ the signal data from file
  TICK_DATA* td = load_tick_bin(argv[1], &size, spos, epos);
  if (td) {
    if ((epos > ((int64_t)(size) + spos - 1)) || (epos <= spos))
      epos = (int64_t)(size) + spos - 1;

    printf("Testing Start %ld to %ld, g-stop %ld\n", spos, epos, g_stop_idx);
    auto stm = time_t(td->ftime);
    Trade::read_signal_from_file(argv[2], stm);
    if (stm > 0) {
      // output_map();

      Trade::md.mock_make_deal(td, stm, spos, epos);
      Trade::md.show_revenue();

      Trade::clear_map();
    }
    free(td);
  }

#endif // ONLINE

  return (0);
}
