#include "MQTTAsync.h"
#include "mqtt_comm.hpp"
#include <pthread.h>
#include <cstdio>
#include <string>
#include <memory>
#include <queue>

FbConfig config;

FbConfig*
get_config() noexcept
{
  return &config;
}

struct MqttPubContent
{
  time_t t;
  std::string topic;
  std::string msg;

  MqttPubContent(const time_t tt, const std::string t, const std::string m)
    : t(tt)
    , topic(t)
    , msg(m)
  {
  }
};
using MqttPubContentPtr = std::shared_ptr<MqttPubContent>;
using PubQueue = std::queue<MqttPubContentPtr>;

static pthread_mutex_t mtx_pub_queue = PTHREAD_MUTEX_INITIALIZER;
static PubQueue pub_queue;

#define ADDRESS "tcp://222.128.84.165:45789"
#define CLIENTID "TheSchrodingerPublisher"
#define QOS 1
#define TIMEOUT 10000L

static int mqtt_publish_message();

static MQTTAsync mqtt_client;
static MQTTAsync_connectOptions conn_opts =
  MQTTAsync_connectOptions_initializer;
static MQTTAsync pub_client = NULL;
static MQTTAsync_responseOptions pub_opts =
  MQTTAsync_responseOptions_initializer;
static MQTTAsync_message pub_msg = MQTTAsync_message_initializer;

static int finished = 0;
static int run_mqtt = 1;
static int flag_connected = 0;
static int mqtt_ready = 0;

static pthread_t threadId;

void
set_mqtt_ready()
{
  mqtt_ready = 1;
}

void
connlost(void* context, char* cause)
{
  flag_connected = 0;

  printf("\nConnection lost\n");
  if (cause)
    printf("   cause: %s\n", cause);
}

void
onDisconnectFailure(void* context, MQTTAsync_failureData* response)
{
  printf("Disconnect failed %d %d\n", response->token, response->code);
  finished = 1;
}

void
onDisconnect(void* context, MQTTAsync_successData* response)
{
  printf("Successful disconnected\n");
  finished = 1;
}

void
onSendFailure(void* context, MQTTAsync_failureData* response)
{
  printf("Message send failed token %d error code %d\n",
         response->token,
         response->code);
  /*MQTTAsync client = (MQTTAsync)context;
  MQTTAsync_disconnectOptions opts = MQTTAsync_disconnectOptions_initializer;
  int rc;

  opts.onSuccess = onDisconnect;
  opts.onFailure = onDisconnectFailure;
  opts.context = client;
  if ((rc = MQTTAsync_disconnect(client, &opts)) != MQTTASYNC_SUCCESS)
  {
    printf("Failed to start disconnect, return code %d\n", rc);
    exit(EXIT_FAILURE);
  }*/
}

void
onSent(void* context, MQTTAsync_successData* response)
{
  printf("Message with token value %d delivery confirmed\n", response->token);
}

void
onConnectFailure(void* context, MQTTAsync_failureData* response)
{
  printf("Connect failed, rc %d\n", response ? response->code : 0);
  finished = 1;
  flag_connected = 0;
}

void
onConnect(void* context, MQTTAsync_successData* response)
{
  printf("Successful connected\n");
  pub_client = (MQTTAsync)(context);

  pub_opts.onSuccess = onSent;
  pub_opts.onFailure = onSendFailure;
  pub_opts.context = pub_client;

  flag_connected = 1;
}

int
messageArrived(void* context,
               char* topicName,
               int topicLen,
               MQTTAsync_message* m)
{
  // not expecting any messages
  return 1;
}

void*
pub_main(void* param)
{
  int rc;
  FbConfig* config = (FbConfig*)(param);
start:
  if ((rc = MQTTAsync_create(&mqtt_client,
                             config->mqtt_broker,
                             config->mqtt_clientid,
                             MQTTCLIENT_PERSISTENCE_NONE,
                             NULL)) != MQTTASYNC_SUCCESS) {
    printf("Failed to create client object, return code %d\n", rc);
    return NULL;
  }

  if ((rc = MQTTAsync_setCallbacks(
         mqtt_client, NULL, connlost, messageArrived, NULL)) !=
      MQTTASYNC_SUCCESS) {
    printf("Failed to set callback, return code %d\n", rc);
    return NULL;
  }

  conn_opts.keepAliveInterval = 20;
  conn_opts.cleansession = 1;
  conn_opts.onSuccess = onConnect;
  conn_opts.onFailure = onConnectFailure;
  conn_opts.context = mqtt_client;

  if ((rc = MQTTAsync_connect(mqtt_client, &conn_opts)) != MQTTASYNC_SUCCESS) {
    printf("Failed to start connect, return code %d\n", rc);
    return NULL;
  }

  sleep(10); // waiting for connect

  while (run_mqtt) {
    if (run_mqtt && !flag_connected) {
      MQTTAsync_destroy(&mqtt_client);
      goto start;
    }

    mqtt_publish_message();

    sleep(1);
  }

  printf("Start Quit Mqtt Client Thread\n");
  MQTTAsync_disconnectOptions dis_opts =
    MQTTAsync_disconnectOptions_initializer;

  dis_opts.onSuccess = onDisconnect;
  dis_opts.onFailure = onDisconnectFailure;
  dis_opts.context = mqtt_client;
  if ((rc = MQTTAsync_disconnect(mqtt_client, &dis_opts)) !=
      MQTTASYNC_SUCCESS) {
    printf("Failed to start disconnect, return code %d\n", rc);
    return NULL;
  }

  sleep(2);
  MQTTAsync_destroy(&mqtt_client);
  return (void*)((int64_t)(rc));
}

/*
{"form": "swing", "trend": 1, "timing1": "2.7-4.7", "grade1": 4, "up1":
"49,904.4", "dn1":"", "next": 2, "timing2": "6-8", "grade2": 5, "up2": "",
"dn2": "47099.01"}
{"form": "swing", "trend": 1, "timing1": "2.7-4.7", "grade1": 4, "up1":
"49,904.4", "dn1":"", "next": ""}
*/

int
pub_message(const char* msg, const char* topic)
{
  if (!run_mqtt) {
    return (-1);
  }

  if (!flag_connected) {
    return (-2);
  }

  pub_msg.payload = (void*)(msg);
  pub_msg.payloadlen = (int)strlen(msg);
  pub_msg.qos = QOS;
  pub_msg.retained = 0;
  // printf("pub message %s, payload %s, len %d\n", topic,
  // (char*)(pub_msg.payload), pub_msg.payloadlen);
  int rc = 0;
  if ((rc = MQTTAsync_sendMessage(pub_client, topic, &pub_msg, &pub_opts)) !=
      MQTTASYNC_SUCCESS) {
    printf("Failed to start sendMessage, return code %d\n", rc);

    if (!flag_connected) {
      if ((rc = MQTTAsync_connect(mqtt_client, &conn_opts)) !=
          MQTTASYNC_SUCCESS) {
        printf("Failed to start connect, return code %d\n", rc);
        finished = 1;
      }
    }
  }
  // printf("send msg %d\n", rc);
  return rc;
}

// publish the Back in TIme Direction
// static const char* bit_direction_fmt =
// "{\"cmd\":\"bit_dir\",\"inst\":\"%s\","
// "\"master\":%d,\"direction\":%d,\"datapos\":%ld,\"time\":%lu}";

int
mqtt_init()
{
  config.mqtt_broker = strdup(ADDRESS);
  config.mqtt_clientid = strdup(CLIENTID);
  // FbConfig *config = get_config();
  // Create a thread that will function threadFunc()
  int err = pthread_create(&threadId, NULL, pub_main, (void*)(&config));
  if (err) {
    run_mqtt = 0;
    printf("Error on create the mqtt client thread %d\n", err);
  } else {
    run_mqtt = 1;
    printf("Good, created the mqtt client thread %d\n", err);
    fflush(stdout);
  }
  return err;
}

int
mqtt_release()
{
  if (run_mqtt) {
    run_mqtt = 0;
    int err = pthread_join(threadId, NULL);
    printf("Good, destroyed the mqtt client thread %d\n", err);
    return err;
  } else {
    return (0);
  }
}

// static char* bit_direction_fmt = "{\"cmd\": \"bit_dir", \"inst\":\"%s\","
// "\"master\":%d,\"direction\":%d,\"datapos\":%ld,\"time\":%lu}";
// int mqtt_pub_bit_direction(const char* inst, bool master, int dir,
//                            int64_t pos) {
//     if (mqtt_no_init) return -1;
//
//     char msg[1024];
//     snprintf(msg, 1024, bit_direction_fmt, inst, master, dir, pos, time(0));
//     return pub_message(msg, get_config()->mqtt_pub_trend);
// }

static int
mqtt_publish_message()
{
  pthread_mutex_lock(&mtx_pub_queue);
  while (!pub_queue.empty()) {
    if (flag_connected) {
      const auto& content = pub_queue.front();
      int ret = pub_message(content->msg.c_str(), content->topic.c_str());
      if (!ret) {
        pub_queue.pop();
      } else {
        printf("The MQTT publish error\n");
        break;
      }
    } else {
      printf("The MQTT did not connected\n");
      break;
    }
  }
  pthread_mutex_unlock(&mtx_pub_queue);

  return (0);
}

// int mqtt_pub_trend(const char* msg) {
//   if (flag_connected) {
//     return pub_message((char*)msg, get_config()->mqtt_pub_trend);
//   }
//
//   return (0);
// }
//
// int mqtt_pub_trading_command(const char *msg) {
//   if (flag_connected) {
//     return pub_message((char*)msg, get_config()->mqtt_pub_trend);
//   }
//
//   return (0);
// }

const char* PUB_PREDICT_LINE_TOPIC = { "usen/puupeteer/prdctline" };

static void
ts_to_hour_str(char* buf, const int64_t ts)
{
  struct tm ltm;
  time_t lt = ts;
  localtime_r(&lt, &ltm);
  snprintf(buf,
           64,
           "%04d-%02d-%02d %02d:00",
           ltm.tm_year + 1900,
           ltm.tm_mon + 1,
           ltm.tm_mday,
           ltm.tm_hour);
}

static void
ts_to_hour_str_2(char* buf, const int64_t ts)
{
  struct tm ltm;
  time_t lt = ts;
  localtime_r(&lt, &ltm);
  snprintf(buf,
           64,
           "%04d-%02d-%02d %02d:00:00",
           ltm.tm_year + 1900,
           ltm.tm_mon + 1,
           ltm.tm_mday,
           ltm.tm_hour);
}


int
mqtt_send_message(const char* msg)
{
  if (!mqtt_ready)
    return (0);

  MqttPubContentPtr content =
    std::make_shared<MqttPubContent>(time(0), PUB_PREDICT_LINE_TOPIC, msg);
  // printf("the message : %s, %s, %ld\n", content->topic.c_str(),
  //        content->msg.c_str(), content->t);
  pthread_mutex_lock(&mtx_pub_queue);
  pub_queue.push(content);
  pthread_mutex_unlock(&mtx_pub_queue);

  printf("mqtt msg: %s\n", msg);

  return (0);
}

