#include "make_deal.hpp"
#include "dirs.hpp"
#include "energy.hpp"
#include "lines.hpp"
#include "position.hpp"
#include "schrodinger.hpp"
#include "td_signal.hpp"
#include "tick.h"
#include "wfw.hpp"
#if (!STONE_DEBUG)
#include <iostream>
#endif // !STONE_DEBUG
#include <cstdint>
#include <cstdio>
#include <ctime>

#if (ONLINE_DATA_COMM)
#include <arpa/inet.h>
#include <iostream>
#include <netdb.h>
#include <netinet/in.h>
#include <regex>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

using Strings = std::vector<std::string>;

static void print_strings [[maybe_unused]] (const Strings& strs)
{
  for (const auto& s : strs) {
    std::cout << s << "*";
  }
  std::cout << '\n';
}

static Strings split [[maybe_unused]] (const char* buf, const char* spliter)
{
  // passing -1 as the submatch index parameter performs splitting
  std::string input(buf);
  std::regex re(spliter);
  std::sregex_token_iterator first{ input.begin(), input.end(), re, -1 }, last;
  return { first, last };
}
#endif // for TCP/IP

extern int commision;

#if (ONLINE_DATA_COMM)
int g_run = 0;
int g_sock = 0;
#if (STONE_DEBUG)
int64_t g_stop_idx = 0;
int64_t g_start_idx = 0;
#endif
#else
int64_t g_stop_idx = 0;
int64_t g_start_idx = 0;
#endif // UDP

namespace Trade {

const char* version = "0.6.5.0.1 - min_api = v43-ms";

Case_Result
make_case_result(int result, uint32_t case_no) noexcept
{
  return std::make_pair(result, case_no);
}

MakeDeal md;
double g_big_loss = -20;

MakeDeal::MakeDeal()
{
  dd_bd.set_deep(1000);
#if SAVE_STOP_IDX_TO_FILE
  if (save_stop_idx)
    init_save_stop_file();
#endif
}

MakeDeal::~MakeDeal()
{
#if SAVE_STOP_IDX_TO_FILE
  if (save_stop_idx)
    close_save_stop_file();
#endif
}

#if (ONLINE_DATA_COMM)
int
MakeDeal::make_deal(const short udp_port) noexcept
{
  int ret = 0;
  const int recvbuf_len = 2048;
  char line[recvbuf_len];

  struct sockaddr_in addr;
  addr.sin_family = AF_INET;
  addr.sin_port = htons(udp_port);
  addr.sin_addr.s_addr = inet_addr("127.0.0.1"); // htonl(INADDR_ANY);

  if ((g_sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
    perror("socket");
    return -1;
  }

  // port bind to server
  if (bind(g_sock, (struct sockaddr*)&addr, sizeof(addr)) < 0) {
    perror("udp_data_receiver bind error");
    return -2;
  }

  struct sockaddr_in clientAddr;
  memset(&clientAddr, 0, sizeof(clientAddr));
  socklen_t socklen = sizeof(clientAddr);

  // set_mqtt_ready();

  bool start_trade = false;
  int8_t test_signal = 0;
  int8_t signal_probe = 0;

  for (; g_run;) {
    int len = recvfrom(
      g_sock, line, recvbuf_len, 0, (struct sockaddr*)&clientAddr, &socklen);
    if (len <= 0) {
      printf("Receive UDP Data error %d\n", len);
      sleep(1);
      continue;
    }
    line[len] = 0;

    time_t tm_signal = 0;
    ret = parse_signal(line, tm_signal);
    if (ret < 0)
      continue;

    signal_probe = ret;
    test_signal |= signal_probe;
    start_trade = (test_signal >= 7);

    // --------------------------------------------------------------
    // Begin: the basic data was changed
    event_t event = 0;

    if (start_trade) {
      event |= basic_data_changed(signal_probe, tm_signal);

      if (!ret) {
        ret = reached(event);
      }

      if (!ret) {
        ret = energy_battle(event, tm_signal);
      }

      if (!ret) {
        ret = fighting(event);
      }
    } // start trade

    // mqtt_publish_message();

    if (ret < 0) {
      printf("Quit The Make Deal Program with %d\n", ret);
      break;
    }

    ret = tjm_do_pet_actions();

  } // For loop

  printf("ALL REVENUE %.02lf $ (%.02lf hop)\n", revenue * g_hop, revenue);

  return (0);
}

#else
// !ONLINE_DATA_COMM

static bool first_time_ = true;
int
MakeDeal::mock_make_deal(const TICK_DATA* td,
                         const time_t stm,
                         const int64_t spos,
                         const int64_t epos) noexcept
{
  // size_t all = 0;
  // int errs = 0;
  bool start_trade = false;
  int8_t test_signal = 0;
  idx_tm_ = stm;
  idx__ = spos;

  for (int64_t i = 0; i < (epos - spos + 1); ++i, ++idx_tm_, ++idx__) {
    t_now_ = time_t(td[i].ftime);
    hp_now_ = td[i].price / g_hop;
    int ret = 0;

    {
      int v[4] = { 0, 0, 0, 0 };
      int vol = mock.mock_deal(idx__, hp_now_, td[i].ftime, v);
      // Run it in mock_deal already
      // mock_update_float_revenue();

      if (vol) {
        show_revenue();
      }
    }

    event_t event = 0;

#if (STONE_DEBUG)
    if (idx__ >= g_stop_idx) {
      if (idx__ == g_stop_idx)
        show_params();
    }
#endif

    if (!ret) {
      ret = hited_tom_open_or_stop_price(event);
    }

    if (!ret) {
      ret = reached(event);
    }

    if (!ret) {
      ret = test_dirs(event);
    }

    if (!ret) {
      ret = tjm_hited_something(event);
    }

    StrVecPtr sv = find_strings(idx_tm_);
    if (!sv) {
      if (!start_trade || g_start_idx > idx__)
        continue;

      if (g_start_idx == idx__) {
        if (first_time_) {
          event |=
            (EVENT_CE_DIR_CHANGED | EVENT_BIG_DIR_CHANGED |
             EVENT_SWING_DIR_CHANGED | EVENT_TTS_CHANGED |
             EVENT_FIGHT_DIR_CHANGED | EVENT_SPACE_DIR_CHANGED |
             EVENT_5DIRS_CHANGED | EVENT_PDC_DIR_CHANGED | EVENT_TURN_CHANGED);
          first_time_ = false;
        }
      }
      if (!ret)
        ret = fighting(event);

#if (STONE_DEBUG || STEP_BY_STEP)
      if (ret) {
        if (ret < 0) {
          break;
        }
      }
#endif
    } else {
      event_t evt_reached = event;

      for (const auto& line : *sv) {
        int8_t signal_probe = 0;
        event = evt_reached;
        time_t tm;
        auto result = parse_signal_line(
          line, event, signal_probe, start_trade, test_signal, tm);
        if (result != 0) {
          continue;
        }
        if (start_trade) {
          event |= basic_data_changed(signal_probe, tm);

          if (!ret) {
            ret = energy_battle(event, tm);
          }

          if (!ret) {
            ret = check_ced_change(event);
          }

          if (!ret)
            ret = time_battle(event);

          if (g_start_idx <= idx__) {
            if (first_time_) {
              event |= (EVENT_CE_DIR_CHANGED | EVENT_BIG_DIR_CHANGED |
                        EVENT_SWING_DIR_CHANGED | EVENT_TTS_CHANGED |
                        EVENT_FIGHT_DIR_CHANGED);
              first_time_ = false;
            }

            if (!ret)
              ret = fighting(event);
          }

          if (ret < 0) {
            break;
          }
        }
      } // signals-looping

      if (ret < 0) {
        printf("Quit The Make Deal Program with %d\n", ret);
        break;
      }

      if (!ret) {
        int v[4] = { 0, 0, 0, 0 };
        int vol = mock.mock_deal(idx__, hp_now_, td[i].ftime, v);

        if (vol) {
          show_revenue();
        }
      }

    } // if - find_strings

    if (idx__ >= g_start_idx) {
      ret = tjm_do_pet_actions();
      if (ret) {
        printf("_._._._._._._._._._._._._._._._._._._._._._._._._._.\n\n");
      }
#if (STONE_DEBUG || STEP_BY_STEP)
      if (ret) {
        if (step_by_step() < 0) {
          break;
        }
      }
#endif // (STONE_DEBUG || STEP_BY_STEP)
    }
  } // tick loop

  if (Tom.is_running()) {
    DealCasePtr dc = std::make_shared<DealCase>();
    dc->case_no = 9999;
    tjm_action_tom_stop(dc);
  }

  printf("ALL REVENUE %.02lf $ (%.02lf hop)\n", revenue * g_hop, revenue);

#if (SAVE_STOP_IDX_TO_FILE)
  save_all_deal();
#endif // SAVE_STOP_IDX_TO_FILE

  return (0);
}
#endif // ONLINE_DATA_COMM

void
MakeDeal::set_hop(const double h) noexcept
{
  g_hop = h;
}

bool
MakeDeal::good_params() noexcept
{
  return (dirs.mid_dir && dirs.ein_dir && lines.nup && lines.ndn &&
          (wfw.valied()));
}

void
MakeDeal::show_revenue() noexcept
{
#if (STONE_DEBUG || STEP_BY_STEP || ONLINE_DATA_COMM)
  printf("\n\033[38;5;46mRVN\033[0m\n");
  mock.revenue.print(idx__);
  mock.pos_up.print(hp_now_, idx__);
  mock.pos_dn.print(hp_now_, idx__);
  mock.mock_print_orders_();
  printf("_____________________RVN : g%ld\n\n", idx__);
#endif
}

void
MakeDeal::show_params() noexcept
{
#if (STONE_DEBUG || STEP_BY_STEP || ONLINE_DATA_COMM)
#if ONLINE_DATA_COMM
  // send_signal_energy_dir_changed();
  // send_signal_pets_was_changed();
  printf("\n\033[38;5;46mPARAMS\033[0m : %.02lf %ld\n", hp_now_, t_now_);
#else
  printf(
    "\n\033[38;5;46mPARAMS\033[0m : g%ld %ld %.02lf\n", idx__, idx__, hp_now_);
#endif // ONLINE

  lines.print(idx__, hp_now_);
  // dirs.print(t_now_, wave.dir, idx__);
  wave.print(dirs.mid_dir, dirs.ein_dir, idx__);
  wfw.print(t_now_, hp_now_);
  Jerry.print("Jerry", idx__, hp_now_, t_now_);
  mickey.print("Mickey", idx__, hp_now_, t_now_);
  Tom.print("Tom", idx__, hp_now_, t_now_);

  // em_up.print(" now + ");
  /// em_dn.print(" now - ");

  // pdc.print(t_now_);
  swing.print(t_now_, hp_now_);
  print_reached("swing",
                ((swing.dir_swing) ? swing.dir_swing : 1),
                swing.target.reached,
                idx__);

  // dd_bd.print(t_now_, idx__);

  printf("\033[38;5;46mS%d; %dB(%d,%dm %d,%dm L%d %dm); CE(%d,%dm %d,%dm); "
         "F(%d,%dm %d,%dm); E(%d,%dm %d,%dm); PDC(%d,%dm %d,%dm)\033[0m\n",
         swing.dir_swing,
         dirs.big_dir,
         dd_bd.get_first_dir(),
         dd_bd.get_first_durantion(t_now_),
         dd_bd.get_second_dir(),
         dd_bd.get_second_durantion(),
         dd_bd.long_dir,
         dd_bd.long_dur,
         dd_ced.get_first_dir(),
         dd_ced.get_first_durantion(t_now_),
         dd_ced.get_second_dir(),
         dd_ced.get_second_durantion(),
         dd_ft.get_first_dir(),
         dd_ft.get_first_durantion(t_now_),
         dd_ft.get_second_dir(),
         dd_ft.get_second_durantion(),
         dd_em.get_first_dir(),
         dd_em.get_first_durantion(t_now_),
         dd_em.get_second_dir(),
         dd_em.get_second_durantion(),
         dd_pdc.get_first_dir(),
         dd_pdc.get_first_durantion(t_now_),
         dd_pdc.get_second_dir(),
         dd_pdc.get_second_durantion());
  printf("\033[38;5;46mF+%u -%u; E+%u -%u; PDC(%d,%d %d,%d)\033[0m\n",
         ft_up,
         ft_dn,
         em_up.force_case,
         em_dn.force_case,
         pdc.mid.cur_dir,
         pdc.ein.cur_dir,
         pdc.mid.possible_dir,
         pdc.ein.possible_dir);

  printf("\033[38;5;168m  ~~~~~~~~ The Dir = %d ~~ %.02lf ~~~ U %d %d mins D "
         "%d %d mins ~~~\033[0m\n",
         the_dir,
         dd_ced.get_hp_delta(),
         TO_MINUTES(get_strong_timer___(1)),
         TO_MINUTES(get_weak_timer___(1)),
         TO_MINUTES(get_strong_timer___(-1)),
         TO_MINUTES(get_weak_timer___(-1)));

  turns.print(idx__, t_now_, hp_now_);
  double large_to_now = 0, large_to_now_with_large = 0, large_to_new = 0,
         large_to_new_with_large = 0, oppo_to_new = 0,
         oppo_to_new_with_large = 0, oppo_to_now = 0,
         oppo_to_now_with_large = 0, delta_first_to_now = 0;

  turns.up_to_now(t_now_,
                  hp_now_,
                  large_to_new,
                  large_to_new_with_large,
                  oppo_to_new,
                  oppo_to_new_with_large,
                  delta_first_to_now);
  large_to_now = large_to_new + delta_first_to_now;
  large_to_now_with_large = large_to_new_with_large + delta_first_to_now;
  oppo_to_now = oppo_to_new + delta_first_to_now;
  oppo_to_now_with_large = oppo_to_new_with_large + delta_first_to_now;
  printf("<<<<<< %d, tjm %d | now %.02lf %.02lf new %.02lf %.02lf o-now %.02lf "
         "%.02lf "
         "o-new %.02lf %.02lf >>>>>>\n",
         dir_turns_space,
         tjm_dir,
         large_to_now,
         large_to_now_with_large,
         large_to_new,
         large_to_new_with_large,
         oppo_to_now,
         oppo_to_now_with_large,
         oppo_to_new,
         oppo_to_new_with_large);

  printf("\033[38;5;46mSPACE(%d,%dm %d,%dm); TTS(%d,%dm %d,%dm); BigNeddle "
         "%d\033[0m\n",
         dd_sd.get_first_dir(),
         dd_sd.get_first_durantion(t_now_),
         dd_sd.get_second_dir(),
         dd_sd.get_second_durantion(),
         dd_tts.get_first_dir(),
         dd_tts.get_first_durantion(t_now_),
         dd_tts.get_second_dir(),
         dd_tts.get_second_durantion(),
         Tom.incomming.d_big_reverse);

  Tom.close_price.print(hp_now_);
#if ONLINE_DATA_COMM
  printf("_____________________OARANS : %.02lf %ld\n\n", hp_now_, t_now_);
#else
  printf(
    "_____________________OARANS : g%ld %ld %.02lf\n\n", idx__, idx__, hp_now_);
#endif // ONLINE

#endif
}

#if (STEP_BY_STEP)
static int64_t prev_idx = 0;
extern std::string event_show_type;
extern std::string tjm_run_type;

int64_t
MakeDeal::step_by_step() noexcept
{
  if (idx__ < g_stop_idx || idx__ <= prev_idx)
    return (0);

  prev_idx = idx__;
  int64_t ret = 0;
enter_something:
  std::string str;
  std::cout << "Enter Command: " << idx__ << " ";
  std::cin >> str;
  if (str == "quit") {
    ret = -1;
    goto exit_return;
  } else if (str == "p") {
    show_params();
  } else if (str == "r") {
    mock.pos_up.print(hp_now_, idx__);
    mock.pos_dn.print(hp_now_, idx__);
    mock.revenue.print(idx__);
  } else if (str == "o") {
    mock.mock_print_orders_();
  } else if (str[0] == 'g') {
    g_stop_idx = std::stoll(str.c_str() + 1);
    printf("Next stop is %ld\n", g_stop_idx);
    goto exit_return;
  } else if (str[0] == 'f') {
    g_stop_idx = std::stoll(str.c_str() + 1) * 60 + idx__;
    printf("Next stop is %ld\n", g_stop_idx);
    goto exit_return;
  } else if (str == "tu") {
    tjm_add_action_tom_run(1, 1, hp_now_, 1, true);
    tjm_do_pet_actions();
    Tom.open_price(idx__, t_now_, hp_now_);
    show_params();
  } else if (str == "tn") {
    tjm_add_action_tom_run(-1, 1, hp_now_, 1, true);
    tjm_do_pet_actions();
    Tom.open_price(idx__, t_now_, hp_now_);
    show_params();
  } else if (str == "ts") {
    tjm_add_action_tom_stop(9997);
    tjm_do_pet_actions();
    show_params();
  } else if (str == "ju") {
    tjm_add_action_jerry_run(1, 9998);
    tjm_do_pet_actions();
    show_params();
  } else if (str == "jn") {
    tjm_add_action_jerry_run(-1, 9998);
    tjm_do_pet_actions();
    show_params();
  } else if (str == "js") {
    tjm_add_action_jerry_stop(9996);
    tjm_do_pet_actions();
    show_params();
  } else if (str == "mu") {
    tjm_add_action_mickey_run(1, 9999);
    tjm_do_pet_actions();
    show_params();
  } else if (str == "mn") {
    tjm_add_action_mickey_run(-1, 9999);
    tjm_do_pet_actions();
    show_params();
  } else if (str == "ms") {
    tjm_add_action_mickey_stop(9995);
    tjm_do_pet_actions();
    show_params();
  } else if (str[0] == 'e') {
    event_show_type = str;
  } else if (str == "mtjm") {
    tjm_run_type = str;
    show_params();
  } else if (str == "atjm") {
    tjm_run_type = str;
    show_params();
  } else if (str == "help") {
    printf(
      "p - show_params; r - mock position and revenue; o - mock orders;\n"
      "tu, tn, ts - Tom up down stop; ju, jn, ts - Jerry up down stop;\n"
      "g____ go to next idx; f____ go to next minutes; help - show this\n"
      "mtjm: manual run tjm, atjm: auto run tjm\n"
      "event to stop: \n"
      "  eb es esb - bigdir, swing; e3d - ce, bd, fting, pdc dir changed\n"
      "  ece/ectj - CE dir chaged, ets - TTS changed\n"
      "  etr/ect ejr emr - Tom, Jerry, mickey reached; epr - pets reached\n"
      "  erp - Tom reaced price; eall - all event; eno - no event stop\n"
      "  esd - SpaceDir; estj - space-dir and Tom and Jerry\n");
  } else {
    goto exit_return;
  }
  goto enter_something;

exit_return:
  return ret;
}
#endif // STEP_BY_STEP

// default font family 'Droid Sans Mono', 'monospace', monospace

void
Pricing::print(const double hpx) noexcept
{
  printf("Pricing D%d T%d | %d %d %d | %.02lf %.02lf %.02lf, take %d unit\n",
         d,
         asap_type,
         pr_hpx,
         pr_line_1,
         pr_line_2,
         this->hpx,
         line1,
         line2,
         take_unit);
}

void
MakeDeal::recall_all_orders_()
{
  mock.mock_recall_all_close_order(1, idx__);
  mock.mock_recall_all_close_order(-1, idx__);
  mock.mock_recall_all_open_order(1, idx__);
  mock.mock_recall_all_open_order(-1, idx__);
}

int
MakeDeal::check_position__(const dir_t d) noexcept
{
  auto pos = mock.mock_get_position_(d);
  if (pos->vol < 1)
    return (0);

  return (0);
}

uint8_t
MakeDeal::dirs_was_changed__() noexcept
{
  if (!d5.wave && !wave.dir)
    return (0);

  dir_t ed = lines.ext_rgn.ext_dir(hp_now_);

  uint8_t result = 0;
  if (dirs.mid_dir && !d5.mid)
    d5.mid = dirs.mid_dir;
  if (dirs.ein_dir && !d5.ein)
    d5.ein = dirs.ein_dir;
  if (wfw.g2_dir && !d5.g2)
    d5.g2 = wfw.g2_dir;

  if (wave.dir && ed == wave.dir) {
    if (!d5.wave && !d5.ext) {
      d5.ext = d5.wave = ed;
      result = 1;
    } else {
      if (d5.wave == -ed) {
        d5.wave = d5.ext = ed;
        result = 1;

        if (d5.mid == -dirs.mid_dir) {
          d5.mid = dirs.mid_dir;
          result |= 2;
        }

        if (d5.ein == -dirs.ein_dir) {
          d5.ein = dirs.ein_dir;
          result |= 4;
        }

        if (d5.g2 == -wfw.g2_dir) {
          d5.g2 = wfw.g2_dir;
          result |= 8;
        }
      }
    }
  }

  if (result) {
    // #if (STONE_DEBUG)
    //     int8_t color = (result > 1) ? 26 : 28;
    //     printf("\033[38;5;%dm ----- the Bear dir. w%d l%d v%d e%d g%d "
    //            "-----\033[0m g%ld\n",
    //            color,
    //            wave.dir,
    //            d5.ext,
    //            dirs.mid_dir,
    //            dirs.ein_dir,
    //            wfw.g2_dir,
    //            idx__);
    // #endif // (STONE_DEBUG)

    if (bear_dir != d5.wave) {
      // #if (STONE_DEBUG)
      //       printf(
      //         "\033[38;5;26m -----  the BEAR dir %d <=== %d  -----\033[0m
      //         g%ld\n", d5.wave, bear_dir, idx__);
      // #endif // (STONE_DEBUG)

      bear_dir = d5.wave;
    }
  } else {
    d5.mid = dirs.mid_dir;
    d5.ein = dirs.ein_dir;
    d5.g2 = wfw.g2_dir;
  }

  return result;
}

int8_t
MakeDeal::parse_signal(const char* line, time_t& tm_signal) noexcept
{
  int8_t signal_probe = 0;
  int ret = 0;

  Document doc;
  if (doc.Parse(line).HasParseError()) {
    printf("Parse Error at %s\n", line);
    ret = -1000;
  }

  int type = 0;
  if (!ret) {
    if (doc.HasMember("t")) {
      type = doc["t"].GetInt();
      // printf("Type %d ", type);
      // if (type)
      //   printf("%d, %s\n", type, line);
    } else {
      printf("Error type at %s\n", line);
      ret = -1001;
    }
  }

  if (!ret) {
    if (doc.HasMember("i")) {
      tm_signal = time_t(doc["i"].GetInt64());
      if (tm_signal < t_now_) {
        printf("TmError %ld < t_now %ld\n", tm_signal, t_now_);
        return (0);
      }
    } else {
      if (type != 0) {
        printf("Error message, no time. %s\n", line);
        ret = -1002;
      }
    }
  }

  if (!ret) {
    switch (type) {
      case 0:
        ret = ParseTick(doc, tick, line);
        if (!ret) {
          hp_now_ = tick.price / g_hop;
          t_now_ = time_t(tick.ftime);
        }
        break;
      case 1: {
        ret = ParseLines(doc, lines, line);
        // lines.print(idx__, hp_now_);
        if (ret)
          ret = -10000;
        signal_probe |= 1;
      } break;
      case 10: {
        // StageDirs sd;
        ret = ParseDirs(doc, dirs, line);
        dirs.check_zero_dir();
        // dirs.print(td[i].ftime, wave.dir, idx__);
        signal_probe |= 2;
        if (ret)
          ret = -20000;
      } break;
      case 11: {
        // Wave w;
        ret = ParseWave(doc, wave, line);
        // wave.print(dirs.vss_dir, dirs.ein_dir, idx__);
        signal_probe |= 4;
        if (ret)
          ret = -30000;

      } break;
      case 12:
        break;
      case 13:
        ret = ParseWfw(doc, wfw, line);
        // wfw.print(t_now_);
        signal_probe |= 16;
        if (ret)
          ret = -50000;

        break;
      default:
        ret = -60000;
        break;
    }
  }

  if (ret < 0) {
    printf("Error at Type %d, err %d. %s\n", type, ret, line);
  } else {
    ret = signal_probe;
  }

  return ret;
}

int
MakeDeal::check_zzzz() noexcept
{
  int ret = 0;
  bool zz_zz = (!wave.solved_cls_spp && !wave.solved_vss_spp);
  if (zz_zz) {
    if (!swing.swing) {
      ret = 1;
      swing.set(idx__, hp_now_, t_now_, lines);

#if (STONE_DEBUG || STEP_BY_STEP)
      // swing.print(t_now_, hp_now_);
#endif

#if (ONLINE_DATA_COMM)
      printf("(((((( \033[48;5;001mZZ ZZ\033[0m )))) g%ld %ld\n",
             t_now_,
             t_now_ + 3600 + 1800);
#endif // ONLINE_DATA_COMM
    }
  }

  return ret;
}

#if (SAVE_STOP_IDX_TO_FILE)
static FILE* fp_save_idx = nullptr;
bool
init_save_stop_file() noexcept
{
  fp_save_idx = fopen("stop_idx.log", "a");
  return (fp_save_idx != nullptr);
}

void
close_save_stop_file() noexcept
{
  fclose(fp_save_idx);
}

static int64_t prev_saved_idx = 0;

bool
save_stop_idx_to_file(const int64_t idx,
                      const double hp_delta,
                      const dir_t dir,
                      const int t[4],
                      const dir_t bd,
                      const bool swing,
                      const dir_t sdir,
                      const double hpx,
                      const Lines& l) noexcept
{
  if (!fp_save_idx)
    init_save_stop_file();
  if (fp_save_idx) {
    bool good = true;
    if (std::abs(hp_delta) > 4.3) {
      good = (fprintf(fp_save_idx, "- ") == 2);
    }

    if (good)
      good = (fprintf(fp_save_idx,
                      "g%ld %.02lf %d; U%d D%d g%d g%d |",
                      idx,
                      hp_delta,
                      dir,
                      t[0],
                      t[1],
                      t[2],
                      t[3]) > 0);
    if (good) {
      good =
        (fprintf(fp_save_idx,
                 "B%d %c%d | %.02lf | %d(%.02lf %.02lf) %d(%.02lf %.02lf) \n",
                 bd,
                 ((swing) ? 'S' : ' '),
                 sdir,
                 hpx,
                 l.nup,
                 (hpx - l.hp_up_1),
                 (hpx - l.hp_up_2),
                 l.ndn,
                 (hpx - l.hp_dn_1),
                 (hpx - l.hp_dn_2)));
    }
    prev_saved_idx = idx;
    return good;
  } else {
    return (false);
  }
}
#endif // SAVE_STOP_IDX_TO_FILE

void
MakeDeal::save_all_deal() noexcept
{
  FILE* fp = fopen("deal-records.csv", "w");
  fprintf(fp, "ALL REVENUE %.02lf $ (%.02lf hop)\n", revenue * g_hop, revenue);
  for (const auto& deal : tdv) {
    // fprintf(fp,
    //         "%d, %c, %d, %.02lf, g%ld, %ld, %.02lf, g%ld, %ld, %.02lf,
    //         %.02lf, "
    //         "%.02lf, %ld, "
    //         "%.02lf, %.02lf, %ld, %.02lf, %.02lf, %.02lf\n",
    //         deal->index,
    //         deal->dir,
    //         deal->hited,
    //         deal->hpx_avg,
    //         deal->o_idx,
    //         deal->o_tm,
    //         deal->o_hp,
    //         deal->c_idx,
    //         deal->c_tm,
    //         deal->c_hp,
    //         deal->rvn_max,
    //         deal->hp_rvn_max,
    //         deal->idx_rvn_max,
    //         deal->rvn_min,
    //         deal->hp_rvn_min,
    //         deal->idx_rvn_min,
    //         deal->rvn_px,
    //         deal->rvn_hp,
    //         (deal->max_loss_px - deal->o_hp));
    fprintf(fp,
            "%d, %c, %d, %.02lf, %.02lf, g%ld, g%ld, %.02lf, g%ld, %.02lf, "
            "g%ld, %.02lf, "
            "%.02lf\n",
            deal->index,
            deal->dir,
            deal->hited,
            deal->hpx_avg,
            deal->o_hp,
            deal->o_idx,
            deal->c_idx,
            deal->rvn_max,
            deal->idx_rvn_max,
            deal->rvn_min,
            deal->idx_rvn_min,
            deal->rvn_px,
            deal->rvn_hp);
  }
  fprintf(fp, "\n");

  fclose(fp);
}

} // namespace Trade
