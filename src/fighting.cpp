#include "fighting.hpp"
#include "dirs.hpp"
#include "energy.hpp"
#include "lines.hpp"
#include "make_deal.hpp"
#include "schrodinger.hpp"
#include "tjm.hpp"
#include "wfw.hpp"
#include <cstdint>
#include <cstdio>
#include <ctime>
#include <memory>

/*
 * 1. I need the fastest move, but I can not waiting for it.
 * 2. I do not konw how huge loss when I close something.
 */

namespace Trade {

void
print_pet_action(const uint32_t action) noexcept
{
  /*
    DA_RUN_TOM = 1,
    DA_RUN_JERRY = 2,
    DA_RUN_MIKEY = 4,
    DA_STOP_TOM = 8,
    DA_STOP_JERRY = 16,
    DA_STOP_MIKEY = 32,
    DA_CLOSE_POSITION = 64,
    DA_OPEN_POSITION = 128,
    DA_EMPTY_POSITION = 256,
  */
}

int
fight_add_fight_action(DealActionVec& dav,
                       const DEAL_ACTION action,
                       const dir_t d) noexcept
{
  for (const auto& dv : dav) {
    if (dv->action == action && dv->dir == d)
      return (0);
  }
  dav.push_back(std::make_shared<DealAction>(d, action));
  return 1;
}

int
MakeDeal::tjm_do_pet_actions() noexcept
{
  bool good = verify_tjm_action();
  if (!good)
    return (12345);

  int ret = 0;
  for (const auto& action : tjm) {
    switch (action->pet_action) {
      case PET_ACTION::JERRY_RUN:
        ret |= tjm_action_jerry_run(action->d, action->dc);
        break;
      case PET_ACTION::JERRY_STOP:
        ret |= tjm_action_jerry_stop(action->dc);
        break;
      case PET_ACTION::TOM_RUN:
        ret |= tjm_action_tom_run(action->d, action->dc);
        break;
      case PET_ACTION::TOM_STOP:
        ret |= tjm_action_tom_stop(action->dc);
        break;
      case PET_ACTION::MICKEY_RUN:
        ret |= tjm_action_mickey_run(action->d, action->dc);
        break;
      case PET_ACTION::MICKEY_STOP:
        ret |= tjm_action_mickey_stop(action->dc);
        break;
      case PET_ACTION::SET_CLOSE_ALL:
        tjm_action_set_close_all(action->dc);
        ret |= PET_ACTION::SET_CLOSE_ALL;
        break;
      case PET_ACTION::SET_CLOSE_PART:
        tjm_action_set_close_part(action->dc);
        ret |= PET_ACTION::SET_CLOSE_PART;
        break;
      case PET_ACTION::CLOSE_PART:
        ret |= tjm_action_close_part(action->dc);
        break;
      case PET_ACTION::CLOSE_ALL:
        ret |= tjm_action_close_all(action->dc);
        break;
      case PET_ACTION::OPENED:
        ret |= tjm_action_opened(action->dc);
        break;
      default:
        break;
    }
  }

  clear_tjm_action();

  return ret;
}

int
MakeDeal::tjm_add_action_mickey_run(const dir_t d, const uint32_t cno) noexcept
{
  DealCasePtr dc = std::make_shared<DealCase>(d, cno);
  bool appended = add_tjm_action(d, PET_ACTION::MICKEY_RUN, dc, t_now_);
  if (appended)
    return PET_ACTION::MICKEY_RUN;
  else
    return PET_ACTION::PET_NONE;
}

int
MakeDeal::tjm_add_action_mickey_stop(const uint32_t cno) noexcept
{
  auto dc = std::make_shared<DealCase>(0, cno);
  bool apend = add_tjm_action(0, PET_ACTION::MICKEY_STOP, dc, t_now_);
  if (apend)
    return PET_ACTION::MICKEY_STOP;
  else
    return (0);
}

int
MakeDeal::tjm_add_action_tom_run(const dir_t d,
                                 const uint32_t cno,
                                 const double hp_base,
                                 const case_t hp_base_cno,
                                 bool b_set_forced_stop) noexcept
{
  DealCasePtr dc = std::make_shared<DealCase>(d, cno);

  // Set open price and forced-stop price
  dc->open = std::make_shared<OpenPrice>(
    d, hp_base_cno, (hp_base_cno == PRICE_CNO_GOOD_OPPOSITE_LINE2), hp_base);
  if (b_set_forced_stop && !Tom.is_running()) {
    dc->close = std::make_shared<ClosePrice>();
    tjm_cal_close_forced(cno, hp_base_cno, (!Tom.is_running()), dc->close);
  }

  bool appended = add_tjm_action(d, PET_ACTION::TOM_RUN, dc, t_now_);
  if (appended) {
    return PET_ACTION::TOM_RUN;
  } else {
    return PET_ACTION::PET_NONE;
  }
}

int
MakeDeal::tjm_add_action_tom_run(const dir_t d, const DealCasePtr& dc) noexcept
{
  bool appended = add_tjm_action(d, PET_ACTION::TOM_RUN, dc, t_now_);
  if (appended)
    return PET_ACTION::TOM_RUN;
  else
    return PET_ACTION::PET_NONE;
}

int
MakeDeal::tjm_add_action_tom_stop(const case_t cno) noexcept
{
  if (Jerry.is_running())
    tjm_add_action_jerry_stop(cno);

  auto dc = std::make_shared<DealCase>(0, cno);
  bool appended = add_tjm_action(0, PET_ACTION::TOM_STOP, dc, t_now_);
  if (appended)
    return PET_ACTION::TOM_STOP;
  else
    return PET_ACTION::PET_NONE;
}

int
MakeDeal::tjm_add_action_set_stop(const uint32_t cno) noexcept
{
  printf("Add Stop action\n");
  if (Jerry.is_running()) {
    tjm_add_action_jerry_stop(cno);
  }
  return tjm_add_action_tom_stop(cno);
}

int
MakeDeal::tjm_add_action_set_reverse(const uint32_t cno) noexcept
{
  printf("Add reverse action\n");
  const dir_t d = Tom.get_dir();
  if (Jerry.is_running()) {
    tjm_add_action_jerry_stop(cno);
  }
  tjm_add_action_tom_stop(cno);

  double hp_base = 0;
  case_t hp_base_cno = 0;
  bool good = tjm_good_entry_price(-d, hp_base, hp_base_cno);
  if (good) {
    if (tjm_add_action_tom_run(-d, cno, hp_base, hp_base_cno, true)) {
      return PET_ACTION::TOM_RUN;
    }
  }

  return PET_ACTION::PET_NONE;
}

int
MakeDeal::tjm_add_action_jerry_run(const dir_t d, const uint32_t cno) noexcept
{
  auto dc = std::make_shared<DealCase>(d, cno);
  //
  // CHOOSE Set the good-to-win or min-win close-prices
  //

  bool appended = add_tjm_action(d, PET_ACTION::JERRY_RUN, dc, t_now_);
  if (appended)
    return PET_ACTION::JERRY_RUN;
  else
    return PET_ACTION::PET_NONE;
}

int
MakeDeal::tjm_add_action_jerry_stop(const uint32_t cno) noexcept
{
  auto dc = std::make_shared<DealCase>(0, cno);
  bool apend = add_tjm_action(0, PET_ACTION::JERRY_STOP, dc, t_now_);
  if (apend)
    return PET_ACTION::JERRY_STOP;
  else
    return (0);
}

int
MakeDeal::tjm_action_tom_run(const dir_t d, const DealCasePtr& dc) noexcept
{
  if (!d)
    return 0;

  int ret = 0;
  if (d == Tom.get_dir()) {
    // CHOOSE should I set the stop price ???D
    printf(
      "<<< Signal >>> Run Tom to %d, with open-base-price $%.02lf (%.02lf)\n",
      d,
      dc->open->hp_base * g_hop,
      dc->open->hp_base);
  } else {
    ret |= tjm_action_tom_stop(dc);

    time_t tm1 = strong_timer___(d);
    time_t tm2 = weak_timer___(d);
    if (tm1 < t_now_ + MINUTES_30)
      tm1 = t_now_ + MINUTES_45;
    else if (tm1 - t_now_ > TWO_HOURS)
      tm1 = t_now_ + TWO_HOURS;
    if (tm2 < t_now_ + MINUTES_30)
      tm2 = t_now_ + MINUTES_45;

    Lines l;
    l.copy((swing.swing) ? swing.target.lines : lines);

    Tom.run(
      d, tm1, tm2, l, hp_now_, t_now_, idx__, d5, get_energy_mark(d), dc, 1);
    Tom.case_no = dc->case_no;
    Tom.closed_part_times = false;

    if (dc->open) {
      Tom.os_entries.update_open_prices(
        dc->open->hp_base, d, (dc->open->good_for_now));
    } else {
      printf("Error, Have No Open Price!\n");
    }

    if (dc->close && dc->close->set_these_prices == CLOSE_PRICE_STOP_LOSS) {
      Tom.close_price.set_prices(dc->close.get());
    }
    // Tom.closed_price.set(dc->stop.get(), dc->case_no);

    ret |= PET_ACTION::TOM_RUN;
  }

  return ret;
}

int
MakeDeal::tjm_action_jerry_run(const dir_t d, const DealCasePtr& dc) noexcept
{
  int ret = 0;
  if (Jerry.get_dir() != d) {
    printf("_______________________Jerry run__________________________\n");
    auto& em_mouse = get_energy_mark(d);
    // time_t tm1 = get_strong_timer___(d);
    // time_t tm2 = get_weak_timer___(d);
    time_t tm = weak_timer___(d);
    Jerry.run(d, tm, tm, lines, hp_now_, t_now_, idx__, d5, em_mouse, dc, 2);
    Jerry.set_open_entries(hp_now_, 2);
    if (dc->close)
      Tom.close_price.set_prices(dc->close.get());
    printf("~~~~~~~~~~~~~~~~~~~~~~~Jerry run~~~~~~~~~~~~~~~~~~~~~~~~~~\n");

    ret = PET_ACTION::JERRY_RUN;
  } else {
  }

  return ret;
}

int
MakeDeal::tjm_action_mickey_run(const dir_t d, const DealCasePtr& dc) noexcept
{
  if (mickey.get_dir() != d) {
    printf("_______________________Mickey run__________________________\n");
    auto& em_mouse = get_energy_mark(d);
    time_t tm = weak_timer___(d);
    if (tm - t_now_ > MINUTES_45)
      tm = MINUTES_45;
    const auto& l = (swing.swing) ? swing.target.lines : lines;
    mickey.run(d, tm, tm, l, hp_now_, t_now_, idx__, d5, em_mouse, dc, 3);
    double base_hpx = 0;
    tjm_get_entry_price(d, base_hpx);
    mickey.set_open_entries(base_hpx, 2);
    printf("~~~~~~~~~~~~~~~~~~~~~~~Mickey run~~~~~~~~~~~~~~~~~~~~~~~~~~\n");

    return PET_ACTION::MICKEY_RUN;
  }

  return (0);
}

int
MakeDeal::tjm_action_tom_stop(const DealCasePtr& dc) noexcept
{
  if (Tom.get_dir()) {
    revenue += Tom.incomming.rvn_total;
    ++deal_times;

    auto td = std::make_shared<TheDeal>();
    td->dir = (Tom.get_dir() > 0) ? 'l' : 's';
    td->to_amount = Tom.amout_to;
    td->reached = Tom.target.reached;
    td->c_case = dc->case_no;
    td->index = deal_times;
    td->o_idx = Tom.my_idx;
    td->o_tm = Tom.my_tm;
    td->o_hp = Tom.os_entries.mpa[0].px;
    td->c_idx = idx__;
    td->c_tm = t_now_;
    td->c_hp = hp_now_;
    td->hpx_avg = Tom.os_entries.average;
    td->hited = Tom.os_entries.amount_reached;
    td->rvn_px = Tom.incomming.rvn_total * g_hop;
    td->rvn_hp = Tom.incomming.rvn_single;
    td->rvn_max = Tom.incomming.rvn_max;
    td->idx_rvn_max = Tom.incomming.idx_rvn_max;
    td->hp_rvn_max = Tom.incomming.hp_rvn_max;
    td->rvn_min = Tom.incomming.rvn_min;
    td->idx_rvn_min = Tom.incomming.idx_rvn_min;
    td->hp_rvn_min = Tom.incomming.hp_rvn_min;
    tdv.push_back(td);

    printf(
      "--- \033[38;5;200mCat %d Revenue %.02lf$ (%.02lf) All Revenue %.02lf$ "
      "(%.02lf) Times %u\033[0m ---\n",
      Tom.get_dir(),
      Tom.incomming.rvn_total * g_hop,
      Tom.incomming.rvn_total,
      revenue * g_hop,
      revenue,
      deal_times);

    // old_tom.copy_from(&Tom);
    Tom.stop(idx__, t_now_, dc->case_no);

    jerry_to_amount = 0;
    jerry_reached = 0;

    return PET_ACTION::TOM_STOP;
  }
  return (0);
}

int
MakeDeal::tjm_action_jerry_stop(const DealCasePtr& d [[maybe_unused]]) noexcept
{
  int ret = 0;
  if (Jerry.get_dir()) {
    Jerry.stop(idx__, t_now_, d->case_no);
    ret |= PET_ACTION::JERRY_STOP;
  }
  // renew_cat_timer();
  return ret;
}

int
MakeDeal::tjm_action_mickey_stop(const DealCasePtr& d [[maybe_unused]]) noexcept
{
  int ret = 0;
  if (mickey.get_dir()) {
    mickey.stop(idx__, t_now_, d->case_no);
    ret = PET_ACTION::MICKEY_STOP;
  }
  return ret;
}

int
MakeDeal::tjm_action_set_close_all(const DealCasePtr& dc) noexcept
{
  if (dc && dc->close) {
    Tom.close_price.set_prices(dc->close.get());
    return PET_ACTION::SET_CLOSE_ALL;
  }

  return PET_ACTION::PET_NONE;
}

int
MakeDeal::tjm_action_set_close_part(const DealCasePtr& dc) noexcept
{
  if (dc && dc->close) {
    Tom.close_price.set_prices(dc->close.get());
    return PET_ACTION::SET_CLOSE_ALL;
  }

  return PET_ACTION::PET_NONE;
}

int
MakeDeal::tjm_action_close_part(const DealCasePtr& dc) noexcept
{
  if (!Tom.is_running()) {
    printf("<< !Warning! >> Tom closed all %d poision already\n",
           Tom.get_dir());
    return PET_ACTION::PET_NONE;
  }
  if (Tom.closed_all) {
    printf("<< !Warning! >> Tom closed all %d poision already\n",
           Tom.get_dir());
    return PET_ACTION::PET_NONE;
  }

  // TODO Send Signal to Market-API
  Tom.close_price.cancel_part(dc->close->hited_price_type);
  Tom.closed_part_times++;

  printf("<< Signal >> Close Part %d poision, Rvn %.02lf, part rvn "
         "%.02lf, Times %u\n",
         dc->dir,
         Tom.incomming.rvn_single,
         Tom.close_price.closed_rvn,
         Tom.closed_part_times);
  return PET_ACTION::CLOSE_PART;
}

int
MakeDeal::tjm_action_close_all(const DealCasePtr& dc) noexcept
{
  if (!Tom.is_running()) {
    printf("<< !Warning! >> Tom closed all %d poision already\n",
           Tom.get_dir());
    return PET_ACTION::PET_NONE;
  }
  if (Tom.closed_all) {
    printf("<< !Warning! >> Tom closed all %d poision already\n",
           Tom.get_dir());
    return PET_ACTION::PET_NONE;
  }

  Tom.close_price.cancel_all(dc->close->hited_price_type);

  // TODO Send Signal to Market-API
  printf("<< Signal >> CloseALL %d poision, Rvn %.02lf\n",
         dc->dir,
         Tom.incomming.rvn_single);
  return PET_ACTION::CLOSE_ALL;
}

int
MakeDeal::tjm_action_opened(const DealCasePtr& dc) noexcept
{
  return PET_ACTION::OPENED;
}

int
MakeDeal::tjm_action_cancel_seted_close(const DealCasePtr& dc) noexcept
{
  if (dc) {
    Tom.close_price.cancel(dc->cancel_close);
  }
  return PET_ACTION::PET_NONE;
}

int
MakeDeal::tjm_action_tom_open(const dir_t d, const double hpx) noexcept
{
  // TODO Send Signal to Market-API
  printf("<< Signal >> Open %d position, %.02lf\n", d, hpx);
  return PET_ACTION::OPENED;
}

dir_t
MakeDeal::fight_strong_compound_energy_dir() noexcept
{
  const dir_t up = 1;
  const dir_t dn = -1;

  dir_t d = 0;
  if (pdc.no_mid_ein()) {
    if (dd_em.get_first_dir() == up && dd_ft.get_first_dir() == up) {
      d = 1;
    } else if (dd_em.get_first_dir() == dn && dd_ft.get_first_dir() == dn) {
      d = -1;
    }
  } else {
    if (dd_em.get_first_dir() == up) {
      // Try 1
      if (dd_ft.get_first_dir() == up) {
        if (dd_pdc.get_first_dir() == up) {
          // dd_pdc.first_pdc_time_is_good(t_now_))
          d = 1;
        }
      }
    } else if (dd_em.get_first_dir() == dn) {
      // Try -1
      if (dd_ft.get_first_dir() == dn) {
        if (dd_pdc.get_first_dir() == dn) {
          // dd_pdc.first_pdc_time_is_good(t_now_)) {
          d = -1;
        }
      }
    }
  }
  return (d);
}

uint32_t
MakeDeal::fight_the_side_(const dir_t d, const bool is_opposite) noexcept
{
  uint32_t ret = 0;
  if (wfw.g2_dir == d)
    ret |= DFI_G2_DIR;
  if (sign(wfw.g2_hpx - hp_now_) == d)
    ret |= DFI_G2_HP_DELTA;
  // if (std::abs(wfw.g2_type) == 2)
  //   ret |= DFI_G2_BOTH_DIR;
  if ((wfw.cls_dir == d) &&
      ABOUT_TIME_DELTA(wfw.tm_cls_near, t_now_, ONE_HALF_HOUR))
    ret |= DFI_WFW_CLS_DIR;
  if ((wfw.vss_dir == d) && ABOUT_TIME_DELTA(wfw.tm_vss_far, t_now_, AN_HOUR))
    ret |= DFI_WFW_VSS_DIR;

  if (is_opposite) {
    if (pdc.confused())
      ret |= DFI_PDC_CONFUSED;
    if (pdc.current_dir() == d)
      ret |= DFI_PDC_CURRENT_OPPOSITE;
    if (pdc.possible_dir() == d)
      ret |= DFI_PDC_POSIBLE_OPPOSITE;
  } else {
    if (pdc.current_dir() == d)
      ret |= DFI_PDC_CURRENT_SAMESIDE;
    if (pdc.possible_dir() == d)
      ret |= DFI_PDC_POSIBLE_SAMESIDE;
  }

  if (wave.solved_cls_spp == d)
    ret |= DFI_SPP_CLS_DIR;
  if (wave.solved_vss_spp == d)
    ret |= DFI_SPP_VSS_DIR;

  if (dirs.mid_dir == d)
    ret |= DFI_MID_DIR;
  if (dirs.ein_dir == d)
    ret |= DFI_VSS_DIR;

  if (pdc.no_mid_ein() && !wave.solved_cls_spp && !wave.solved_vss_spp) {
    ret = 0;
    if (dt_dir_zzzz_g2_and_first__(d))
      ret = 1000;
  }

  return ret;
}

bool
MakeDeal::fight_now_is_swing_() noexcept
{
  bool s = false;
  s = ((swing.swing) || dd_bd.is_swing_dir(t_now_));
  return s;
}

Lines&
MakeDeal::fight_get_the_lines__() noexcept
{
  if (mickey.is_running()) {
    return mickey.target.lines;
  }
  if (Tom.is_running()) {
    return Tom.target.lines;
    // } else if (swing.dir_swing) {
    //   return swing.target.lines;
  } else {
    return lines;
  }
}

dir_t
MakeDeal::big_dir_swing_to_dir_() noexcept
{
  dir_t d = 0;
  if (dd_bd.is_swing_dir(t_now_)) {
    int ced_duration = get_first_ced_duration__();
    if (ced_duration >= 180)
      d = dd_bd.get_first_dir();
  }
  return d;
}

int
MakeDeal::get_first_ced_duration__() noexcept
{
  // !!!
  // must ce = ft = em
  // must have > 2 dirs-changed
  // !!!
  int dur = 0;
  if (dd_ced.get_first_dir() == dd_ft.get_first_dir() &&
      dd_ced.get_first_dir() == dd_pdc.get_first_dir()) {
    int d1 = dd_ced.get_first_durantion(t_now_);
    int d2 = dd_ft.get_first_durantion(t_now_);
    int d3 = dd_pdc.get_first_durantion(t_now_);

    dur = std::max(d1, d2);
    dur = std::max(dur, d3);
  }
  return dur;
}

int
MakeDeal::fight_mickey_mouse_reached(const uint8_t reached) noexcept
{
  if (reached & LIMIT_REACHED::TIMER_OUT) {
    if (dd_ced.get_first_dir() == 0) {
      return (0);
    }
  }

  int ret = 0;
  if (swing.swing && swing.dir_swing == Tom.get_dir()) {
    if (dd_bd.get_first_dir() == Tom.get_dir()) {
      if (dd_ced.get_first_dir() == Tom.get_dir()) {
        // swing = Tom
        // bd = Tom
        // ced = Tom
      } else {
        // swing = Tom
        // bd = Tom
        // ced = -Tom
      }
    } else if (dd_bd.get_first_dir() == -Tom.get_dir()) {
      if (dd_ced.get_first_dir() == Tom.get_dir()) {
        // swing = Tom
        // bd = -Tom
        // ced = Tom
      } else {
        // swing = Tom
        // bd = -Tom
        // ced = -Tom
      }
    } else {
      if (dd_ced.get_first_dir() == Tom.get_dir()) {
        // swing = Tom
        // bd = 0
        // ced = Tom
      } else {
        // swing = Tom
        // bd = 0
        // ced = -Tom
      }
    }
  } else if (swing.swing && swing.dir_swing == -Tom.get_dir()) {
    if (dd_bd.get_first_dir() == Tom.get_dir()) {
      if (dd_ced.get_first_dir() == Tom.get_dir()) {
        // swing = -Tom
        // bd = Tom
        // ced = Tom
      } else {
        // swing = -Tom
        // bd = Tom
        // -ced = Tom
      }
    } else if (dd_bd.get_first_dir() == -Tom.get_dir()) {
      if (dd_ced.get_first_dir() == Tom.get_dir()) {
        // swing = -Tom
        // bd = -Tom
        // ced = Tom
      } else {
        // swing = -Tom
        // bd = -Tom
        // -ced = Tom
      }
    } else {
      if (dd_ced.get_first_dir() == Tom.get_dir()) {
        // swing = -Tom
        // bd = 0
        // ced = Tom
      } else {
        // swing = -Tom
        // bd = 0
        // -ced = Tom
      }
    }
  } else if (!swing.swing) {
    if (dd_bd.get_first_dir() == Tom.get_dir()) {
      if (dd_ced.get_first_dir() == Tom.get_dir()) {
        // swing = 0
        // bd = Tom
        // ced = Tom
      } else {
        // swing = 0
        // bd = Tom
        // -ced = Tom
      }
    } else if (dd_bd.get_first_dir() == -Tom.get_dir()) {
      if (dd_ced.get_first_dir() == Tom.get_dir()) {
        // swing = 0
        // bd = -Tom
        // ced = Tom
      } else {
        // swing = 0
        // bd = -Tom
        // -ced = Tom
      }
    } else {
      if (dd_ced.get_first_dir() == Tom.get_dir()) {
        // swing = 0
        // bd = 0
        // ced = Tom
      } else {
        // swing = 0
        // bd = 0
        // -ced = Tom
      }
    }
  }
  return ret;
}

int
MakeDeal::big_dir_long_long_time__() noexcept
{
  if (dd_bd.dm_list.size() > 3) {
    dd_bd.durantion(dd_bd.get_first_dir(), t_now_);
  }
  return (0);
}

} // namespace trade
