rm *stop*csv

backtest /ssd2/data/test_re/bt888-reshaped.bin $MP/signal/logs/v43/16-ms.log 25000000 25600000 25057197 25057197 ss
mv stop_idx.log 16-stop-idx.log
# mv deal-records.csv deal-records-16.csv

backtest /ssd2/data/test_re/btrsp-20210718-20211228.bin $MP/signal/logs/v43/12-ms.log 2740000 3340000 2796912 2796912 ss
mv stop_idx.log 12-stop-idx.log
# mv deal-records.csv deal-records-12.csv

backtest /ssd2/data/test_re/bt888-reshaped.bin $MP/signal/logs/v43/20-ms.log 12444800 13044800 12503107 12503107 ss
mv stop_idx.log 20-stop-idx.log
# mv deal-records.csv deal-records-20.csv

backtest /ssd2/data/test_re/btrsp-20210718-20211228.bin $MP/signal/logs/v43/9-ms.log 2444800 99999999 2500000 2500000 ss
mv stop_idx.log 9-stop-idx.log
# mv deal-records.csv deal-records-09.csv

backtest /ssd2/data/test_re/bt888-reshaped.bin $MP/signal/logs/v43/3-ms.log 2444800 99999999 2500000 2500000 ss
mv stop_idx.log 3-stop-idx.log
# mv deal-records.csv deal-records-03.csv

backtest /ssd2/data/test_re/bt888-reshaped.bin $MP/signal/logs/v43/21-ms.log 20000000 20600000 20064000 20064000 ss
mv stop_idx.log 21-stop-idx.log
# mv deal-records.csv deal-records-21.csv

backtestd /ssd2/data/test_re/bt888-reshaped.bin $MP/signal/logs/v43/22-ms.log 14550000 15150000 14608469 14608469 ss
mv stop_idx.log 22-stop-idx.log
# mv deal-records.csv deal-records-22.csv

backtest /ssd2/data/test_re/bt888-reshaped.bin /ssd1/mycat/projects/signal/logs/v43/3-30.log 15000000 15600000 15057470 15057470 ss
mv stop_idx.log 3-30-stop-idx.log
# mv deal-records.csv deal-records-3-30.csv
