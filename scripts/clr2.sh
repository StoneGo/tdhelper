#!/bin/bash
printf "\e[38;1;46m Test Text \e[0m 46\n"
printf "\e[38;2;118m Test Text \e[0m 118\n"
printf "\e[38;3;128m Test Text \e[0m 128\n"
printf "\e[38;4;158m Test Text \e[0m 158\n"
printf "\e[38;5;192m Test Text \e[0m 192\n"
printf "\e[38;6;231m Test Text \e[0m 231\n"
printf "\e[38;7;231m Test Text \e[0m 231\n"
printf "\e[38;8;231m Test Text \e[0m 231\n"
printf "\e[38;9;231m Test Text \e[0m 231\n"
printf "\e[38;10;231m Test Text \e[0m 231\n"
printf "\e[38;11;231m Test Text \e[0m 231\n"
printf "\e[38;12;231m Test Text \e[0m 231\n"


for((i=0; i<256; i++)); do
    printf "\e[48;5;${i}m%03d" $i;
    # printf "\e[${i};38;192m%3d" $i;
    # printf "\e[48;${i};001m%03d" $i;
    printf '\e[0m';
    [ ! $((($i - 15) % 6)) -eq 0 ] && printf ' ' || printf '\n'
done


