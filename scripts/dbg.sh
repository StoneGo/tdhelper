#!/bin/bash

EXE=backdbg

# DATAPATH=/home/stone/projects/sdg-test/data
# SIGNALPATH=/home/stone/projects/sdg-test/signal

DATAPATH=/ssd2/data/test_re
# SIGNALPATH=/ssd1/mycat/projects/Figures/fc/logs
SIGNALPATH=/ssd1/mycat/projects/signal/logs
# DATAPATH=/dd/home/frank/data
# SIGNALPATH=/dd/home/frank/signal



if [ $# != 3 ]
then
    echo 'Usage bash dbg.sh <index> <log version> <min-stop-idx>'
else
    if [ $1 = '8' ]
    then
        $EXE $DATAPATH/btrsp-20230525-20230627.tick $SIGNALPATH/v$2/$1.log 2099200 5987200 $3
    elif [ $1 = '4' ]
    then
        $EXE $DATAPATH/bt888-reshaped.bin $SIGNALPATH/v$2/$1.log 25000000 28888000 $3
    elif [ $1 = '1' ]
    then
        $EXE $DATAPATH/bt888-reshaped.bin $SIGNALPATH/v$2/$1.log 14890187 18778187 $3
    elif [ $1 = '7' ]
    then
        $EXE $DATAPATH/btrsp-20210718-20211228.bin $SIGNALPATH/v$2/7.log 8184730 12072730 $3
    elif [ $1 = '2' ]
    then
        $EXE $DATAPATH/bt888-reshaped.bin $SIGNALPATH/v$2/$1.log 21811055 26695543  $3
    elif [ $1 = '3' ]
    then
        $EXE $DATAPATH/bt888-reshaped.bin $SIGNALPATH/v$2/3.log 2444800 3044800  $3
    elif [ $1 = '9' ]
    then
        echo $SIGNALPATH/v$2/9.log
        echo $DATAPATH/btrsp-20210718-20211228.bin
        # $EXE $DATAPATH/btrsp-20210718-20211228.bin $SIGNALPATH/v$2/9.log 2444800 99999999 $3
        $EXE $DATAPATH/btrsp-20210718-20211228.bin $SIGNALPATH/v$2/9-1.log 2444800 3044800 $3
    elif [ $1 = '10' ]
    then
        $EXE $DATAPATH/bt888-reshaped.bin $SIGNALPATH/v$2/10.log 2099200 5899200 $3
    elif [ $1 = '11' ]
    then
        $EXE $DATAPATH/bt888-reshaped.bin $SIGNALPATH/v$2/$1.log 2570000 2870000 $3
     elif [ $1 = '12' ]
    then
        $EXE $DATAPATH/btrsp-20210718-20211228.bin $SIGNALPATH/v$2/$1-ms.log 2740000 2940000 $3
    elif [ $1 = '13' ]
    then
        $EXE $DATAPATH/bt888-reshaped.bin $SIGNALPATH/v$2/$1.log 25700000 26088000 $3
    elif [ $1 = '14' ]
    then
        $EXE $DATAPATH/bt888-reshaped.bin $SIGNALPATH/v$2/$1.log 2620000 2960000 $3
    elif [ $1 = '15' ]
    then
        $EXE $DATAPATH/bt888-reshaped.bin $SIGNALPATH/v$2/$1.log 26400000 26700000 $3
    elif [ $1 = '16' ]
    then
        $EXE $DATAPATH/bt888-reshaped.bin $SIGNALPATH/v$2/$1-ms.log 25000000 25600000 $3
    elif [ $1 = '17' ]
    then
        $EXE $DATAPATH/bt888-reshaped.bin $SIGNALPATH/v$2/$1.log 25300000 26000000 $3
    elif [ $1 = '18' ]
    then
        $EXE $DATAPATH/bt888-reshaped.bin $SIGNALPATH/v$2/$1.log 25000000 25300000 $3
    fi
fi
