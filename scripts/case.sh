#!/bin/bash

EXE=ttwice

DATAPATH=/home/stone/projects/sdg-test/data
SIGNALPATH=/home/stone/projects/sdg-test/signal

# DATAPATH=/app/sean/data/testing/data
# SIGNALPATH=/app/sean/data/testing/signal

# DATAPATH=/ssd2/data/test_re
# SIGNALPATH=/ssd1/mycat/projects/Figures/fc/logs

# DATAPATH=/dd/home/frank/data
# SIGNALPATH=/dd/home/frank/signal


if [ $# != 2 ]
then
    echo 'Usage bash case.sh <index> <log version>'
else
    if [ $1 = '8' ]
    then
        $EXE $DATAPATH/btrsp-20230525-20230627.tick $SIGNALPATH/v$2/$1.log 2099200 5987200
    elif [ $1 = '4' ]
    then
        $EXE $DATAPATH/bt888-reshaped.bin $SIGNALPATH/v$2/$1.log 25000000 28888000
    elif [ $1 = '1' ]
    then
        $EXE $DATAPATH/bt888-reshaped.bin $SIGNALPATH/v$2/$1.log 14890187 18778187
    elif [ $1 = '7' ]
    then
        $EXE $DATAPATH/btrsp-20210718-20211228.bin $SIGNALPATH/v$2/7.log 8184730 12072730
    elif [ $1 = '2' ]
    then
        $EXE $DATAPATH/bt888-reshaped.bin $SIGNALPATH/v$2/$1.log 21811055 99999999
    elif [ $1 = '5' ]
    then
        $EXE $DATAPATH/bt888-reshaped.bin $SIGNALPATH/v$2/$1.log 17023646 22811055
    elif [ $1 = '3' ]
    then
        $EXE $DATAPATH/bt888-reshaped.bin $SIGNALPATH/v$2/$1.log 2099200 99999999
    elif [ $1 = '9' ]
    then
        $EXE $DATAPATH/btrsp-20210718-20211228.bin $SIGNALPATH/v$2/9.log 2099200 99999999
    elif [ $1 = '10' ]
    then
        $EXE $DATAPATH/bt888-reshaped.bin $SIGNALPATH/v$2/10.log 2099200 5899200
    elif [ $1 = '11' ]
    then
        $EXE $DATAPATH/bt888-reshaped.bin $SIGNALPATH/v$2/$1.log 2570000 2870000
    elif [ $1 = '12' ]
    then
        $EXE $DATAPATH/btrsp-20210718-20211228.bin $SIGNALPATH/v$2/$1.log 2740000 2940000
    elif [ $1 = '13' ]
    then
        $EXE $DATAPATH/bt888-reshaped.bin $SIGNALPATH/v$2/$1.log 25700000 26088000
    elif [ $1 = '14' ]
    then
        $EXE $DATAPATH/bt888-reshaped.bin $SIGNALPATH/v$2/$1.log 2620000 2960000
    elif [ $1 = '15' ]
    then
        $EXE $DATAPATH/bt888-reshaped.bin $SIGNALPATH/v$2/$1.log 26400000 26700000
    elif [ $1 = '16' ]
    then
        $EXE $DATAPATH/bt888-reshaped.bin $SIGNALPATH/v$2/$1.log 25000000 25600000
    elif [ $1 = '17' ]
    then
        $EXE $DATAPATH/bt888-reshaped.bin $SIGNALPATH/v$2/$1.log 25300000 26000000
    elif [ $1 = '18' ]
    then
        $EXE $DATAPATH/bt888-reshaped.bin $SIGNALPATH/v$2/$1.log 25000000 25300000
    fi
fi
